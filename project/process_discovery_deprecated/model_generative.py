import torch as th
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import random
import tqdm

# from project.process_discovery_deprecated.layers import *
from layers import *

import time

class GATNetwork(nn.Module):
  def __init__(self, in_dim, hidden_dims, out_dim, num_heads):
    super().__init__()
    self.mCuda = th.cuda.is_available()
    self.embedding_size = in_dim
    self.number_of_layers = len(hidden_dims) + 1
    self.layers = nn.ModuleList([MultiHeadGATLayer(in_dim, hidden_dims[0], num_heads)])
    for layer_in_dim, layer_out_dim in zip(hidden_dims[:-1], hidden_dims[1:]):
      self.layers.append(MultiHeadGATLayer(layer_in_dim * num_heads, layer_out_dim, num_heads))
    self.layers.append(MultiHeadGATLayer(hidden_dims[-1] * num_heads, out_dim, 1))

  def prepare_training(self):
    return 0

  def forward(self, graph, hidden_state):
    for index, layer in enumerate(self.layers):
      hidden_state = layer(graph, hidden_state)
      if index < (self.number_of_layers - 1):
        hidden_state = F.relu(hidden_state)
    return hidden_state


class GenerativeModel(nn.Module):
  def __init__(self, embedding_size):
    super().__init__()
    self.mCuda = th.cuda.is_available()
    # Can be a pretrained discriminative network.
    self.graph_attention_network1 = GATNetwork(in_dim=embedding_size, hidden_dims=[32, 64, 32], out_dim=8, num_heads=4)
    self.graph_attention_network2 = GATNetwork(in_dim=9, hidden_dims=[16], out_dim=8, num_heads=4)
    self.add_place_agent = AddPlaceAgent(9)
    self.choose_place_agent = ChoosePlaceAgent(8)


  def prepare_for_train(self):
    self.graph_attention_network1.prepare_training()
    self.graph_attention_network2.prepare_training()
    self.add_place_agent.prepare_training()
    self.choose_place_agent.prepare_training()
    self.step_count = 0


  def prepare_for_inference(self):
    self.add_place_agent.prepare_inference()
    self.choose_place_agent.prepare_inference()
    self.step_count = 0


  def get_log_prob(self):
    add_place_log_p    = th.cat(self.add_place_agent.log_probabilities).sum()
    choose_place_log_p = th.cat(self.choose_place_agent.log_probabilities).sum()
    return add_place_log_p + choose_place_log_p


  def get_prob(self):
    add_place_p = th.cat(self.add_place_agent.probabilities).prod()
    choose_place_p = th.FloatTensor(self.choose_place_agent.probabilities).prod()
    return add_place_p * choose_place_p


  @property
  def action_step(self):
    old_step_count = self.step_count
    self.step_count += 1
    return old_step_count


  def forward_train(self, graph, features, number_of_places, places):
    self.prepare_for_train()

    n = graph.number_of_nodes()
    decisions = np.zeros(graph.number_of_nodes(), dtype=np.float32)
    decisions[-number_of_places:] = -1.
    if self.mCuda:
      graph.ndata['decision'] = th.cuda.FloatTensor(decisions)
    else:
      graph.ndata['decision'] = th.tensor(decisions)

    h = self.graph_attention_network1(graph, features)
    h = th.cat((h, graph.nodes[:].data['decision'].reshape(n, 1)), dim=1)

    while self.add_place_agent(graph, h, int(len(places) != self.step_count)):
      h = self.graph_attention_network2(graph, h)
      place = self.choose_place_agent(graph, h, place=places[self.action_step], number_of_places=number_of_places)
      h = th.cat((h, graph.nodes[:].data['decision'].reshape(n, 1)), dim=1)

    return -self.get_log_prob()


  def forward_inference(self, graph, features, number_of_places, route=None, prune=0):
    self.prepare_for_inference()
    n = graph.number_of_nodes()
    decisions = np.zeros(graph.number_of_nodes(), dtype=np.float32)
    decisions[-number_of_places:] = -1.
    if self.mCuda:
      graph.ndata['decision'] = th.cuda.FloatTensor(decisions)
    else:
      graph.ndata['decision'] = th.tensor(decisions)

    h = self.graph_attention_network1(graph, features)
    h = th.cat((h, graph.nodes[:].data['decision'].reshape(n, 1)), dim=1)

    pruned_route = None

    joint_probability = 1
    places = []
    while self.add_place_agent(graph, h, None) and len(places) <= 20:
      joint_probability *= self.add_place_agent.probabilities[-1].data.item()
      h = self.graph_attention_network2(graph, h)
      if route is not None:
        places.append(self.choose_place_agent(graph, h, place=None, number_of_places=number_of_places, top=route[self.action_step]))
      else:
        places.append(self.choose_place_agent(graph, h, place=None, number_of_places=number_of_places))
      joint_probability *= self.choose_place_agent.probabilities[-1].data.item()
      h = th.cat((h, graph.nodes[:].data['decision'].reshape(n, 1)), dim=1)
      if joint_probability < prune:
        pruned_route = route[:self.step_count]
        break

    return (places, self.get_prob(), pruned_route)


  def forward(self, graph, features, number_of_places, places=None, route=None, prune=0):
    if self.training:
      return self.forward_train(graph, features, number_of_places, places=places)
    else:
      return self.forward_inference(graph, features, number_of_places, route=route, prune=prune)
