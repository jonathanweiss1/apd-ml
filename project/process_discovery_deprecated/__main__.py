# from project.process_discovery_deprecated.preprocessing import get_data
# from project.process_discovery_deprecated.model_generative import GenerativeModel

from preprocessing import get_data
from model_generative import GenerativeModel
from petrinet import PetrinetHandler

from colorama import Fore, Style
import torch as th
import numpy as np
import random
import tqdm
import time

CUDA = th.cuda.is_available()

from data import *
lll = range(734)

random.seed(1)
trainIndices = random.sample(range(len(lll)), int(0.8 * len(lll)))

lll = [b for i, b in enumerate(lll) if i in trainIndices]
# lll = [b for i, b in enumerate(lll) if i not in trainIndices]

# lll = [6, 254, 355, 622, 785, 1007, 1249, 1315, 1355, 1420, 1509, 1575, 1693, 1698, 1880, 1889, 1951, 2128, 2132, 2185, 2216, 2268]
# lllfp = [78, 203, 275, 756, 899, 970, 1391]
#
# lll = [1249, 1575, 1693, 1880, 2185] # really not doing well
# lll = [2128, 2132]


embedding_size = 22
embedding_strategy = 'onehotshifted'
# embedding_strategy = 'random'
embedding_strategy = 'onehot'
logPrefix = '/home/dominique/TUe/thesis/git_data/process_trees/logs_compressed/'
petrinetPrefix = '/home/dominique/TUe/thesis/git_data/process_trees/petrinets/'
pbs = get_data(lll, fDepth=1, fLogPrefix=logPrefix, fEmbeddingSize=embedding_size, fPetrinetPrefix=petrinetPrefix, embedding_strategy=embedding_strategy) #, fExport='/home/dominique/TUe/thesis/git_data/process_trees/petrinets_predicted/')


model = GenerativeModel(embedding_size)
if CUDA:
  model.cuda()
optimizer = th.optim.Adam(model.parameters(), lr=1e-3)

pbs = list(pbs.values())
numberOfPbs = len(pbs)
print('NUMBER OF DATA POINTS: ', numberOfPbs)

train = True

def evaluate(true_places, predicted_places):
  true_places = sorted(true_places)
  string = ''
  for true_place in true_places:
    if true_place in predicted_places:
      string += Fore.CYAN
    string += f'{true_place}{Style.RESET_ALL} '
  string += Fore.RED
  for predicted_place in predicted_places:
    if predicted_place not in true_places:
      string += f'{predicted_place} '

  return string + Style.RESET_ALL

def getTree(top, depth, routes=None):
  if routes is None or len(routes) == 0:
    routes = [[i] for i in range(top)]
  if len(routes[0]) == depth:
    return routes
  return getTree(top, depth, [route + [t] for route in routes for t in range(top)])

def checkPrefixes(route, prefixes):
  for prefix in prefixes:
    if route[:len(prefix)] == prefix:
      return True
  return False


def export_prediction(target_places, predicted_places, transition_labels, traces=None, fFilename=''):
  pnetHandler = PetrinetHandler()
  pnetHandler.fromPlaces(predicted_places, transition_labels, fPlaceLabels=['' if p in target_places else '1' for p in predicted_places])
  pnetHandler.fromPlaces(target_places, transition_labels, fPlaceLabels=['' if p in predicted_places else '0' for p in target_places])
  if traces is not None:
    startPlace = pnetHandler.addPlace('>')
    pnetHandler.mInitialMarking[startPlace] += 1
    endPlace = pnetHandler.addPlace('|')
    pnetHandler.mFinalMarking[endPlace] += 1
    maxLength = len(max(traces, key=lambda x: len(x)))
    filler_label = len(transition_labels)
    for trace in traces:
      # print([transition_labels[t + 1] for t in trace])
      trace += [filler_label] * (maxLength - len(trace))
      currentNode = startPlace
      for index, transition in enumerate(trace):
        letter = transition_labels[transition + 1] if transition < len(transition_labels) else ''
        nextNode = pnetHandler.addTransition(letter) if index % 2 == 0 else pnetHandler.addPlace(letter)
        pnetHandler.addArc(currentNode, nextNode)
        currentNode = nextNode
      if len(trace) % 2 == 0:
        nextNode = pnetHandler.addTransition('')
        pnetHandler.addArc(currentNode, nextNode)
        currentNode = nextNode
      pnetHandler.addArc(currentNode, endPlace)
  pnetHandler.visualize(fExport=fFilename, fDebug=True)


if not train:
  model.load_state_dict(th.load(f'/home/dominique/TUe/thesis/git/project/process_discovery/checkpoints/checkpoint_gen_2_050.pth')['state_dict'])
  model.training = False
  model.eval()

  n_correct = 0
  n_correct_with_fps = 0
  n_fps = 0
  n_places_incorrect = 0
  n_places_total = 0
  t1 = time.time()
  for pb in pbs:
    start_time = time.time()
    g = pb.mNet
    places = [i for i, p in enumerate(pb.mTarget) if p == 1]
    n_places_total += len(places)
    # prob = 0
    probs = [{'prob': 0}] * 5
    # winning_route = None

    all_places = [str((set(sorted(input)), set(sorted(output)))) for input, output in pb.mPossiblePlaces.keys()]

    routes = sorted(getTree(3, 6))
    routes = [route + [0] * 15 for route in routes]
    prefixes = []
    for route in routes:
      if checkPrefixes(route, prefixes):
        continue
      predicted_place_indices_try, prob_try, prunedPrefix = model(g, pb.mFeatures, len(pb.mTarget), route=route, prune=probs[-1]['prob'])

      # predicted_place_indices_try, prob_try, prunedPrefix = model(g, pb.mFeatures, len(pb.mTarget))
      if prunedPrefix is not None:
        prefixes.append(prunedPrefix)
      correct = set(places) == set(predicted_place_indices_try)
      # if prob_try > prob:
      #   prob = prob_try
      #   predicted_place_indices = predicted_place_indices_try
      #   winning_route = ''.join([str(r) for r in route])
      if prob_try > probs[-1]['prob']:
        probs[-1] = {'prob': prob_try, 'predicted_place_indices': predicted_place_indices_try, 'route': ''.join([str(r) for r in route])}
        probs = sorted(probs, reverse=True, key=lambda x: x['prob'])
        # print(probs)

    for i in range(len(probs)):
      correct = set(places) == set(probs[i]['predicted_place_indices'])
      correct_with_fps = len(set(places) - set(probs[i]['predicted_place_indices'])) == 0
      n_places_incorrect += len(set(places) - set(probs[i]['predicted_place_indices']))
      n_fps += len(set(probs[i]['predicted_place_indices']) - set(places))
      if correct:
        n_correct += 1
      if correct_with_fps:
        n_correct_with_fps += 1
      predicted_places = [all_places[i] for i in probs[i]['predicted_place_indices']]
      # filename = f'/home/dominique/TUe/thesis/git_data/process_trees/petrinets_predicted/{pb.mName:04d}_prediction.png'
      # export_prediction(pb.mTargetPlaces, predicted_places, pb.mTransitionNames, fFilename=filename)
      # filename = f'/home/dominique/TUe/thesis/git_data/process_trees/petrinets_predicted/{pb.mName:04d}_prediction_traces.png'
      # export_prediction(pb.mTargetPlaces, predicted_places, pb.mTransitionNames, pb.mTraces, fFilename=filename)
      evaluation_string = evaluate(pb.mTargetPlaces, predicted_places)
      print(f'{Fore.CYAN if correct else Fore.GREEN if correct_with_fps else ""} ({pb.mName:>7}) ({probs[i]["prob"]:.4f})'
            f'{evaluation_string} {Style.RESET_ALL if correct else ""}({time.time() - start_time:.1f}s) {probs[i]["route"]}')
  print(f'correct: {n_correct}/{numberOfPbs}: {n_correct / numberOfPbs * 100:.2f}%')
  print(f'correct w/ fp: {n_correct_with_fps}/{numberOfPbs}: {n_correct_with_fps / numberOfPbs * 100:.2f}%')
  print(f'correct places: {n_places_total - n_places_incorrect}/{n_places_total}: {(1 - n_places_incorrect / n_places_total) * 100:.2f}%')
  print(f'nfp: {n_fps}/{numberOfPbs}: {n_fps / numberOfPbs * 100:.2f}%')
  print(f'{time.time() - t1:.2f} seconds')

if train:
  model.training = True
  bestLoss = np.inf
  bestEpoch = 0

  for epoch in tqdm.trange(500):
    t11 = time.time()
    sumLoss = 0
    random.shuffle(pbs)
    t1 = time.time()
    for pbCount, pb in enumerate(pbs):
      if embedding_strategy == 'random':
        pb.getFeatures()
      if embedding_strategy == 'onehotshifted':
        pb.getFeatures(shift=(epoch % embedding_size))
      places = [i for i, p in enumerate(pb.mTarget) if p == 1]

      # t1 = time.time()
      g = pb.mNet
      loss = model(g, pb.mFeatures, len(pb.mTarget), places=places)
      # t2 = time.time()
      sumLoss += loss

      optimizer.zero_grad()
      t4 = time.time()
      # print(f'zg, {t4 - t2:.2f}')
      loss.backward()
      t5 = time.time()
      # print(f'bw, { t5 - t4:.2f}')
      optimizer.step()
      t6 = time.time()
      # print(f'os, {t6 - t5:.2f}')

      model.eval()
      predicted_place_indices, prob_try, _ = model(g, pb.mFeatures, len(pb.mTarget))
      model.train()

      all_places = [str((set(input), set(output))) for input, output in pb.mPossiblePlaces.keys()]
      correct = set(places) == set(predicted_place_indices)
      evaluation_string = evaluate(pb.mTargetPlaces, [all_places[i] for i in predicted_place_indices])

      print(f'{Fore.CYAN if correct else ""} epoch: {epoch:03d} ({pbCount:03d}/{numberOfPbs}) {loss:>7.4f} ({g.number_of_nodes():>3} nds) ({pb.mName:>4}) '
            f'{evaluation_string} {Style.RESET_ALL if correct else ""}')

      # print(f'epoch: {epoch:03d} {loss:.4f} {str(sorted(places)):>20} ({pb.mName})')
      # print(f'{Fore.CYAN if correct else ""}epoch: {epoch:03d} {loss:.4f} {str(sorted(places)):>20} {sorted(predicted_places)} ({pb.mName})  ({g.number_of_nodes()} nodes) {Style.RESET_ALL if correct else ""}')

    # print(f'{time.time() - t1:.2f}')
    if sumLoss < bestLoss:
      bestLoss = sumLoss
      bestEpoch = epoch
      if epoch > 10:
        th.save({'state_dict': model.state_dict()}, f'/home/dominique/TUe/thesis/git/project/process_discovery/checkpoints/checkpoint_gen_random_1_{epoch:03d}.pth')
    print(f'best epoch: {bestEpoch}')
