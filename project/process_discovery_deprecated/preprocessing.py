import matplotlib.pyplot as plt
import networkx as nx
import torch as th
import dgl
import itertools
import numpy as np
from collections import OrderedDict, defaultdict
import string
import tqdm

from petrinet import PetrinetHandler


class GraphBuilder:
  def __init__(self, fName, fEmbbedingSize, fTraces, fPetrinetHandler, fTransitions, fDepth=2, fCuda=False, embedding_strategy='onehot'):
    self.mName = fName
    self.mEmbeddingSize = fEmbbedingSize
    self.mCuda = fCuda or th.cuda.is_available()
    self.device = th.device('cuda' if th.cuda.is_available() else 'cpu')
    self.mTraces = fTraces
    self.mNet = dgl.DGLGraph()
    self.mDepth = fDepth
    self.mNumberOfTransitions = len(self.getTransitions())

    self.mTransitionNames = fTransitions
    self.mTargetNet = self.petrinetToDGLGraph(fPetrinetHandler, fTransitions)

    self.mPossiblePlaces = defaultdict(list)
    self.mPetrinetTransitionLabels = [1, self.mNumberOfTransitions + 2] + list(range(2, self.mNumberOfTransitions + 2))
    self.mPetrinetGraph = self.initializePetrinetGraph()

    self.mTraceGraph = self.buildTraceGraph()

    self.initializeFeatures()
    self.mTarget, self.mTargetPlaces = self.getTarget(fFromData=True)

    self.connectTraceGraphToPetrinet()
    self.addSelfEdges()
    self.embedding_strategy = embedding_strategy
    self.mFeatures = self.getFeatures()


  def __hash__(self):
    return hash(str(self.mTargetPlaces))


  def getFeatures(self, shift=0):
    if self.embedding_strategy == 'onehot':
      features = th.zeros((len(self.mNet.nodes), self.mEmbeddingSize), device=self.device)
      for nodeIndex in range(len(self.mNet.nodes)):
        nodeLabel = int(self.mNet.nodes[nodeIndex].data['label'].item())
        features[nodeIndex] = onehot(nodeLabel, self.mEmbeddingSize, cuda=True)

    elif self.embedding_strategy == 'onehotshifted':
      features = th.zeros((len(self.mNet.nodes), self.mEmbeddingSize), device=self.device)
      for nodeIndex in range(len(self.mNet.nodes)):
        nodeLabel = int(self.mNet.nodes[nodeIndex].data['label'].item())
        if nodeLabel == 0:
          features[nodeIndex] = onehot(nodeLabel, self.mEmbeddingSize, cuda=True)
        else:
          features[nodeIndex] = onehot((nodeLabel + shift) % (self.mEmbeddingSize - 1), self.mEmbeddingSize, cuda=True)

    elif self.embedding_strategy == 'random':
      features = th.zeros((len(self.mNet.nodes), self.mEmbeddingSize), device=self.device)
      seeds = np.random.randint(100, size=len(self.mTransitionNames) + 1)
      for nodeIndex in range(len(self.mNet.nodes)):
        nodeLabel = int(self.mNet.nodes[nodeIndex].data['label'].item())
        np.random.seed(seeds[nodeLabel])
        features[nodeIndex] = th.tensor(np.random.random(self.mEmbeddingSize), device=self.device)
        # print(nodeLabel, features[nodeIndex])
      # print(a)
    else:
      raise NotImplementedError(f'Embedding strategy ({self.embedding_strategy}) not implemented, choose one of: [onehot, onehotshifted, random].')
    return features


  def getTarget(self, fFromData=True):
    targetVector = [0] * len(self.mPossiblePlaces)
    targetAdjacencyMatrix = self.mTargetNet.adjacency_matrix(transpose=False).to_dense().numpy()
    targetAdjacencyMatrixT = self.mTargetNet.adjacency_matrix(transpose=True).to_dense().numpy()
    targetPlaces = []
    for index in range(self.mTargetNet.number_of_nodes()):
      if self.mTargetNet.nodes[index].data['label'].item() == 0:
        if fFromData:
          # Get input transitions of the place
          inputTransitions = set(sorted([self.mPetrinetTransitionLabels.index(int(self.mTargetNet.nodes[j].data['label'].item()))
                                  for j, value in enumerate(targetAdjacencyMatrix[index]) if value == 1]))
          # Get output transitions of the place
          outputTransitions = set(sorted([self.mPetrinetTransitionLabels.index(int(self.mTargetNet.nodes[j].data['label'].item()))
                                   for j, value in enumerate(targetAdjacencyMatrixT[index]) if value == 1]))
        else:
          # Get input transitions of the place
          inputTransitions = set(sorted([int(self.mTargetNet.nodes[j].data['label'].item())
                                  for j, value in enumerate(targetAdjacencyMatrix[index]) if value == 1]))
          # Get output transitions of the place
          outputTransitions = set(sorted([int(self.mTargetNet.nodes[j].data['label'].item())
                                   for j, value in enumerate(targetAdjacencyMatrixT[index]) if value == 1]))
        targetPlaces.append(str((inputTransitions, outputTransitions)))
        for index2, possiblePlace in enumerate(self.mPossiblePlaces.keys()):
          if inputTransitions == set(possiblePlace[0]) and outputTransitions == set(possiblePlace[1]):
            targetVector[index2] = 1
            break
    return targetVector, sorted(targetPlaces)

  def getTransitions(self):
    distinctTransitions = set()
    for trace in self.mTraces:
      distinctTransitions.update(set(trace))
    return distinctTransitions

  def addEdgeData(self, fGraph, fNumberOfEdges, fReverse=True):
    totalNumberOfEdges = fGraph.number_of_edges()
    if fReverse:
      if self.mCuda:
        fGraph.edges[range(totalNumberOfEdges - (fNumberOfEdges * 2), totalNumberOfEdges - fNumberOfEdges)].data['direction'] = th.tensor([[[1], [0]]] * fNumberOfEdges).cuda()
        fGraph.edges[range(totalNumberOfEdges - fNumberOfEdges, totalNumberOfEdges)].data['direction'] = th.tensor([[[0], [1]]] * fNumberOfEdges).cuda()
      else:
        if self.mCuda:
          fGraph.edges[range(totalNumberOfEdges - (fNumberOfEdges * 2), totalNumberOfEdges - fNumberOfEdges)].data['direction'] = th.tensor([[[1], [0]]] * fNumberOfEdges)
          fGraph.edges[range(totalNumberOfEdges - fNumberOfEdges, totalNumberOfEdges)].data['direction'] = th.tensor([[[0], [1]]] * fNumberOfEdges)
    else:
      if self.mCuda:
        fGraph.edges[range(totalNumberOfEdges - fNumberOfEdges, totalNumberOfEdges)].data['direction'] = th.tensor([[[1], [0]]] * fNumberOfEdges).cuda()
      else:
        fGraph.edges[range(totalNumberOfEdges - fNumberOfEdges, totalNumberOfEdges)].data['direction'] = th.tensor([[[1], [0]]] * fNumberOfEdges)


  def addPlace(self, fGraph, fInputTransitions, fOutputTransitions, fTraceId=tuple, fVerbose=False):
    if fVerbose:
      print(f'{fInputTransitions} -> {fOutputTransitions}')
    place = (tuple(fInputTransitions), tuple(fOutputTransitions))
    self.mPossiblePlaces[place].append(fTraceId)

    if len(self.mPossiblePlaces[place]) > 1:
      if fVerbose:
        print('Place already added.')
      return

    fGraph.add_nodes(1)
    if self.mCuda:
      fGraph.nodes[[fGraph.number_of_nodes() - 1]].data['label'] = th.FloatTensor([[0]]).cuda()
    else:
      fGraph.nodes[[fGraph.number_of_nodes() - 1]].data['label'] = th.FloatTensor([[0]])
    id = fGraph.number_of_nodes() - 1
    sources = fInputTransitions + [id] * len(fOutputTransitions)
    destinations = [id] * len(fInputTransitions) + fOutputTransitions
    addReverse = True
    if addReverse:
      fGraph.add_edges(sources + destinations, destinations + sources)
    else:
      fGraph.add_edges(sources, destinations)
    self.addEdgeData(fGraph, len(sources), addReverse)


  def getCombinations(self, fTransitions, fMinimumSize=1):
    combinations = []
    for i in range(fMinimumSize, len(fTransitions) + 1):
      combinations.extend(list(itertools.combinations(fTransitions, i)))
    return combinations


  def addXORPlaces(self, fGraph):
    flatten = lambda l: [item for sublist in l for item in sublist]

    sameInputs  = defaultdict(lambda: defaultdict(set))
    sameOutputs = defaultdict(lambda: defaultdict(set))
    for p, traceIds in self.mPossiblePlaces.items():
      sameInputs[p[0]][p[1]].update(traceIds)
      sameOutputs[p[1]][p[0]].update(traceIds)

    # [print(k, v) for k, v in sameOutputs.items()]

    for inputTransitions, outputTransitionGroups in sameInputs.items():
      outputTransitionGroups = [(key, value) for key, value in outputTransitionGroups.items()]
      if len(outputTransitionGroups) > 1:
        outputCombinations = [np.array(combination) for combination in
                              self.getCombinations(outputTransitionGroups, fMinimumSize=2)]

        outputCombinations = [list(combination[:, 0]) for combination in outputCombinations
                              if len(set(flatten(combination[:, 1]))) == len(flatten(combination[:, 1]))]

        for outputTransitions in outputCombinations:
          outputs = list(set(itertools.chain.from_iterable(outputTransitions)))
          # if 1 not in outputs:
          self.addPlace(fGraph, list(inputTransitions), outputs)
    for outputTransitions, inputTransitionGroups in sameOutputs.items():
      inputTransitionGroups = [(key, value) for key, value in inputTransitionGroups.items()]
      if len(inputTransitionGroups) > 1:
        inputCombinations = [np.array(combination) for combination in
                             self.getCombinations(inputTransitionGroups, fMinimumSize=2)]
        inputCombinations = [list(combination[:, 0]) for combination in inputCombinations
                              if len(set(flatten(combination[:, 1]))) == len(flatten(combination[:, 1]))]
        for inputTransitions in inputCombinations:
          inputs = list(set(itertools.chain.from_iterable(inputTransitions)))
          # if 0 not in inputs:
          self.addPlace(fGraph, inputs, list(outputTransitions))


  def addXORPlacesDep(self, fGraph):
    sameInputs  = defaultdict(list)
    sameOutputs = defaultdict(list)
    for p in self.mPossiblePlaces:
      sameInputs[ p[0]].append(p[1])
      sameOutputs[p[1]].append(p[0])

    for inputTransitions, outputTransitionGroups in sameInputs.items():
      if len(outputTransitionGroups) > 1:
        outputCombinations = self.getCombinations(outputTransitionGroups, fMinimumSize=2)
        for outputTransitions in outputCombinations:
          outputs = list(set(itertools.chain.from_iterable(outputTransitions)))
          # if 1 not in outputs:
          self.addPlace(fGraph, list(inputTransitions), outputs)
    for outputTransitions, inputTransitionGroups in sameOutputs.items():
      if len(inputTransitionGroups) > 1:
        inputCombinations = self.getCombinations(inputTransitionGroups, fMinimumSize=2)
        for inputTransitions in inputCombinations:
          inputs = list(set(itertools.chain.from_iterable(inputTransitions)))
          # if 0 not in inputs:
          self.addPlace(fGraph, inputs, list(outputTransitions))

  def initializePetrinetGraph(self):
    self.mPetrinetTransitionLabels = [1, self.mNumberOfTransitions + 2] + list(range(2, self.mNumberOfTransitions + 2))
    net = dgl.DGLGraph()
    net.add_nodes(self.mNumberOfTransitions + 2)
    if self.mCuda:
      net.nodes[range(self.mNumberOfTransitions + 2)].data['label'] = th.FloatTensor(np.array(self.mPetrinetTransitionLabels).reshape(self.mNumberOfTransitions + 2, 1)).cuda()
    else:
      net.nodes[range(self.mNumberOfTransitions + 2)].data['label'] = th.FloatTensor(np.array(self.mPetrinetTransitionLabels).reshape(self.mNumberOfTransitions + 2, 1))

    for traceIndex, trace in enumerate(self.mTraces):
      trace = [t + 2 for t in trace]
      traceLength = len(trace)
      for depth in range(1, self.mDepth + 1):
        self.addPlace(net, [0], [trace[min(depth - 1, traceLength - 1)]], fTraceId=(traceIndex, 0))
        self.addPlace(net, [trace[traceLength - depth]], [1], fTraceId=(traceIndex, traceLength-depth))
        for i in range(-depth + 1, len(trace) - 1):
          fromTo = (max(i, 0), min(i + depth, traceLength - 1))
          self.addPlace(net, [trace[fromTo[0]]], [trace[fromTo[1]]], fTraceId=(traceIndex, fromTo[0]))

    self.addXORPlaces(net)
    placeIds = list(range(self.mNumberOfTransitions + 2, net.number_of_nodes()))
    return net


  def buildTraceGraph(self):
    traceGraph = dgl.DGLGraph()
    traceGraph.add_nodes(2)
    if self.mCuda:
      traceGraph.nodes[[0, 1]].data['label'] = th.FloatTensor([[1], [self.mNumberOfTransitions + 2]]).cuda()
    else:
      traceGraph.nodes[[0, 1]].data['label'] = th.FloatTensor([[1], [self.mNumberOfTransitions + 2]])
    for trace in self.mTraces:
      n = len(traceGraph.nodes)
      tn = len(trace)
      traceGraph.add_nodes(tn)
      newNodeIndices = list(range(n, n + tn))
      if self.mCuda:
        traceGraph.nodes[newNodeIndices].data['label'] = th.FloatTensor(np.array([t + 2 for t in trace]).reshape(tn, 1)).cuda()
      else:
        traceGraph.nodes[newNodeIndices].data['label'] = th.FloatTensor(np.array([t + 2 for t in trace]).reshape(tn, 1))
      sources = [0] + newNodeIndices
      destinations = newNodeIndices + [1]
      addReverse = True
      if addReverse:
        traceGraph.add_edges(sources + destinations, destinations + sources)
      else:
        traceGraph.add_edges(sources, destinations)
      self.addEdgeData(traceGraph, len(sources), addReverse)
    return traceGraph

  def petrinetToDGLGraph(self, fPetrinetHandler, fTransitionLabels=string.ascii_lowercase):
    petrinet = fPetrinetHandler.mPetrinet
    net = dgl.DGLGraph()
    net.add_nodes(len(petrinet.transitions) + len(petrinet.places))
    if self.mCuda:
      net.nodes[range(net.number_of_nodes())].data['label'] = th.FloatTensor(np.zeros((net.number_of_nodes(), 1))).cuda()
    else:
      net.nodes[range(net.number_of_nodes())].data['label'] = th.FloatTensor(np.zeros((net.number_of_nodes(), 1)))
    transitions = list(petrinet.transitions)
    labels = [fTransitionLabels.index(t.label) + 1 for t in transitions]
    if self.mCuda:
      net.nodes[range(len(transitions))].data['label'] = th.FloatTensor(np.array(labels).reshape(len(labels), 1)).cuda()
    else:
      net.nodes[range(len(transitions))].data['label'] = th.FloatTensor(np.array(labels).reshape(len(labels), 1))
    places = list(petrinet.places)

    numberOfTransitions = len(transitions)
    for index, transition in enumerate(transitions):
      net.add_edges([index] * len(transition.out_arcs), [numberOfTransitions + places.index(a.target) for a in transition.out_arcs])
    for index, place in enumerate(places):
      net.add_edges([numberOfTransitions + index] * len(place.out_arcs), [transitions.index(a.target) for a in place.out_arcs])
    return net

  def connectTraceGraphToPetrinet(self):
    n = self.mTraceGraph.number_of_nodes()
    g = dgl.batch([self.mTraceGraph, self.mPetrinetGraph])
    g.flatten()
    for sourceIndex in range(n):
      destinationIndex = self.mPetrinetTransitionLabels.index(int(g.nodes[sourceIndex].data['label'].item())) + n
      g.add_edge(sourceIndex, destinationIndex)
      addReverse = False
      if addReverse:
        g.add_edge(destinationIndex, sourceIndex)
      self.addEdgeData(g, 1, addReverse)
    self.mNet = g

  def addSelfEdges(self):
    # Add self edges
    numberOfNodes = len(self.mNet.nodes)
    self.mNet.add_edges(list(range(numberOfNodes)), list(range(numberOfNodes)))
    self.addEdgeData(self.mNet, numberOfNodes, False)

  def initializeFeatures(self):
    self.mNet.ndata['label'] = th.zeros((self.mNet.number_of_nodes(), 1))

  def visualize(self, fNet='target'):
    nets = {
      'target': self.mTargetNet,
      'full':   self.mNet,
      'petrinet': self.mPetrinetGraph,
      'trace':  self.mTraceGraph
    }
    net = nets[fNet]
    nx_G = net.to_networkx()
    pos = nx.kamada_kawai_layout(nx_G)
    dcolors = ['w', 'r', 'b', 'g', 'm', 'y', 'k', 'c']
    colors = []
    for i in range(len(net.nodes)):
      node = net.nodes[i]
      colors.append(dcolors[int(node.data['label'].item())])
    nx.draw(nx_G, pos, with_labels=True, node_color=colors)
    plt.show()


def onehot(index, embedding_size, cuda=False):
  feature_vector = np.zeros((1, embedding_size))
  feature_vector[0][index - 1] = 1
  if cuda or th.cuda.is_available():
    return th.FloatTensor(feature_vector).cuda()
  else:
    return th.FloatTensor(feature_vector)


def importVariants(fFilename):
  npz = np.load(f'{fFilename}{"" if fFilename[-4:] == ".npz" else ".npz"}', allow_pickle=True)
  transitions = OrderedDict()
  for transitionName, count in npz['transitions']:
    transitions[transitionName] = count

  variants = []
  for count, variant in npz['variants']:
    variants.append({'count': count, 'variant': variant})
  return transitions, variants


def get_data(indices,
            fLogPrefix='/home/dominique/TUe/thesis/git_data/NRIExperimentData/logs/',
            fPetrinetPrefix='/home/dominique/TUe/thesis/git_data/simplePetrinetsReduced/petrinet_',
            fEmbeddingSize=13,
            fDepth=2,
            fVisualize=False,
            fExport='',
            fCuda=False,
            embedding_strategy='onehot',
            fVerbose=False):
  builders = {}
  for i in tqdm.tqdm(indices):
    try:
      transitions, variants = importVariants(f'{fLogPrefix}{i:04d}.npz')
    except FileNotFoundError:
      pass
    transitionLabels = ['>'] + [t[0] for t in transitions] + ['|']
    pnetHandler = PetrinetHandler()

    pnetHandler.importFromFile(f'{fPetrinetPrefix}{i:04d}.pnml')
    # pnetHandler.importFromFile(f'{fPetrinetPrefix}{i}.pnml')
    # pnetHandler.addStartAndEndTransitions()
    # pnetHandler.splitComplexXORPlaces()
    if fVisualize:
      pnetHandler.visualize()
    if fExport != '':
      pnetHandler.visualize(fExport=f'{fExport}{i:04d}_true.png')
      # pnetHandler.export(f'{fExport}{i:04d}.pnml')

    traces = [list(variant['variant']) for variant in variants][:15]
    # [print([v + 2 for v in t]) for t in traces]
    try:
      pb = GraphBuilder(i, fEmbeddingSize, traces, pnetHandler, transitionLabels, fDepth=fDepth, fCuda=fCuda, embedding_strategy=embedding_strategy)
    except ValueError:
      # Silent transition encountered. Can't handle this.
      continue

    if np.sum(pb.mTarget) != len(pnetHandler.mPetrinet.places):
      print('-' * 60)
      print('Not all places from the target petrinet have been constructed as candidate places.')
    else:
      builders[i] = pb

  return builders

# if __name__ == '__main__':
#   lll = [2128, 2132]
#   logPrefix = '/home/dominique/TUe/thesis/git_data/process_trees/logs_compressed/'
#   petrinetPrefix = '/home/dominique/TUe/thesis/git_data/process_trees/petrinets/'
#   pbs = get_data(lll, fDepth=1, fLogPrefix=logPrefix, fPetrinetPrefix=petrinetPrefix)
#   distint
#   for pb in pbs.values():
#     print(pb.mTargetPlaces)