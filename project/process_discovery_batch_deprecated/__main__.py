import dgl
import torch as th
import torch.nn as nn
from colorama import Fore, Style

from preprocessing import get_data, onehot
from layers import GraphEmbedding
from model_generative import GenerativeModel
import time
import tqdm

logPrefix = '/home/dominique/TUe/thesis/git_data/process_trees/logs_compressed/'
petrinetPrefix = '/home/dominique/TUe/thesis/git_data/process_trees/petrinets/'

logPrefix = '/home/dominique/TUe/thesis/git_data/NRIExperimentData/logs/'
petrinetPrefix = '/home/dominique/TUe/thesis/git_data/simplePetrinetsReduced/petrinet_'

lll = [1, 14, 15, 16, 17, 21, 23, 24, 26, 30]
pbs = get_data(lll, fDepth=2, fLogPrefix=logPrefix, fPetrinetPrefix=petrinetPrefix, fVisualize=False)

pbs = list(pbs.values())

bg = dgl.batch([pb.mNet for pb in pbs])
print('number of nodes:', bg.number_of_nodes())

def evaluate(true_places, predicted_places):
  true_places = sorted(true_places)
  string = ''
  for true_place in true_places:
    if true_place in predicted_places:
      string += Fore.CYAN
    string += f'{true_place}{Style.RESET_ALL} '
  string += Fore.RED
  for predicted_place in predicted_places:
    if predicted_place not in true_places:
      string += f'{predicted_place} '

  return string + Style.RESET_ALL

model = GenerativeModel()
if th.cuda.is_available():
  model.cuda()

optimizer = th.optim.Adam(model.parameters(), lr=1e-3)
model.train()
model.training = True

places = []
all_places = []
target_places = []
for pb in pbs:
  places.append([i for i, p in enumerate(pb.mTarget) if p == 1])
  all_places.append([str((set(input), set(output))) for input, output in pb.mPossiblePlaces.keys()])
  target_places.append(pb.mTargetPlaces)
print(len(pbs))

for i in tqdm.trange(500):
  t1 = time.time()
  loss = model(bg, places=places)

  t2 = time.time()
  print(loss)
  optimizer.zero_grad()
  loss.backward()
  optimizer.step()
  t3 = time.time()

  # model.eval()
  # for g, graph_places, graph_all_places, graph_target_places in zip(dgl.unbatch(bg), places, all_places, target_places):
  #   predicted_place_indices, prob_try = model(g)
  #   correct = set(graph_places) == set(predicted_place_indices)
  #   evaluation_string = evaluate(graph_target_places, [graph_all_places[i] for i in predicted_place_indices])
  #   print(evaluation_string)
  # model.train()

  # print(f'forward pass: {t2 - t1:.3f}, backward pass: {t3 - t2:.3f}')
