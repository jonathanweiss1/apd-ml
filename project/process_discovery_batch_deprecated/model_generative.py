import torch as th
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import random
import tqdm
import dgl
# from project.process_discovery_deprecated.layers import *
from layers import *

import time

class GATNetwork(nn.Module):
  def __init__(self, in_dim, hidden_dims, out_dim, num_heads):
    super().__init__()
    self.mCuda = th.cuda.is_available()
    self.embedding_size = in_dim
    self.number_of_layers = len(hidden_dims) + 1
    self.layers = nn.ModuleList([MultiHeadGATLayer(in_dim, hidden_dims[0], num_heads)])
    for layer_in_dim, layer_out_dim in zip(hidden_dims[:-1], hidden_dims[1:]):
      self.layers.append(MultiHeadGATLayer(layer_in_dim * num_heads, layer_out_dim, num_heads))
    self.layers.append(MultiHeadGATLayer(hidden_dims[-1] * num_heads, out_dim, 1))

  def prepare_training(self):
    return 0

  def forward(self, graph, hidden_state):
    for index, layer in enumerate(self.layers):
      hidden_state = layer(graph, hidden_state)
      if index < (self.number_of_layers - 1):
        hidden_state = F.relu(hidden_state)
    return hidden_state


class GenerativeModel(nn.Module):
  def __init__(self):
    super().__init__()
    self.mCuda = th.cuda.is_available()
    # Can be a pretrained discriminative network.
    self.graph_attention_network1 = GATNetwork(in_dim=13, hidden_dims=[32, 64, 32], out_dim=8, num_heads=4)
    self.graph_attention_network2 = GATNetwork(in_dim=9, hidden_dims=[16], out_dim=8, num_heads=4)
    self.add_place_agent = AddPlaceAgent(9)
    self.choose_place_agent = ChoosePlaceAgent(8)


  def prepare_for_train(self):
    self.graph_attention_network1.prepare_training()
    self.graph_attention_network2.prepare_training()
    self.add_place_agent.prepare_training()
    self.choose_place_agent.prepare_training()
    self.step_count = 0


  def prepare_for_inference(self):
    self.add_place_agent.prepare_inference()
    self.choose_place_agent.prepare_inference()
    self.step_count = 0


  def get_log_prob(self):
    add_place_log_p    = th.cat(self.add_place_agent.log_probabilities).sum()
    choose_place_log_p = th.cat(self.choose_place_agent.log_probabilities).sum()
    return add_place_log_p + choose_place_log_p


  def get_prob(self):
    add_place_p = th.cat(self.add_place_agent.probabilities).prod()
    choose_place_p = th.FloatTensor(self.choose_place_agent.probabilities).prod()
    return add_place_p * choose_place_p


  def get_graphs(self, graphbatch, places):
    g_list = dgl.unbatch(graphbatch)
    return [(graph, places[i]) for i, graph in enumerate(g_list) if len(places[i]) > self.step_count]


  def forward_train(self, graphbatch, places):
    self.prepare_for_train()

    n = graphbatch.number_of_nodes()
    h = self.graph_attention_network1(graphbatch, graphbatch.ndata['features'])
    h = th.cat((h, graphbatch.ndata['decision'].reshape(n, 1)), dim=1)

    while sum(self.add_place_agent(graphbatch, h, [int(len(p) > self.step_count) for p in places])) > 0:
      h = self.graph_attention_network2(graphbatch, h)
      graphbatch.ndata['h'] = h
      for graph, graph_places in self.get_graphs(graphbatch, places):
        place = self.choose_place_agent(graph, graph.ndata['h'], place=graph_places[self.step_count])
      h = th.cat((h, graphbatch.nodes[:].data['decision'].reshape(n, 1)), dim=1)
      self.step_count += 1

    return -self.get_log_prob()


  def forward_inference(self, graph, features, route=None):
    self.prepare_for_inference()
    n = graph.number_of_nodes()
    h = self.graph_attention_network1(graph, features)
    h = th.cat((h, graph.ndata['decision'].reshape(n, 1)), dim=1)

    places = []
    while sum(self.add_place_agent(graph, h, None)) > 0 and len(places) <= 20:
      h = self.graph_attention_network2(graph, h)
      if route is not None:
        places.append(self.choose_place_agent(graph, h, place=None, top=route[self.action_step]))
      else:
        places.append(self.choose_place_agent(graph, h, place=None))
      h = th.cat((h, graph.nodes[:].data['decision'].reshape(n, 1)), dim=1)

    return (places, self.get_prob())


  def forward(self, graph, places=None, route=None):
    if self.training:
      return self.forward_train(graph, places=places)
    else:
      return self.forward_inference(graph, graph.ndata['features'], route=route)
