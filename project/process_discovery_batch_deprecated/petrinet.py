from pm4py.visualization.petrinet import factory as vis_factory
from pm4py.objects.petri.importer.versions import pnml as pnml_importer
from pm4py.objects.petri.exporter import factory as pnml_exporter
from pm4py.objects.petri.petrinet import PetriNet, Marking
from pm4py.objects.petri import utils


class PetrinetHandler:
  def __init__(self, fName=''):
    self.mPetrinet = PetriNet(fName)
    self.mInitialMarking = Marking()
    self.mFinalMarking = Marking()
    self.mStochasticInformation = {}

  def importFromFile(self, fFilename):
    if '/' not in fFilename:
      fFilename = f'/home/dominique/TUe/thesis/git_data/petrinets/{fFilename}'
    fFilename = f'{fFilename}{"" if (fFilename[-5:] == ".pnml") else ".pnml"}'
    elements = pnml_importer.import_net(fFilename)
    if len(elements) == 3:
      self.mPetrinet, self.mInitialMarking, self.mFinalMarking = elements
    elif len(elements) == 4:
      self.mPetrinet, self.mInitialMarking, self.mFinalMarking, self.mStochasticInformation = elements

  def addStartAndEndTransitions(self):
    startTransition = self.addTransition('>')
    for startPlace in self.mInitialMarking:
      self.addArc(startTransition, startPlace)
    endTransition = self.addTransition('|')
    for finalPlace in self.mFinalMarking:
      self.addArc(finalPlace, endTransition)

  def splitComplexXORPlaces(self):
    numberOfPlaces = len(self.mPetrinet.places)
    places = list(self.mPetrinet.places)
    placesToRemove = []
    count = 0
    for index in range(numberOfPlaces):
      place = [place for i, place in enumerate(places) if i == index][0]
      if len(place.in_arcs) > 1 and len(place.out_arcs) > 1:
        placesToRemove.append(place)
        initialMarking = place in self.mInitialMarking
        finalMarking = place in self.mFinalMarking
        for in_arc in place.in_arcs:
          count += 1
          newPlace = self.addPlace(f'new_{count}')
          for arc in place.out_arcs:
            self.addArc(newPlace, arc.target)
          if initialMarking and in_arc.source.name == '>':
            self.mInitialMarking[newPlace] += 1
          if finalMarking:
            self.mFinalMarking[newPlace] += 1
          self.addArc(in_arc.source, newPlace)
    for place in placesToRemove:
      utils.remove_place(self.mPetrinet, place)
      for marking in [self.mInitialMarking, self.mFinalMarking]:
        if place in marking:
          del marking[place]

  def addPlace(self, fName=''):
    place = PetriNet.Place(fName)
    self.mPetrinet.places.add(place)
    return place

  def addTransition(self, fName=''):
    transition = PetriNet.Transition(fName, fName)
    self.mPetrinet.transitions.add(transition)
    return transition

  def addArc(self, fSource, fTarget, fWeight=1):
    utils.add_arc_from_to(fSource, fTarget, self.mPetrinet, fWeight)

  def visualize(self, fDebug=False):
    parameters = {'debug': True} if fDebug else {}
    gviz = vis_factory.apply(*self.get(), parameters=parameters)
    vis_factory.view(gviz)

  def get(self, fStochasticInformation=False):
    elements = self.mPetrinet, self.mInitialMarking, self.mFinalMarking
    if fStochasticInformation:
      elements += (self.mStochasticInformation,)

    return elements

  def export(self, fFilename):
    pnml_exporter.apply(self.mPetrinet, self.mInitialMarking, fFilename, final_marking=self.mFinalMarking)


def getPetrinetFromFile(fPetrinetName='', fStochasticInformation=True, fVisualize=False):
  petrinet = PetrinetHandler(fPetrinetName)
  petrinet.importFromFile(fPetrinetName)

  if fVisualize:
    petrinet.visualize()

  return petrinet.get(fStochasticInformation=fStochasticInformation)


def createSequencePetrinet(fTransitions, fName='', fVisualize=False):
  petrinet = PetrinetHandler(fName)
  currentPlace = petrinet.addPlace('')
  petrinet.mInitialMarking[currentPlace] = 1

  for transition in fTransitions:
    t = petrinet.addTransition(transition)
    petrinet.addArc(currentPlace, t)
    currentPlace = petrinet.addPlace('')
    petrinet.addArc(t, currentPlace)

  petrinet.mFinalMarking[currentPlace] = 1

  if fVisualize:
    petrinet.visualize()

  return petrinet.get()


def getTestPetrinet(fPetrinetName='', fVisualize=False):
  petrinet = PetrinetHandler(fPetrinetName)
  source = petrinet.addPlace('source')
  sink1 = petrinet.addPlace('sink1')
  p1 = petrinet.addPlace('')

  a = petrinet.addTransition('a')
  b = petrinet.addTransition('b')
  c = petrinet.addTransition('c')
  d = petrinet.addTransition('d')
  e = petrinet.addTransition('e')

  petrinet.addArc(source, a)
  petrinet.addArc(a, p1)
  petrinet.addArc(p1, c)
  petrinet.addArc(c, p1)
  petrinet.addArc(p1, b)
  petrinet.addArc(b, p1)
  petrinet.addArc(p1, d)
  petrinet.addArc(d, p1)
  petrinet.addArc(p1, e)
  petrinet.addArc(e, sink1)

  petrinet.mInitialMarking[source] = 1
  petrinet.mFinalMarking[sink1] = 1

  if fVisualize:
    petrinet.visualize(fDebug=True)

  return petrinet.get()

if __name__ == '__main__':
  # getPetrinetFromFile('/home/dominique/TUe/thesis/git_data/petrinets/01_running-example_alpha.pnml', fVisualize=True)
  getTestPetrinet(fVisualize=True)
  # createSequencePetrinet('a,b,d,e'.split(','), fVisualize=True)
  # createSequencePetrinet('a,b,d,e'.split(','), fVisualize=True)
  # createSequencePetrinet('a,d,b,e'.split(','), fVisualize=True)
