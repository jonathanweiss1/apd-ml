import torch as th
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Categorical, Bernoulli
import numpy as np
import time

import dgl


def bernoulli_action_log_prob(logits, actions, useCuda):
  """Calculate the log p of an action with respect to a Bernoulli
  distribution. Use logit rather than prob for numerical stability."""
  log_probs = th.cat([F.logsigmoid(-logits), F.logsigmoid(logits)], dim=1)
  if useCuda:
    return log_probs.gather(1, th.cuda.LongTensor(actions).unsqueeze(1))
  else:
    return log_probs.gather(1, th.LongTensor(actions).unsqueeze(1))


class GraphEmbedding(nn.Module):
  def __init__(self, node_hidden_size):
    super().__init__()

    self.graph_hidden_size = 2 * node_hidden_size

    self.node_gating = nn.Sequential(
      nn.Linear(node_hidden_size, 1),
      nn.Sigmoid()
    )
    self.node_to_graph = nn.Linear(node_hidden_size, self.graph_hidden_size)

  def forward(self, g, h):
    g.ndata['hh'] = self.node_gating(h) * self.node_to_graph(h)
    return dgl.sum_nodes(g, 'hh')


class AddPlaceAgent(nn.Module):
  def __init__(self, node_hidden_size):
    super().__init__()
    self.mCuda = th.cuda.is_available()
    self.graph_embedding = GraphEmbedding(node_hidden_size)
    self.add_place = nn.Linear(self.graph_embedding.graph_hidden_size, 1)

  def prepare_training(self):
    self.log_probabilities = []

  def prepare_inference(self):
    self.probabilities = []

  def forward(self, graph, node_embeddings, actions):
    '''
    :param graph:
    :param node_embeddings:
    :param action: 0 for stopping, 1 for adding a place.
    :return: whether to add a new place or not.
    '''
    graph_embedding = self.graph_embedding(graph, node_embeddings)
    logits = self.add_place(graph_embedding)

    if not self.training:
      probs = th.sigmoid(logits)
      action = Bernoulli(probs).sample().item()
      if action == 0:
        self.probabilities.append(1 - probs[0].data)
      else:
        self.probabilities.append(probs[0].data)
      actions = [action]

    add_places = [bool(action == 1) for action in actions]

    if self.training:
      sample_log_probs = bernoulli_action_log_prob(logits, actions, self.mCuda)
      self.log_probabilities.append(sample_log_probs)
    return add_places

class ChoosePlaceAgent(nn.Module):
  def __init__(self, node_hidden_size):
    super().__init__()
    self.mCuda = th.cuda.is_available()
    self.choose_place = nn.Linear(node_hidden_size, 1)

  def prepare_training(self):
    self.log_probabilities = []

  def prepare_inference(self):
    self.probabilities = []

  def forward(self, graph, embeddings, place, top=None):
    n = graph.number_of_nodes()

    embedding_size = embeddings.shape[1]
    places = (graph.ndata['label'] == 0)
    placeEmbeddings = (places * embeddings)
    placeEmbeddings = placeEmbeddings[placeEmbeddings.nonzero(as_tuple=True)].view(places.sum(), embedding_size)

    place_scores = self.choose_place(placeEmbeddings).view(1, -1)
    if self.mCuda:
      graph.ndata['score'] = th.cuda.FloatTensor([(-10**10)] * n).unsqueeze(1)
    else:
      graph.ndata['score'] = th.FloatTensor([(-10 ** 10)] * n).unsqueeze(1)
    graph.ndata['score'] *= (graph.ndata['label'] != 0)
    graph.ndata['score'][(graph.ndata['score'] == 0)] = place_scores

    place_probs = dgl.softmax_nodes(graph, 'score')

    if not self.training:
      place_probs = place_probs[-sum(places):]
      if top is not None:
        place = ((place_probs[0] == sorted(place_probs[0], reverse=True)[top]).nonzero()).data.item()
      else:
        place = Categorical(place_probs.flatten()).sample().item()
      self.probabilities.append(place_probs[place].data)

    if self.mCuda:
      graph.nodes[n - sum(places) + place].data['decision'] = th.cuda.FloatTensor([1.])
    else:
      graph.nodes[n - sum(places) + place].data['decision'] = th.tensor([1.])

    if self.training:
      if place_probs.nelement() > 1:
        self.log_probabilities.append(
          F.log_softmax(place_scores, dim=1)[:, place: place + 1])

    return place


class GATLayer(nn.Module):
  def __init__(self, in_dim, out_dim):
    super().__init__()
    self.fc = nn.Linear(in_dim, out_dim, bias=False)
    self.fcr = nn.Linear(in_dim, out_dim, bias=False)
    self.attn_fc = nn.Linear( 2 * out_dim, 1, bias=False)
    self.attn_fcr = nn.Linear(2 * out_dim, 1, bias=False)
    self.reset_parameters()

  def reset_parameters(self):
    gain = nn.init.calculate_gain('relu')
    nn.init.xavier_normal_(self.fc.weight, gain=gain)
    nn.init.xavier_normal_(self.fcr.weight, gain=gain)
    nn.init.xavier_normal_(self.attn_fc.weight, gain=gain)
    nn.init.xavier_normal_(self.attn_fcr.weight, gain=gain)

  def edge_attention(self, edges):
    z2 = th.cat([edges.src['z'], edges.dst['z']], dim=1)
    zr2 = th.cat([edges.src['zr'], edges.dst['zr']], dim=1)
    a = self.attn_fc(z2)
    ar = self.attn_fc(zr2)
    return {'e': F.leaky_relu(a), 'er': F.leaky_relu(ar)}

  def message_func(self, edges):
    nrNodes, nrFeatures = edges.src['z'].shape
    return {
      'q': th.sum(edges.data['direction'] * th.cat((edges.src['z'],  edges.src['zr']),  dim=1).reshape(nrNodes, 2, nrFeatures), dim=1),
      'e': th.sum(edges.data['direction'] * th.cat((edges.data['e'], edges.data['er']), dim=1).reshape(nrNodes, 2, 1), dim=1)
    }

  def reduce_func(self, nodes):
    alpha = F.softmax(nodes.mailbox['e'], dim=1)
    h = th.sum(alpha * nodes.mailbox['q'], dim=1)
    return {'h': h}

  def forward(self, g, h):
    with g.local_scope():
      z = self.fc(h)
      zr = self.fcr(h)
      g.ndata['z'] = z
      g.ndata['zr'] = zr
      g.apply_edges(self.edge_attention)
      g.update_all(self.message_func, self.reduce_func)
      return g.ndata.pop('h')


class MultiHeadGATLayer(nn.Module):
  def __init__(self, in_dim, out_dim, num_heads):
    super().__init__()
    self.heads = nn.ModuleList()
    for i in range(num_heads):
      self.heads.append(GATLayer(in_dim, out_dim))

  def forward(self, g,  h):
    head_outs = [attn_head(g, h) for attn_head in self.heads]
    return th.cat(head_outs, dim=1)
