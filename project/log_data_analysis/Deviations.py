import pandas as pd
from collections import defaultdict, OrderedDict

class Clusterer:
  def __init__(self, fDeviations, fDeviationCounts):
    self.mDeviations = fDeviations
    self.mDeviationCounts = fDeviationCounts
    self.mClusters   = {}
    self.mCluster    = {
      'validTrace':     self.__clusterByValidTrace,
      'deviationType':  self.__clusterByDeviationType,
      'deviationTrace': self.__clusterByDeviationTrace,
      'subTrace':       self.__clusterBySubTrace,
      'activity':       self.__clusterByActivity,
      'validIndex':     self.__clusterByValidIndex,
    }

  def cluster(self, fClusterBy=''):
    if fClusterBy not in self.mCluster.keys():
      raise ValueError(f'{fClusterBy} not implemented, choose one of {self.mCluster.keys()}.')

    self.mCluster[fClusterBy](fClusterBy)

  @staticmethod
  def removeSpaces(fString):
    return ''.join(str(fString).split(' '))

  def toTable(self):
    columns = ['cluster type', 'cluster key', 'deviation', 'count']
    dataTables = {}
    for clusterBy, clusters in self.mClusters.items():
      table = []
      for key, deviationCluster in clusters.items():
        for deviation, count in deviationCluster.items():
          table.append([clusterBy, str(key), str(deviation), count])
      dataTables[clusterBy] = pd.DataFrame(table, columns=columns)

    return dataTables

  def print(self, fVerbose=False):
    for clusterBy, clusters in self.mClusters.items():
      print('-' * 45)
      print(clusterBy)
      for key, deviations in clusters.items():
        totalCount = sum(deviations.values())
        print(f'{totalCount:>6.1f} {key}')
        if fVerbose:
          for deviation, count in deviations.items():
            print(f'{count}: {deviation}')

  def __clusterByValidTrace(self, fClusterType):
    clusters = defaultdict(lambda: defaultdict(float))
    for deviation in self.mDeviations:
      clusters[tuple(deviation.mValidTrace)][deviation] += self.mDeviationCounts[deviation]
    self.mClusters[fClusterType] = OrderedDict(sorted(clusters.items(), key=lambda t: sum(t[1].values()), reverse=True))

  def __clusterByDeviationType(self, fClusterType):
    clusters = defaultdict(float)
    for deviation in self.mDeviations:
      clusters[deviation.mName] += 1
    self.mClusters[fClusterType] = OrderedDict(sorted(clusters.items(), key=lambda t: t[1], reverse=True))

  def __clusterByDeviationTrace(self, fClusterType):
    clusters = defaultdict(lambda: {'count': 0.0, 'deviations': defaultdict(float)})
    for deviation in self.mDeviations:
      clusters[(tuple(deviation.mDeviation), deviation.mSymbol)]['count'] += self.mDeviationCounts[deviation]
      clusters[(tuple(deviation.mDeviation), deviation.mSymbol)]['deviations'][deviation] += self.mDeviationCounts[deviation]
    self.mClusters[fClusterType] = OrderedDict(sorted(clusters.items(), key=lambda t: t[1]['count'], reverse=True))

  def __clusterBySubTrace(self, fClusterType):
    return

  def __clusterByValidIndex(self, fClusterType):
    clusters = defaultdict(lambda: defaultdict(float))
    for deviation in self.mDeviations:
      clusters[(tuple(deviation.mValidTrace), deviation.mOtherIndex)][deviation] += 1
    self.mClusters[fClusterType] = OrderedDict(sorted(clusters.items(), key=lambda t: sum(t[1].values()), reverse=True))

  def __clusterByActivity(self, fClusterType):
    clusters = defaultdict(float)
    for deviation in self.mDeviations:
      for activity in deviation.mDeviation:
        clusters[activity] += self.mDeviationCounts[deviation]
    self.mClusters[fClusterType] = OrderedDict(sorted(clusters.items(), key=lambda t: t[1], reverse=True))

class Deviation:
  def __init__(self, fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex):
    self.mDeviation  = tuple(fDeviation)
    self.mFullTrace  = fFullTrace
    self.mOwnIndex   = fOwnIndex
    self.mValidTrace = fValidTrace
    self.mOtherIndex = fOtherIndex
    self.mName       = ''

  def __hash__(self):
    return hash((self.mDeviation, self.mName, tuple(self.mFullTrace), tuple(self.mValidTrace), self.mOwnIndex, self.mOtherIndex))

  def __eq__(self, other):
    return hash(self) == hash(other)

  def __ne__(self, other):
    return self != other

  def __str__(self):
    return f'({self.mDeviation} {self.mFullTrace} {self.mValidTrace} {self.mOwnIndex} {self.mOtherIndex}'

class SwapDeviation(Deviation):
  def __init__(self, fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex):
    super().__init__(fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex)
    self.mName = 'swap'
    self.mSymbol = '<->'

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    return f'{super().__str__()} {self.mSymbol})'

class InsertionDeviation(Deviation):
  def __init__(self, fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex):
    super().__init__(fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex)
    self.mName = 'insertion'
    self.mSymbol = '++'

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    return f'{super().__str__()} {self.mSymbol})'

class RepetitionDeviation(Deviation):
  def __init__(self, fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex):
    super().__init__(fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex)
    self.mName = 'repetition'
    self.mSymbol = '<<'

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    return f'{super().__str__()} {self.mSymbol})'

class SubstitutionDeviation(Deviation):
  def __init__(self, fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex):
    super().__init__(fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex)
    self.mName = 'substitution'
    self.mSymbol = '</>'

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    return f'{super().__str__()} {self.mSymbol})'

class OmissionDeviation(Deviation):
  def __init__(self, fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex):
    super().__init__(fDeviation, fFullTrace, fOwnIndex, fValidTrace, fOtherIndex)
    self.mName = 'omission'
    self.mSymbol = '--'

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    return f'{super().__str__()} {self.mSymbol})'

if __name__ == '__main__':
  deviations = [InsertionDeviation([0, 1], [0, 1, 0, 1, 2], 0, [0, 1, 2], 1),
                InsertionDeviation([0, 1], [0, 1, 0, 1, 2], 0, [0, 1, 2], 1)]

  clusterer = Clusterer(deviations)
  for clusterBy in clusterer.mCluster.keys():
    clusterer.cluster(clusterBy)
  clusterer.print()
