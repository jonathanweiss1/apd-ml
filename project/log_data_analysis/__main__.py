from project.log_data_analysis.analyze_log_data import LogAnalyzer, LogAnalyzerLoad
from project.log_data_analysis.comparer import Comparer
from project.data_handling.petrinet import getPetrinetFromFile

import argparse
import json
import os
import time
import numpy as np

np.set_printoptions(linewidth=400)

parser = argparse.ArgumentParser()
parser.add_argument('--verbose', help='Verbose output', action="store_true")
parser.add_argument('-n', '--numberOfTraces', help='Number of traces', type=int)
parser.add_argument('-c', '--config', help='Config filename in json format', type=str)
parser.add_argument('-d', '--dataDirectory', help='dataDirectory', type=str)
parser.add_argument('-lf', '--logFilename', help='logFilename', type=str)
parser.add_argument('-pf', '--petrinetFilename', help='petrinetFilename', type=str)

parser.add_argument('-v', '--visualize', help='Visualize the petrinet', action="store_true")
parser.add_argument('-p', '--plotVariants', help='Plot the variants', action="store_true")
parser.add_argument('-es', '--exportStatistics', help='Export the statistics about the trace variant frequencies', action="store_true")
parser.add_argument('-e', '--exportVariants', help='Export the variants', type=str)
parser.add_argument('-ex', '--exportVariantsXes', help='Export the variants as xes', type=str)
parser.add_argument('-i', '--importVariants', help='Import the variants from npz file', type=str)
parser.add_argument('-cmp', '--compare', help='Compare the variants', nargs=3, action='append')
parser.add_argument('-s', '--save', help='Save log analyzer after loading logs', type=str)
parser.add_argument('-f', '--force', help='Force log analyzer to reload logs', action="store_true")
parser.add_argument('-l', '--load',  help='Load log analyzer object from pickle', type=str)
parser.add_argument('-main', '--mainBehavior',  help='Get main behavior', type=float)
parser.add_argument('-far', '--filterActivitiesRegex', help='Specify regex to filter activities', type=str)
parser.add_argument('-faf', '--filterActivitiesFrequency', help='Filter activities on frequency', type=float)
parser.add_argument('-foa', '--filterActivitiesAttribute', help='Filter activities on attribute', nargs=2, action='append')
parser.add_argument('-port', '--port', help='Port of dash application server', type=int)

args = parser.parse_args()

if args.numberOfTraces is None:
  args.numberOfTraces = -1

if args.config is None:
  args.config = 'config.json'

with open(f'{os.path.dirname(os.path.realpath(__file__))}/{args.config}') as jsonFile:
  config = json.load(jsonFile)

if args.dataDirectory is None:
  args.dataDirectory = config['dataDirectory']

if args.logFilename is None and args.importVariants is None:
  args.logFilename = config['logFilename']

###################
# Start of script #
###################
startTime = time.time()
if args.petrinetFilename is not None:
  net, initialMarking, finalMarking = getPetrinetFromFile(args.petrinetFilename, fStochasticInformation=False, fVisualize=args.visualize)

if args.load is not None:
  logAnalyzer = LogAnalyzerLoad(f'{args.dataDirectory}{args.load}')
elif args.importVariants is not None:
  logAnalyzer = LogAnalyzer(None, fNumberOfTraces=0, fImport=f'{args.dataDirectory}{args.importVariants}')
else:
  logAnalyzer = LogAnalyzer(f'{args.dataDirectory}{args.logFilename}', fNumberOfTraces=args.numberOfTraces)

if args.filterActivitiesAttribute is not None:
  logAnalyzer.filterOnAttribute(args.filterActivitiesAttribute[0][0], args.filterActivitiesAttribute[0][1])

if args.save is not None and not args.force:
  logAnalyzer.save(args.dataDirectory, args.save)

if any([args.exportVariants, args.plotVariants, args.compare, args.mainBehavior]):
  regex = args.filterActivitiesRegex if args.filterActivitiesRegex is not None else ''
  frequency = args.filterActivitiesFrequency if args.filterActivitiesFrequency is not None else 100
  logAnalyzer.getVariants(fRegex=regex, fFrequent=frequency)
  if args.save is not None:
    logAnalyzer.save(args.dataDirectory, args.save)

if args.mainBehavior is not None:
  mainBehavior = logAnalyzer.getMainBehavior(args.mainBehavior, args.exportStatistics)
  countStringLength = len(str(mainBehavior[0]['count']))
  print(f'Total number of variants: {len(logAnalyzer.mVariants)}.')
  print(f'The {args.mainBehavior}% main behavior consists of {len(mainBehavior)} variants:')
  # [print(f'{variant["count"]:>{countStringLength}} {variant["variant"]}') for variant in mainBehavior]

if args.exportVariants is not None:
  logAnalyzer.exportVariantsLog(f'{args.dataDirectory}{args.exportVariants}', fTop=-1)

if args.exportVariantsXes is not None:
  logAnalyzer.export_to_xes(f'{args.dataDirectory}{args.exportVariantsXes}', fTop=-1)

if args.plotVariants:
  datasetName = ''
  if args.logFilename is not None:
    datasetName = f'{" ".join(args.dataDirectory.split("/")[-2].split("_"))} {args.logFilename}'
  elif args.importVariants is not None:
    datasetName = f'{" ".join(args.dataDirectory.split("/")[-2].split("_"))} {args.importVariants}'

  logAnalyzer.plotVariantFrequencies(fName=datasetName, fVisualize=args.visualize, fExportStatistics=args.exportStatistics)


if args.compare is not None:
  if args.compare == [['0', '0', '0']]:
    # Get main behavior from statistics file
    statistics_filename = f'{"".join(logAnalyzer.mLogFilename.split("_compressed"))}_trace_variant_freq_distr.json'
    with open(statistics_filename, 'r') as jsonFile:
      statistics = json.load(jsonFile)
      if statistics['number_of_main_traces_80.0'] > 50:
        print(f'{statistics["number_of_main_traces_80.0"]} > 50')
        statistics["number_of_main_traces_80.0"] = 50
      args.compare = [[statistics['number_of_main_traces_80.0'], statistics['number_of_main_traces_80.0'], -1]]
      print('args.compare:', args.compare)

  mainBehavior = int(args.compare[0][0])
  deviations   = [int(i) for i in args.compare[0][1:]]

  if args.port is None:
    args.port = 8050
  logAnalyzer.deviationsReport([0, mainBehavior], deviations, fPort=args.port, fVerbose=args.verbose, fExport=args.exportStatistics)


# print(f'Total execution time: {time.time() - startTime:.2f} seconds.')