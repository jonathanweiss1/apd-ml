from project.log_data_analysis.Deviations import SwapDeviation, InsertionDeviation, RepetitionDeviation, SubstitutionDeviation, OmissionDeviation
from project.log_data_analysis.DamerauLevenshtein import DamerauLevenshteinAlgorithm
# from Deviations import SwapDeviation, InsertionDeviation, RepetitionDeviation, SubstitutionDeviation, OmissionDeviation
# from DamerauLevenshtein import DamerauLevenshteinAlgorithm

import functools
import numpy as np
from itertools import chain, combinations



class CountCalls:
    def __init__(self, func):
        functools.update_wrapper(self, func)
        self.func = func
        self.num_calls = 0

    def __call__(self, *args, **kwargs):
        self.num_calls += 1
        print(f"Call {self.num_calls} of {self.func.__name__!r}")
        return self.func(*args, **kwargs)

def memoize(function):
  memory = {}

  def memoizer(*args, **kwargs):
    key = str(args) + str(kwargs)
    if key not in memory:
      memory[key] = function(*args, **kwargs)
    return memory[key]

  return memoizer

class Comparer:
  def __init__(self, fVariant1, fVariant2):
    self.mVariant1 = fVariant1
    self.mVariant2 = fVariant2
    self.mCosts = {
      'deletion':     1.0,
      'insertion':    1.0,
      'substitution': 1.5,
      'swap':         0.9
    }
    self.mDamerauLevenshtein = DamerauLevenshteinAlgorithm(self.mVariant1, self.mVariant2)
    self.mDistance = self.mDamerauLevenshtein.getDistance()
    self.mConversionOperations = self.mDamerauLevenshtein.getResult()
    self.mNoiseDetectors = [
      RepetitionDetector(self.mVariant1, self.mVariant2, self.mConversionOperations),
      SwapDetector(self.mVariant1, self.mVariant2, self.mConversionOperations),
      InsertionDetector(self.mVariant1, self.mVariant2, self.mConversionOperations),
      OmissionDetector(self.mVariant1, self.mVariant2, self.mConversionOperations),
      SubstitutionDetector(self.mVariant1, self.mVariant2, self.mConversionOperations)
    ]

  def getDeviations(self):
    deviations = []
    [deviations.extend(detector.mDetections) for detector in self.mNoiseDetectors]
    return deviations

  def detectNoise(self):
    [detector.detect() for detector in self.mNoiseDetectors]

  def computeLevenshteinDistance(self):
    return self.mPatterns

  def damerauLevenshtein(self):
    distances = np.array([[(0, [''])] * (len(self.mVariant2) + 1) for a in range(len(self.mVariant1) + 1)])
    distances[:, 0][1:] = [(i + 1, ['insertion']) for i in range(len(self.mVariant1))]
    distances[0][1:]    = [(i + 1, ['deletion'])  for i in range(len(self.mVariant2))]
    for i in range(len(self.mVariant1)):
      for j in range(len(self.mVariant2)):
        substitutionDistance = (int(self.mVariant1[i] != self.mVariant2[j]) * self.mCosts['substitution'])
        distances[i + 1][j + 1] = min((distances[i][j + 1][0] + self.mCosts['deletion'], distances[i][j + 1][1] + ['deletion']),
                                      (distances[i + 1][j][0] + self.mCosts['insertion'], distances[i + 1][j][1] + ['insertion']),
                                      (distances[i][j][0] + substitutionDistance, distances[i][j][1] + ['' if substitutionDistance == 0 else 'substitution']),
                                      key=lambda x: x[0])
        if i > 1 and j > 1 and self.mVariant1[i] == self.mVariant2[j - 1] and self.mVariant1[i - 1] == self.mVariant2[j]:
          distances[i + 1][j + 1] = min(distances[i + 1][j + 1],
                                        (distances[i - 1][j - 1][0] + self.mCosts['swap'], distances[i - 1][j - 1][1] + ['swap', 'swap']),
                                        key=lambda x: x[0])
    return (distances[-1][-1][0], np.array(distances[-1][-1][1][1:]))

  @memoize
  def levenshtein(self, fVariant1, fVariant2):
    # Compute the minimum edit distance between two trace variants.
    if len(fVariant1) == 0 or len(fVariant2) == 0:
      return max(len(fVariant1), len(fVariant2))

    return min([self.levenshtein(fVariant1[:-1], fVariant2) + self.mCosts['deletion'],
                self.levenshtein(fVariant1, fVariant2[:-1]) + self.mCosts['insertion'],
                self.levenshtein(fVariant1[:-1], fVariant2[:-1]) +
                    int(fVariant1[-1] != fVariant2[-1]) * self.mCosts['substitution']])

class NoiseDetector:
  def __init__(self, fValidVariant, fOtherVariant, fOperations):
    self.mValidVariant = np.array(fValidVariant)
    self.mOtherVariant = np.array(fOtherVariant)
    self.mOperations   = fOperations
    self.mDetections   = []
    self.mName         = ''

  def __str__(self):
    return f'{self.mName:<12} {self.mDetections}'

  def detect(self):
    return []

class RepetitionDetector(NoiseDetector):
  def __init__(self, fValidVariant, fOtherVariant, fOperations):
    super().__init__(fValidVariant, fOtherVariant, fOperations)
    self.mName = 'repetition'

  def detect(self):
    insertions = [operation for operation in self.mOperations if operation.mOperation == 'insertion']
    if len(insertions) == 0:
      return
    indices = np.array([(insertion.mJ - 1, insertion.mI - 1, self.mOtherVariant[insertion.mJ - 1]) for insertion in insertions])
    for group in np.split(indices, np.where(np.diff(indices, axis=0)[:,0] != 1)[0] + 1):
      breakOuterLoop = False
      for i in range(min(len(list(group)), group[0][1]), 0, -1):
        if breakOuterLoop:
          break
        combs = np.array([group[i_:i_ + i] for i_ in range(0, len(group) - i + 1)])
        combs = np.take(combs, np.unique(combs[:, :, 2], axis=0, return_index=True)[1], axis=0)
        for subGroup in combs:
          if (subGroup[-1][0] - subGroup[0][0]) == (len(subGroup) - 1):
            subTrace = np.take(self.mOtherVariant, subGroup[:, 0])
            validTraceCompare = self.mValidVariant[:subGroup[0][1] - len(subGroup) + 2]
            if ((len(validTraceCompare) >= len(subTrace)) and
                any([all(self.mValidVariant[i:i + len(subTrace)] == subTrace) for i in
                     np.where(validTraceCompare == subTrace[0])[0]])):
              self.mDetections.append(RepetitionDeviation(subTrace, self.mOtherVariant, subGroup[0][0], self.mValidVariant, subGroup[0][1]))
              breakOuterLoop = True
              break

class SwapDetector(NoiseDetector):
  def __init__(self, fValidVariant, fOtherVariant, fOperations):
    super().__init__(fValidVariant, fOtherVariant, fOperations)
    self.mName = 'swap'

  def detect(self):
    swaps = np.array([operation for operation in self.mOperations if 'swap' in operation.mOperation])
    for a, b in zip(swaps[::2], swaps[1::2]):
      self.mDetections.append(SwapDeviation(np.take(self.mOtherVariant, [a.mJ - 1, b.mJ - 1]), self.mOtherVariant, a.mJ - 1, self.mValidVariant, a.mI - 1))

class InsertionDetector(NoiseDetector):
  def __init__(self, fValidVariant, fOtherVariant, fOperations):
    super().__init__(fValidVariant, fOtherVariant, fOperations)
    self.mName = 'insertion'

  def detect(self):
    insertions = [operation for operation in self.mOperations if operation.mOperation == 'insertion']
    if len(insertions) == 0:
      return
    indices = np.array([[insertion.mJ - 1, insertion.mI - 1] for insertion in insertions])
    for group in np.split(indices, np.where(np.diff(indices[:, 0]) != 1)[0] + 1):
      self.mDetections.append(InsertionDeviation(np.take(self.mOtherVariant, group[:, 0]), self.mOtherVariant, group[0][0], self.mValidVariant, group[0][1]))
      # for subGroup in chain(*(combinations(group, i) for i in range(1, len(group) + 1))):
      #   if (subGroup[-1] - subGroup[0]) == (len(subGroup) - 1):
      #     self.mDetections.append(InsertionNoise(np.take(self.mOtherVariant, subGroup), self.mOtherVariant))

class OmissionDetector(NoiseDetector):
  def __init__(self, fValidVariant, fOtherVariant, fOperations):
    super().__init__(fValidVariant, fOtherVariant, fOperations)
    self.mName = 'omission'

  def detect(self):
    deletions = [operation for operation in self.mOperations if operation.mOperation == 'deletion']
    if len(deletions) == 0:
      return
    indices = np.array([[deletion.mI - 1, deletion.mJ - 1] for deletion in deletions])
    for group in np.split(indices, np.where(np.diff(indices[:, 0]) != 1)[0] + 1):
      self.mDetections.append(OmissionDeviation(np.take(self.mValidVariant, group[:, 0]), self.mOtherVariant, group[0][1], self.mValidVariant, group[0][0]))
      # for subGroup in chain(*(combinations(group, i) for i in range(1, len(group) + 1))):
      #   if (subGroup[-1] - subGroup[0]) == (len(subGroup) - 1):
      #     self.mDetections.append(OmissionNoise(np.take(self.mValidVariant, subGroup), self.mOtherVariant))

class SubstitutionDetector(NoiseDetector):
  def __init__(self, fValidVariant, fOtherVariant, fOperations):
    super().__init__(fValidVariant, fOtherVariant, fOperations)
    self.mName = 'substitution'

  def detect(self):
    # TODO detect subprocess substitutions
    substitutions = [operation for operation in self.mOperations if operation.mOperation == 'substitution']
    indices = np.array([[substitution.mI - 1, substitution.mJ - 1] for substitution in substitutions])
    for indexPair in indices:
      self.mDetections.append(SubstitutionDeviation([[self.mValidVariant[indexPair[0]]], [self.mOtherVariant[indexPair[1]]]], self.mOtherVariant, indexPair[0], self.mValidVariant, indexPair[1]))

if __name__ == '__main__':
  valids = [
    # [0, 1, 2, 3, 5],
    # [0, 4],
    # [0, 1],
    # [0, 1, 2, 3, 4],
    # [0, 1, 2, 3, 4, 4],
    # [0, 1, 2, 4, 3, 4],
    # [0, 1, 4],
    # [0, 1, 2, 6, 3, 7],
    # [0, 1, 2, 3, 4, 5]
    [3, 7],
  ]

  deviations = [
    # [0, 1, 2, 3, 6, 7],
    # [0, 4, 1],
    # [0, 1, 2, 6, 3, 7, 8, 9, 4],
    # [0, 1, 2, 10, 3, 4],
    # [0, 1, 6, 7],
    # [0, 1, 2, 6, 7, 3, 8, 9, 4],
    # [0, 1, 2, 10, 3, 5],
    # [0, 1, 2, 6, 10, 3, 7]
    [7, 6, 3]
  ]

  for d in deviations:
    distances = [(Comparer(v, d), v) for v in valids]
    comparer, valid = min(distances, key=lambda x: x[0].mDistance)
    # print(comparer.mDistance, valid, d)
    comparer.mDamerauLevenshtein.printConversion()

    comparer.detectNoise()
    [print(detector) for detector in comparer.mNoiseDetectors]
