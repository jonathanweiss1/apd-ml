import json
import matplotlib.pyplot as plt
import numpy as np
import os
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('-e', '--export', action='store_true')
args = parser.parse_args()


plt.style.use('ggplot')

datasets = ['road_traffic_fine', 'sepsis', 'BPI_2012_A', 'BPI_2012_O', 'BPI_2017_A', 'BPI_2017_O', 'BPI_2020_Domestic_declarations',
            'BPI_2020_International_declarations', 'BPI_2020_Prepaid_travel_cost', 'BPI_2020_Request_for_payment', 'BPI_2020_Permit_log']
datasets_short = ['RTF', 'Sepsis', '\'12A', '\'12O', '\'17A', '\'17O', '\'20DD', '\'20ID', '\'20PTC', '\'20RFP', '\'20PL']

real_statistics = {}
for dataset in datasets:
  with open(f'/home/dominique/TUe/thesis/git_data/evaluation_data/{dataset}/data_trace_variant_freq_distr.json', 'r') as file:
    real_statistics[dataset] = json.load(file)

keys = ['alpha', 'alpha_err', 'frequencies_max', 'frequencies_mean', 'frequencies_median', 'frequencies_min', 'number_of_main_traces_80.0', 'number_of_trace_variants',
        'deviation_distributions_geom:in_valid_trace_index:shape', 'deviation_distributions_geom:in_valid_trace_index:sse',
        'deviation_distributions_geom:in_valid_trace:shape', 'deviation_distributions_geom:in_valid_trace:sse',
        'deviation_distributions_geom:all:shape', 'deviation_distributions_geom:all:sse'
        ]

def get_statistics_per_category(categories, datasets_statistics):
  statistics_cat = {}
  for category in categories:
    sub_categories = category.split(':')
    statistics_cat[category] = []
    for dataset_name, dataset_statistics in datasets_statistics.items():
      try:
        value = dataset_statistics[sub_categories[0]]
        for sub_category in sub_categories[1:]:
          value = value[sub_category]
        statistics_cat[category].append(value)
      except KeyError:
        print('pass', category)
        pass
  return statistics_cat

real_statistics_cat = get_statistics_per_category(keys, real_statistics)

synth_datasets = range(2663)
synth_statistics = {}
for synth_dataset in synth_datasets:
  if os.path.isfile(f'/home/dominique/TUe/thesis/git_data/process_trees_medium_ws2/logs_compressed/predictions/{synth_dataset:04d}.pnml'):
    try:
      with open(f'/home/dominique/TUe/thesis/git_data/process_trees_medium_ws2/logs/{synth_dataset:04d}_trace_variant_freq_distr.json', 'r') as file:
        statistics = json.load(file)
        if 'number_of_main_traces_80.0' not in statistics.keys():
          continue
        if statistics['alpha_err'] < 0.2:
          synth_statistics[synth_dataset] = statistics
    except FileNotFoundError:
      pass

synth_statistics_cat = get_statistics_per_category(keys, synth_statistics)

def histogram(stats, normalize=True, bin_step=1., bins=None, label=''):
  weights = np.ones_like(stats) / len(stats)
  if bins is None:
    plt.hist(stats, alpha=0.5, weights=weights, bins=np.arange(1, int(max(stats)) + 1, bin_step), align='mid', label=label)
  else:
    plt.hist(stats, alpha=0.5, weights=weights, bins=bins, align='mid', label=label)

def scatter(dataX, dataY, label=''):
  plt.plot(dataX, dataY, marker='o', linestyle='', alpha=.7, ms=8)

def ratio(list1, list2):
  return np.array(list1) / np.array(list2)

plotRatios = False
if plotRatios:
  histogram(ratio(real_statistics_cat['frequencies_max'], real_statistics_cat['number_of_trace_variants']), bin_step=1)
  histogram(ratio(synth_statistics_cat['frequencies_max'], synth_statistics_cat['number_of_trace_variants']), bin_step=1)
  plt.show()


plotAlphas = True
if plotAlphas:
  figure = plt.figure(figsize=(10, 5))
  real_ratios = ratio(real_statistics_cat['number_of_main_traces_80.0'], real_statistics_cat['number_of_trace_variants'])
  plt.plot(synth_statistics_cat['alpha'], ratio(synth_statistics_cat['number_of_main_traces_80.0'], synth_statistics_cat['number_of_trace_variants']),
           marker='o', linestyle='', alpha=.5, ms=8, label='Synthetic data')
  plt.plot(real_statistics_cat['alpha'], real_ratios,
           marker='o', linestyle='', alpha=.7, ms=8, label='Real-life data')

  for dataset_name, x, y in zip(datasets_short, real_statistics_cat['alpha'], real_ratios):
    print(dataset_name, x, y)
    if y != float('NaN'):
      plt.text(x, y, dataset_name, fontsize=8,
               horizontalalignment=('left' if dataset_name not in ['\'20RFP', '\'20PTC'] else 'right'),
               verticalalignment=('bottom' if dataset_name not in ['RTF'] else 'top'))

  # histogram(synth_statistics_cat['alpha'], bin_step=0.1, label='Synthetic data')
  # histogram(real_statistics_cat['alpha'], bin_step=0.1, label='Real-life data')
  plt.xlabel('alpha')
  plt.ylabel(r'# traces in main behavior / # traces')
  plt.legend()

  figure.savefig(f'/home/dominique/TUe/thesis/report/eval_figures/log_frequencies_alpha_main_scatter.pdf')

  plt.show()

plotShapes = False
if plotShapes:
  plt.style.use('ggplot')
  figure = plt.figure(figsize=(10, 5))
  # plt.hist(statistics_cat['deviation_distributions_geom:all:shape'], alpha=0.5)
  # plt.hist(synth_statistics_cat['deviation_distributions_geom:all:shape'], alpha=0.5)
  # histogram(statistics_cat['deviation_distributions_geom:all:shape'], bins=np.linspace(0, 1, 21), label='Real-life data')
  # histogram(synth_statistics_cat['deviation_distributions_geom:all:shape'], bins=np.linspace(0, 1, 21), label='Synthetic data')


  deviation_type = 'all'
  category = f'deviation_distributions_geom:{deviation_type}:'

  rights = []
  tops = []
  if deviation_type == 'in_valid_trace_index':
    plt.xlim(-0.05, 0.9)
    plt.ylim(-0.02, 0.45)
    rights = ['\'20ID', '\'20PL']
    tops = ['\'17O', '\'20PL', 'Sepsis']
  elif deviation_type == 'all':
    plt.xlim(-0.01, 0.8)
    plt.ylim(-0.01, 0.25)
    rights = ['\'20PL', '\'20ID']
    tops = ['\'17A', '\'12O', '\'20ID', 'Sepsis']
    pass
  elif deviation_type == 'in_valid_trace':
    plt.xlim(-0.01, 0.85)
    plt.ylim(-0.01, 0.3)
    rights = []
    tops = ['\'20PL', '\'20PTC']
    pass


  plt.plot(synth_statistics_cat[category + 'shape'], synth_statistics_cat[category + 'sse'], marker='o', linestyle='', alpha=.5, ms=8, label='Synthetic data')
  plt.plot(real_statistics_cat[category + 'shape'], real_statistics_cat[category + 'sse'], marker='o', linestyle='', alpha=.7, ms=8, label='Real-life data')

  for dataset_name, x, y in zip(datasets_short, real_statistics_cat[category + 'shape'], real_statistics_cat[category + 'sse']):
    print(dataset_name, x, y)
    if y != float('NaN'):
      plt.text(x, y, dataset_name, fontsize=8,
               horizontalalignment=('left' if dataset_name not in rights else 'right'),
               verticalalignment=('bottom' if dataset_name not in tops else 'top'))

  plt.xlabel('Geometric distribution shape parameter')
  plt.ylabel('Geometric distribution sum squared error')
  # histogram(synth_statistics_cat['deviation_distributions_geom:all:shape'])
  plt.legend()

  if args.export:
    figure.savefig(f'/home/dominique/TUe/thesis/report/eval_figures/log_deviations_{deviation_type}.pdf')
  plt.show()