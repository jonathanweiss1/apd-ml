import numpy as np

from pprint import pprint


class DamerauLevenshteinCell:
  def __init__(self, fDistance, fI, fJ, fOperation, fLengths, fPreviousCell=None):
    self.mI = fI
    self.mJ = fJ
    self.mLengths = fLengths
    self.mDistance = fDistance + self.__computeDistanceToDiagonal(*fLengths)
    self.mOperation = fOperation
    self.mPreviousCell = fPreviousCell

  def __computeDistanceToDiagonal(self, fLength1, fLength2):
    return 0
    d = max(0, (abs(self.mI - self.mJ) - abs(fLength1 - fLength2))) / 2
    return np.sqrt(2 * (d)**2)

  def getChain(self):
    if self.mPreviousCell is None:
      return [self]
    return self.mPreviousCell.getChain() + [self]

  def getPreviousSymbol(self):
    if self.mPreviousCell is None:
      return ''
    elif self.mPreviousCell.mI == self.mI and self.mPreviousCell.mJ == (self.mJ - 1):
      return '<'
    elif self.mPreviousCell.mJ == self.mJ and self.mPreviousCell.mI == (self.mI - 1):
      return '^'
    else:
      return '\\'

  def __add__(self, fOther):
    return DamerauLevenshteinCell((self.mDistance + fOther.mDistance), fOther.mI, fOther.mJ, fOther.mOperation, self.mLengths, self)

  def __lt__(self, fOther):
    return self.mDistance < fOther.mDistance

  def __gt__(self, fOther):
    return self.mDistance > fOther.mDistance

  def __eq__(self, fOther):
    return self.mDistance == fOther.mDistance

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    return f'({self.mDistance:>4.1f} [{self.mI} {self.mJ}] {self.mOperation:<10} {self.getPreviousSymbol():<1})'

class DamerauLevenshteinAlgorithm:
  Cell = DamerauLevenshteinCell
  def __init__(self, fVariant1, fVariant2):
    self.mVariant1 = fVariant1
    self.mVariant2 = fVariant2
    self.mLengths = (len(fVariant1), len(fVariant2))
    self.mDistanceMatrix = np.array([[self.Cell(0, i, j, 'equal', self.mLengths) for j in range(len(self.mVariant2) + 1)] for i in range(len(self.mVariant1) + 1)])
    self.mCosts = {
      'deletion': 1.0,
      'insertion': 1.0,
      'substitution': 10.5,
      'swap': 0.9
    }
    self.__perform()

  def __perform(self):
    # Initialize first row and column.
    m = self.mDistanceMatrix
    length1 = len(self.mVariant1)
    length2 = len(self.mVariant2)
    for i in range(length1):
      m[i + 1][0] = m[i][0] + self.Cell(self.mCosts['deletion'], i + 1, 0, 'deletion', self.mLengths)
    for i in range(length2):
      m[0][i + 1] = m[0][i] + self.Cell(self.mCosts['insertion'], 0, i + 1, 'insertion', self.mLengths)

    # Fill rest of the matrix.
    for i in range(length1):
      for j in range(length2):
        substitutionDistance = (int(self.mVariant1[i] != self.mVariant2[j]) * self.mCosts['substitution'])
        m[i + 1][j + 1] = min(
          m[i][j + 1] + self.Cell(self.mCosts['deletion'], i + 1, j + 1, 'deletion', self.mLengths),
          m[i + 1][j] + self.Cell(self.mCosts['insertion'], i + 1, j + 1, 'insertion', self.mLengths),
          m[i][j]     + self.Cell(substitutionDistance, i + 1, j + 1, 'equal' if substitutionDistance == 0 else 'substitution', self.mLengths)
        )

        self.__checkSwap(i, j, m)


        # if min(i, j) > 1 and self.mVariant1[i] == self.mVariant2[j - 1] and self.mVariant1[i - 1] == self.mVariant2[j]:
        #   if (m[i - 1][j - 1].mDistance + self.mCosts['swap']) <= m[i + 1][j + 1].mDistance:
        #     m[i][j] = self.Cell(m[i - 1][j - 1].mDistance, i, j, 'swap>', self.mLengths, m[i - 1][j - 1])
        #     m[i + 1][j + 1] = m[i][j] + self.Cell(self.mCosts['swap'], i + 1, j + 1, '<swap', self.mLengths)
        # pprint(m)

  def __checkSwap(self, i, j, m):
    swap1 = (min(i, j) >= 1 and self.mVariant1[i] == self.mVariant2[j - 1] and self.mVariant1[i - 1] == self.mVariant2[j])
    swap2 = (min(i, j) > 1 and self.mVariant1[i] == self.mVariant2[j - 2] and self.mVariant1[i - 2] == self.mVariant2[j])
    swap3 = (i > 1 and j >= 1 and self.mVariant1[i] == self.mVariant2[j - 1] and self.mVariant1[i - 2] == self.mVariant2[j])
    swap4 = (i >= 1 and j > 1 and self.mVariant1[i] == self.mVariant2[j - 2] and self.mVariant1[i - 1] == self.mVariant2[j])
    # print([swap1, swap2, swap3, swap4])
    if not any([swap1, swap2, swap3, swap4]):
      return
    if swap1:
      if self.mCosts['swap'] + m[i - 1][j - 1].mDistance >= m[i + 1][j + 1].mDistance:
        return
      newDistance = m[i - 1][j - 1].mDistance
      m[i][j] = self.Cell(m[i - 1][j - 1].mDistance, i, j, 'swap>', self.mLengths, m[i - 1][j - 1])
    elif swap2:
      newDistance = m[i][j].mDistance - m[i][j].mPreviousCell.mDistance + m[i - 2][j - 2].mDistance
      if self.mCosts['swap'] + newDistance >= m[i + 1][j + 1].mDistance:
        return
      m[i - 1][j - 1] = self.Cell(m[i - 2][j - 2].mDistance, i - 1, j - 1, 'swap>', self.mLengths, m[i - 2][j - 2])
      if m[i][j].getPreviousSymbol() == '\\':
        m[i][j].mPreviousCell = m[i - 1][j - 1]
      else:
        m[i][j].mPreviousCell.mPreviousCell = m[i - 1][j - 1]
    elif swap3:
      newDistance = m[i][j].mDistance - m[i][j].mPreviousCell.mDistance + m[i - 2][j - 1].mDistance
      if self.mCosts['swap'] + newDistance >= m[i + 1][j + 1].mDistance:
        return
      m[i - 1][j] = self.Cell(m[i - 2][j - 1].mDistance, i - 1, j, 'swap>', self.mLengths, m[i - 2][j - 1])
      m[i][j].mPreviousCell = m[i - 1][j]
    elif swap4:
      newDistance = m[i][j].mDistance - m[i][j].mPreviousCell.mDistance + m[i - 1][j - 2].mDistance
      if self.mCosts['swap'] + newDistance >= m[i + 1][j + 1].mDistance:
        return
      m[i][j - 1] = self.Cell(m[i - 1][j - 2].mDistance, i, j - 1, 'swap>', self.mLengths, m[i - 1][j - 2])
      m[i][j].mPreviousCell = m[i][j - 1]

    m[i][j].mDistance = newDistance
    m[i + 1][j + 1] = m[i][j] + self.Cell(self.mCosts['swap'], i + 1, j + 1, '<swap', self.mLengths)

  def getResult(self):
    return self.mDistanceMatrix[-1][-1].getChain()

  def getDistance(self):
    return self.mDistanceMatrix[-1][-1].mDistance

  def printConversion(self):
    for cell in self.getResult()[1:]:
      s = cell.mOperation
      if cell.mOperation in ['insertion', 'equal'] or 'swap' in cell.mOperation:
        s += f' on {self.mVariant2[cell.mJ - 1]}'
      elif cell.mOperation == 'deletion':
        s += f' on {self.mVariant1[cell.mI - 1]}'
      elif cell.mOperation == 'substitution':
        s += f' on {self.mVariant1[cell.mI - 1]} and {self.mVariant2[cell.mJ - 1]}'

      color = '\033[92m' if cell.mOperation == 'equal' else '\033[91m'
      print(f'{color}{s}\033[0m')

  def __lt__(self, fOther):
    return self.mDistanceMatrix[-1][-1] < fOther.mDistanceMatrix[-1][-1]

  def __gt__(self, fOther):
    return self.mDistanceMatrix[-1][-1] > fOther.mDistanceMatrix[-1][-1]

  def __eq__(self, fOther):
    return self.mDistanceMatrix[-1][-1] == fOther.mDistanceMatrix[-1][-1]

if __name__ == '__main__':
  large_width = 400
  np.set_printoptions(linewidth=large_width)

  valids = [
    # [5, 1, 2, 3],
    # [1, 2, 3, 4, 5, 6],
    # [0, 1, 2, 3, 5],
    # [0, 4],
    # [0, 1],
    # [0, 1, 2, 3, 4],
    # [0, 1, 2, 3, 4, 4],
    # [0, 1, 2, 4, 3, 4],
    # [0, 1, 4],
    # [0, 1, 2, 6, 3, 7],
    # [0, 1, 2, 3, 4, 5]
    [3, 7],
  ]

  deviations = [
    # [0, 1, 2, 3, 6, 7],
    # [0, 4, 1],
    # [0, 1, 2, 6, 3, 7, 8, 9, 4],
    # [0, 1, 2, 10, 3, 4],
    # [0, 1, 6, 7],
    # [0, 1, 2, 6, 7, 3, 8, 9, 4],
    # [0, 1, 2, 10, 3, 5],
    # [0, 1, 2, 6, 10, 3, 7],
    # [0, 1, 2, 7, 6, 3],
    # [1, 2, 3, 7, 7, 7, 7, 7, 7, 4, 5, 6],
    # [4, 3, 2, 1]
    [7, 6, 3]
  ]

  for d in deviations:
    result = min([DamerauLevenshteinAlgorithm(a, d) for a in valids])
    print(f'Comparing {result.mVariant1} and {result.mVariant2}')
    result.printConversion()