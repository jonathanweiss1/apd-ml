from project.data_handling.log import LogHandler
from project.data_handling.petrinet import createSequencePetrinet
from project.data_handling.table import InteractiveTable
from project.log_data_analysis.comparer import Comparer
from project.log_data_analysis.Deviations import Clusterer
from project.log_data_analysis.deviationReport import DeviationReport

from collections import OrderedDict, defaultdict
import warnings
import numpy as np
import pandas as pd
import scipy.stats as st
import statsmodels as sm
from scipy.stats._continuous_distns import _distn_names
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset
import numpy as np
import pickle
import tqdm
import plfit
import json

def LogAnalyzerLoad(fFilename):
  with open(fFilename, 'rb') as pickleFile:
    return pickle.load(pickleFile)

class LogAnalyzer(LogHandler):
  def __init__(self, fLogFilename, fNumberOfTraces=-1, fImport=''):
    super().__init__(fLogFilename, fNumberOfTraces=fNumberOfTraces)
    self.mDeviations = OrderedDict()
    if fImport != '':
      self._importVariants(fImport)

  def save(self, fDirectory, fFilename):
    with open(f'{fDirectory}/{fFilename}', 'wb') as pickleFile:
      pickle.dump(self, pickleFile)

  def printVariants(self, fTop=10):
    [print(variant) for variant in self.mVariants[:fTop]]

  def plotVariant(self, fVariant, fVisualize=False):
    transitionList = list(self.mTransitions.keys())
    transitions = [str(transitionList.index(transition)) for transition in fVariant.split(',')]
    print([int(t) for t in transitions])
    if fVisualize:
      createSequencePetrinet(transitions, fVisualize=True)

  def getMainBehavior(self, fPercentage=90., fExport=False):
    totalCount = sum([variant['count'] for variant in self.mVariants])
    tempCount = 0
    mainBehavior = []
    for variant in self.mVariants:
      tempCount += variant['count']
      if tempCount > totalCount * (fPercentage / 100):
        break
      mainBehavior.append(variant)

    if fExport:
      number_of_main_traces = len(mainBehavior)
      save = True

      statistics_filename = f'{"".join(self.mLogFilename.split("_compressed"))}_trace_variant_freq_distr.json'

      try:
        with open(statistics_filename, 'r') as jsonFile:
          statistics = json.load(jsonFile)
          statistics[f'number_of_main_traces_{fPercentage}'] = number_of_main_traces
      except FileNotFoundError:
        print(f'Nevermind this one: {self.mLogFilename}')
        save = False
        # statistics = {f'number_of_main_traces_{fPercentage}': number_of_main_traces}

      if save:
        with open(statistics_filename, 'w') as jsonFile:
          json.dump(statistics, jsonFile, sort_keys=True, indent=2)


    return mainBehavior

  def plotVariantFrequencies(self, fName='', fVisualize=False, fExportStatistics=False):
    # TODO fix plot issues, for now HACK is used.
    import tkinter
    import matplotlib
    matplotlib.use('TkAgg')
    ys = [variant['count'] for variant in self.mVariants]
    fit = plfit.plfit(ys, verbose=True)

    plt.style.use('ggplot')
    fig, axes = plt.subplots(1, 4, figsize=(17, 4))
    # fig.suptitle(f'Trace variant frequencies distribution of dataset: {fName}')
    axes[0].set_title('Trace variant frequencies')
    axes[0].plot(range(len(self.mVariants)), ys)

    axes[3].set_title(f'Linear regression: $\\alpha=${fit._alpha:.2f} +/- {fit._alphaerr:.2f}')
    fit.plotcdf(axes=axes[3])

    # zoomPlot = zoomed_inset_axes(axes[0, 0], 5, loc=1)
    # subRange = [1, int(len(self.mVariants) * 0.1)]
    # zoomPlot.plot(range(subRange[0], subRange[1]), ys[subRange[0]:subRange[1]])
    # zoomPlot.set_ylim(0, ys[0] * 0.1)
    # mark_inset(axes[0, 0], zoomPlot, loc1=2, loc2=4, fc="none", ec="0.5")

    axes[1].set_title('Trace variant frequencies (y log scale)')
    axes[1].set_yscale('log')
    axes[1].plot(range(len(self.mVariants)), ys)
    axes[2].set_title('Trace variant frequencies (y and x log scale)')
    axes[2].set_yscale('log')
    axes[2].set_xscale('log')
    axes[2].plot(range(len(self.mVariants)), ys)

    for i in range(4):
      axes[i].set_ylabel(f'{"frequency" if i < 3 else "trace variant"}')
      axes[i].set_xlabel(f'{"trace variant" if i < 3 else "frequency"}')

    plt.tight_layout()

    if fVisualize:
      plt.show()
    elif fExportStatistics:
      try:
        with open (f'{self.mLogFilename.split(".")[0]}_trace_variant_freq_distr.json', 'r') as jsonFile:
          statistics = json.load(jsonFile)
      except FileNotFoundError:
        statistics = {}
      statistics['alpha'] = fit._alpha
      statistics['alpha_err'] = fit._alphaerr
      statistics['number_of_trace_variants'] = len(self.mVariants)
      statistics['frequencies_max'] = max(ys)
      statistics['frequencies_min'] = min(ys)
      statistics['frequencies_mean'] = np.mean(ys)
      statistics['frequencies_median'] = np.median(ys)

      with open (f'{self.mLogFilename.split(".")[0]}_trace_variant_freq_distr.json', 'w') as jsonFile:
        json.dump(statistics, jsonFile, sort_keys=True, indent=2)

      plt.savefig(f'{self.mLogFilename.split(".")[0]}_trace_variant_freq_distr.pdf')

  def __getDeviations(self, fValidTraces, fDeviatingTraces, fVerbose=False):
    deviations = defaultdict(float)

    self.mDeviations = []

    for d in tqdm.tqdm(fDeviatingTraces):
      if fVerbose: print('=' * 60)
      distances = [(Comparer(v, d[0]), v) for v in fValidTraces]
      minDistance = min(distances, key=lambda x: x[0].mDistance)[0].mDistance
      mins = [x for x in distances if x[0].mDistance == minDistance]
      for comparer, valid in mins:
        comparer.detectNoise()

        for deviation in comparer.getDeviations():
          self.mDeviations.append(deviation)
          deviations[deviation] += (d[1] / len(mins))

        if fVerbose:
          print(comparer.mDistance, valid, d[0])
          comparer.mDamerauLevenshtein.printConversion()
          [print(detector) for detector in comparer.mNoiseDetectors]

      if fVerbose: print('=' * 60)

    self.mDeviationCounts = OrderedDict(sorted(deviations.items(), key=lambda t: (t[1], str(t[0])), reverse=True))

  def deviationsReport(self, fValidRange, fDeviationsRange, fPort=8050, fVerbose=False, fExport=False):
    validTraces = [v['variant'] for v in self.mVariants[fValidRange[0]:fValidRange[1]]]
    if fDeviationsRange[1] == -1:
      deviatingTraces = [(v['variant'], v['count']) for v in self.mVariants[fDeviationsRange[0]:]]
    else:
      deviatingTraces = [(v['variant'], v['count']) for v in self.mVariants[fDeviationsRange[0]:fDeviationsRange[1]]]
    self.__getDeviations(validTraces, deviatingTraces, fVerbose)

    clusterer = Clusterer(self.mDeviations, self.mDeviationCounts)
    for clusterType in clusterer.mCluster.keys():
      clusterer.cluster(clusterType)

    if fDeviationsRange[1] == -1:
      numberOfTraces = sum([v['count'] for v in self.mVariants[fDeviationsRange[0]:]])
    else:
      numberOfTraces = sum([v['count'] for v in self.mVariants[fDeviationsRange[0]:fDeviationsRange[1]]])

    report = DeviationReport(clusterer, numberOfTraces, validTraces)

    if fExport:
      save = True
      statistics_filename = f'{"".join(self.mLogFilename.split("_compressed"))}_trace_variant_freq_distr.json'
      try:
        with open(statistics_filename, 'r') as jsonFile:
          statistics = json.load(jsonFile)
          statistics['deviation_distributions_geom'] = {
            'all':                  report.deviationsDistribution,
            'in_valid_trace':       report.validTraceDeviationsDistribution,
            'in_valid_trace_index': report.validTraceIndexDeviationsDistribution
          }

      except FileNotFoundError:
        print(f'Nevermind this one: {self.mLogFilename}')
        save = False

      if save:
        with open(statistics_filename, 'w') as jsonFile:
          json.dump(statistics, jsonFile, sort_keys=True, indent=2)

    if not fExport:
      print('run')
      report.run(fPort)

    # print(clusterer.mClusters.keys())
    # print(validTraces)
    # print(clusterer.mClusters['deviationTrace'])

    # for clusterType, table in clusterer.toTable().items():
    #   interactiveTable = InteractiveTable(table, f'table-{clusterType}')

    # clusterer.orderClusters()
    # for clusterBy in clusterer.mCluster.keys():
    #   clusterer.cluster(clusterBy)
    # clusterer.print(fVerbose=False)
    #
    # deviationGroups = defaultdict(dict)
    # for deviation, count in self.mDeviations.items():
    #   deviationGroups[deviation.mName][deviation] = count
    #
    # for deviationType, deviations in deviationGroups.items():
    #   print(f'Deviations of type: {deviationType}:')
    #   for deviation, count in deviations.items():
    #     percentage = count / (fDeviationsRange[1] - fDeviationsRange[0]) * 100
    #     if percentage > 1:
    #       print(f'{count:>4.1f} ({percentage:>6.2f}%) {deviation}')

  def compareVariants(self, fVariant1, fVariant2):
    # print(fVariant1)
    # print(fVariant2)
    comparer = Comparer(fVariant1, fVariant2)
    minimumEditDistance = comparer.computeLevenshteinDistance()
    # TODO implement comparison variants:
    #  - Swapped:
    #  - Repetition
    #  - Wrong timestamp
    #  - Random permutation
    #  - Missing event
    #  - Addition of wrong event
    #  - Incorrect branch / double branching

    return minimumEditDistance


