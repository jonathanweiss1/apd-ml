from   collections import defaultdict
import dash
import dash_core_components as dcc
from   dash.dependencies import Input, Output
import dash_html_components as html
import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objs as go
from scipy.stats import expon, geom

COLORS = {
  'red':   'rgb(227,61,45)',
  'blue':  'rgb(66,135,245)',
  'black': 'rgb(64,64,64)',
  'green': 'green'
}


def check_exponential_distribution(data, normalize_data=True, plot=False):
  y, x = np.histogram(data, bins=np.arange(0, max(data), 0.02), density=True)
  midpoints = (x + np.roll(x, -1))[:-1] / 2.0
  loc, scale = expon.fit(data, floc=0)
  pdf = expon.pdf(midpoints, loc=loc, scale=scale)

  if normalize_data:
    y = y / np.linalg.norm(y)
    pdf = pdf / np.linalg.norm(pdf)

  sse = np.sum(np.power(y - pdf, 2.0))

  # plt.hist(data['y_raw'], histtype='stepfilled', alpha=0.2, bins=np.arange(0, max(data['y_raw']), 0.02), align='mid', density=True)
  if plot:
    plt.style.use('ggplot')
    plt.bar(midpoints, y, width=0.02, alpha=0.5, label='Histogram of data')
    plt.plot(midpoints, pdf, label=f'Prediction exponential curve (scale: {scale:0.3f}, sse: {sse:0.3f})')
    plt.legend()
    plt.show()

  return loc, scale, sse

def check_geometric_distribution(data, plot=False):
  y, x = np.histogram(data, bins=np.array(range(max(data) + 1)) - 0.5, density=True)
  x = np.array(range(1, max(data) + 1))
  shape = 1 / np.mean(data)
  geoms = geom.pmf(x, shape)
  sse = np.sum(np.power(y - geoms, 2.0))

  if plot:
    plt.style.use('ggplot')
    plt.bar(x - 1, sorted(y, reverse=True), width=0.9, alpha=0.5, label='Histogram of data')
    plt.plot(x - 1, geoms, 'o', ms=8, label=f'geom pmf {shape}')

    plt.legend()
    plt.show()

  return shape, sse

class DeviationReport:
  def __init__(self, fDeviationsClusterer, fNumberOfTraces, fValidTraces):
    self.mApp = dash.Dash(__name__)
    self.mDeviationsClusterer = fDeviationsClusterer
    self.mNumberOfTraces = fNumberOfTraces
    self.mValidTraces = fValidTraces
    self.mColors = self.__setColors()
    self.mApp.layout = self.__layout()

  def __setColors(self):
    colors = plt.cm.get_cmap('tab20').colors
    colorMapping = {}
    for index, trace in enumerate(self.mValidTraces):
      colorMapping[tuple(trace)] = f'rgb({",".join([str(a * 255) for a in colors[index % len(colors)]])})'
    return colorMapping

  def run(self, fPort):
    self.__addCallbacks()
    self.mApp.run_server(debug=True, port=fPort)

  def __addCallbacks(self):
    @self.mApp.callback(
      Output('cx1', 'children'),
      [Input('11', 'clickData')])
    def update_figure(fPoints):
      if fPoints is None:
        return
      item = fPoints['points'][0]
      deviations = None
      for index, count in enumerate(self.mDeviationsClusterer.mClusters['deviationTrace'].values()):
        if index == item['pointIndex']:
          deviations = count['deviations']
          break
      return [html.Div(dcc.Graph(
             figure=DeviationFigure2(deviation, count / self.mNumberOfTraces * 100, self.mColors[tuple(deviation.mValidTrace)]).mFigure))
              for deviation, count in sorted(deviations.items(), key=lambda x: x[1], reverse=True)[:20]]

    @self.mApp.callback(
      Output('cx2', 'children'),
      [Input('Graph 1', 'clickData')])
    def update_figure2(fPoints):
      if fPoints is None:
        return
      item = fPoints['points'][0]
      deviationCounts = defaultdict(float)
      for index, (trace, deviationCluster) in enumerate(self.mDeviationsClusterer.mClusters['validTrace'].items()):
        if index == item['curveNumber']:
          for deviation, count in deviationCluster.items():
            if deviation.mOtherIndex == item['pointIndex']:
              deviationCounts[(tuple(deviation.mDeviation), deviation.mSymbol)] += count
          break
      data = {'x': [], 'y': []}
      for deviation, count in sorted(deviationCounts.items(), key=lambda x: x[1], reverse=True):
        data['x'].append(str(deviation))
        data['y'].append(round(count / self.mNumberOfTraces * 100, 1))
      return self.__barChart(data, 'fdsafdsa')


  def __layout(self):
    return html.Div(id='body', className='app-body', children=[
      html.Div(id='left', className='left', children=[
        self.__figureLayout('1'),
        self.__deviationTypeGraph()]),
      html.Div(className='data', children=[
        self.__deviationGraph('11'),
        self.__activityGraph('12'),
        html.Div(id='cx2'),
        html.Div(id='cx1')
      ])
    ])

  def __deviationTypeGraph(self):
    data = {'x': [], 'y': []}
    colors = {'insertion': COLORS['blue'], 'swap': COLORS['red'], 'repetition': COLORS['green'], 'omission': COLORS['black']}
    for deviationTrace, count in self.mDeviationsClusterer.mClusters['deviationTrace'].items():
      data['x'].append(str(deviationTrace))
      data['y'].append(count)

    types = list(self.mDeviationsClusterer.mClusters['deviationType'].keys())
    figure = go.Figure(data=[go.Pie(labels=types,
                                    values=list(self.mDeviationsClusterer.mClusters['deviationType'].values()),
                                    marker_colors=[colors[t] for t in types])])
    return html.Div(dcc.Graph(id='Pie graph', figure=figure))

  def __activityGraph(self, fX):
    distinctActivities = set()
    for validTrace in self.mValidTraces:
      distinctActivities.update(validTrace)
    data = {'x': [], 'y': [], 'colors': []}
    for activity, count in self.mDeviationsClusterer.mClusters['activity'].items():
      data['x'].append(f'({activity})')
      data['y'].append(round(count / self.mNumberOfTraces * 100, 1))
      data['colors'].append(COLORS['blue'] if activity in distinctActivities else COLORS['red'])
    return self.__barChart(data, fX, data['colors'])

  def __deviationGraph(self, fId):
    data = {'x': [], 'y': [], 'y_raw': [], 'y_raw2': []}
    for index, (deviationTrace, count) in enumerate(self.mDeviationsClusterer.mClusters['deviationTrace'].items()):
      data['x'].append(str(deviationTrace))
      data['y'].append(round(count['count'] / self.mNumberOfTraces * 100, 1))
      data['y_raw'].append(count['count'] / self.mNumberOfTraces)
      if index < 100:
        data['y_raw2'].extend([index] * int(np.ceil(count['count'])))

    # loc, scale, sse = check_exponential_distribution(data['y_raw'], plot=False)
    # self.deviationsDistribution = {'scale': scale, 'sse': sse, 'number_of_samples': len(data['y_raw'])}

    shape, sse_geom = check_geometric_distribution(data['y_raw2'], plot=False)
    self.deviationsDistribution = {'shape': shape, 'sse': sse_geom, 'number_of_samples': len(set(data['y_raw2']))}

    return self.__barChart(data, fId)

  def __barChart(self, fData, fId, fColors=None):
    if fColors is None:
      fColors = [COLORS['blue']] * len(fData['x'])
    return dcc.Graph(
      id=fId,
      figure={'data':   [{'x': fData['x'], 'y': fData['y'], 'type': 'bar', 'marker': dict(color=fColors)}],
              'layout': {'margin': dict(t=0, l=0, r=0), 'height': 200}})

  def __figureLayout(self, fId):
    figure = ValidTraceIndicesFigure(self.mDeviationsClusterer, 'validTrace', self.mNumberOfTraces, self.mColors)
    self.validTraceDeviationsDistribution      = figure.validTraceDeviationsDistribution
    self.validTraceIndexDeviationsDistribution = figure.validTraceIndexDeviationsDistribution
    return html.Div(dcc.Graph(id=f'Graph {fId}', figure=figure.mFigure), className='figure')

class ValidTraceIndicesFigure:
  def __init__(self, fDeviationsClusterer, fType, fNumberOfTraces, fColors):
    self.mDeviationsClusterer = fDeviationsClusterer
    self.mType = fType
    self.mNumberOfTraces = fNumberOfTraces
    self.mColors = fColors
    self.mFigure = go.Figure(layout=go.Layout(height=800, plot_bgcolor='white', margin=dict(t=0, l=0, b=0, r=0),
                                              xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                                              yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)))
    self.__drawValidTrace()

  def __createLabel(self, fActivity, fCounts):
    print(fActivity)
    label = f'{fActivity}'

    percentage = f'{fCounts["total"] / self.mNumberOfTraces * 100:.1f}' if self.mNumberOfTraces > 0 else '0.0'
    if percentage == '0.0':
      return label
    label += f'<br>{percentage}%'
    # for deviationType, count in fCounts.items():
    #   percentage = f'{count / self.mNumberOfTraces * 100:.1f}' if self.mNumberOfTraces > 0 else '0.0'
    #   if deviationType == 'total' or percentage == '0.0':
    #     continue
    #   label += f'<br>{deviationType} {percentage}%'
    return label

  def __drawValidTrace(self):
    validTracePercentages = []
    validTracesGeom = []
    validTraceIndexPercentages = []
    validTraceIndexGeom = []
    for index, (trace, deviationCluster) in enumerate(self.mDeviationsClusterer.mClusters[self.mType].items()):
      counts = [defaultdict(float) for i in range(len(trace))]
      totalTraceCount = 0
      for deviation, count in deviationCluster.items():
        totalTraceCount += count
        counts[deviation.mOtherIndex]['total'] += count
        counts[deviation.mOtherIndex][deviation.mSymbol] += count

      labels = []
      for a, c in zip(trace, counts):
        labels.append(self.__createLabel(a, c))
        validTraceIndexPercentages.append(c['total'] / self.mNumberOfTraces)
        validTraceIndexGeom.extend([0 if not len(validTraceIndexGeom) else validTraceIndexGeom[-1] + 1] * int(np.ceil(c['total'])))

      traceLabel = f'{totalTraceCount / self.mNumberOfTraces * 100:.1f}%'
      validTracePercentages.append(totalTraceCount / self.mNumberOfTraces)
      validTracesGeom.extend([index] * int(np.ceil(totalTraceCount)))

      self.__addTraceToFigure(list(range(len(trace))), [-index] * len(trace), labels, traceLabel, self.mColors[tuple(trace)], 'bottom center')

    # loc, scale, sse = check_exponential_distribution(validTracePercentages, plot=False)
    # self.validTraceDeviationsDistribution = {'scale': scale, 'sse': sse, 'number_of_samples': len(validTracePercentages)}
    #
    # loc, scale, sse = check_exponential_distribution(validTraceIndexPercentages, plot=False)
    # self.validTraceIndexDeviationsDistribution = {'scale': scale, 'sse': sse, 'number_of_samples': len(validTraceIndexPercentages)}

    shape, sse_geom = check_geometric_distribution(validTracesGeom, plot=False)
    self.validTraceDeviationsDistribution = {'shape': shape, 'sse': sse_geom, 'number_of_samples': len(set(validTracesGeom))}
    shape, sse_geom = check_geometric_distribution(validTraceIndexGeom, plot=False)
    self.validTraceIndexDeviationsDistribution = {'shape': shape, 'sse': sse_geom, 'number_of_samples': len(set(validTraceIndexGeom))}

  def __addTraceToFigure(self, fXs, fYs, fTransitions, fName, fColor, fTextPosition='middle left'):
    self.mFigure.add_trace(go.Scatter(x=fXs, y=fYs, mode='lines+markers+text', text=fTransitions,
                                      textposition=fTextPosition, line=dict(color=fColor),
                                      marker_color=[fColor]*len(fXs), name=fName))

class DeviationFigure2:
  def __init__(self, fDeviation, fCount, fValidColor):
    self.mDeviation = fDeviation
    self.mCount = fCount
    self.mValidColor = fValidColor
    self.mFigure = go.Figure(layout=go.Layout(height=100, plot_bgcolor='white', margin=dict(t=0, l=0, b=0, r=0),
                                              xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                                              yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)))
    self.__addDeviationToFigure()

  def __addDeviationToFigure(self):
    # TODO align deviations on deviating indices.
    validTrace = np.array([[i, 0, point] for i, point in enumerate(self.mDeviation.mValidTrace)])
    deviationFullTrace = np.array([[i, -1, point] for i, point in enumerate(self.mDeviation.mFullTrace)])
    deviationTrace = None
    if self.mDeviation.mName == 'swap':
      startPoint = list(validTrace[self.mDeviation.mOtherIndex + 1][:2])
      endPoint = list(validTrace[self.mDeviation.mOtherIndex][:2])
      deviationTrace = [startPoint] + [[self.mDeviation.mOwnIndex + i, -1] for i, point in
                                       enumerate(self.mDeviation.mDeviation)] + [endPoint]
    elif self.mDeviation.mName in ['insertion', 'repetition']:
      startPoint = list(validTrace[self.mDeviation.mOtherIndex][:2])
      deviationTrace = [startPoint] + [[self.mDeviation.mOwnIndex + i, -1] for i, point in enumerate(self.mDeviation.mDeviation)]
      if self.mDeviation.mOtherIndex < (len(validTrace) - 1):
        deviationTrace = deviationTrace + [list(validTrace[self.mDeviation.mOtherIndex + 1][:2])]
    elif self.mDeviation.mName == 'omission':
      startPoint = list(validTrace[self.mDeviation.mOtherIndex - 1][:2])
      deviationTrace = [startPoint] + [[self.mDeviation.mOwnIndex + 0.5, -1]]
      try:
        deviationTrace += [list(validTrace[self.mDeviation.mOtherIndex + len(self.mDeviation.mDeviation)][:2])]
      except IndexError:
        pass

    self.__addTraceToFigure(validTrace[:, 0], validTrace[:, 1], validTrace[:, 2], 'Valid', self.mValidColor)
    self.__addTraceToFigure(deviationFullTrace[:, 0], deviationFullTrace[:, 1], deviationFullTrace[:, 2],
                           f'Trace {self.mCount:.1f}%', COLORS['black'], 'top center')
    if deviationTrace is not None:
      deviationTrace = np.array(deviationTrace)
      self.__addTraceToFigure(deviationTrace[:, 0], deviationTrace[:, 1], [''] * len(deviationTrace),
                       'Deviation', COLORS['red'], 'middle right')

  def __addTraceToFigure(self, fXs, fYs, fTransitions, fName, fColor, fTextPosition='bottom center'):
    self.mFigure.add_trace(go.Scatter(x=fXs, y=fYs, mode='lines+markers+text', text=fTransitions,
      textposition=fTextPosition, line=dict(color=fColor), marker_color=['black']*len(fXs), name=fName))
