import dash
from dash.dependencies import Input, Output
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd

class InteractiveTable:
  def __init__(self, fData, fTableName, fApp):
    self.mData = fData
    self.mTableName = fTableName
    self.mApp = fApp
    self.__initialize()

  def __initialize(self):
    self.mTableDiv = html.Div(className='data', children=[
      dash_table.DataTable(
        id=self.mTableName,
        columns=[
          {'name': i, 'id': i, 'selectable': True} for i in self.mData.columns
        ],
        data=self.mData.to_dict('records'),
        filter_action='native',
        sort_action='native',
        sort_mode='multi',
        column_selectable='single',
        row_selectable='multi',
        selected_columns=[],
        selected_rows=[],
        style_cell={'width': '25%'},
        page_action='native',
        page_current=0,
        page_size=15,
      ),
      html.Div(id=f'{self.mTableName}-container')
    ])

    # @self.mApp.callback(
    #   Output(self.mTableName, 'style_data_conditional'),
    #   [Input(self.mTableName, 'selected_columns')]
    # )
    # def update_styles(selected_columns):
    #   return [{
    #       'if': { 'column_id': i },
    #       'background_color': '#D2F3FF'
    #   } for i in selected_columns]

    # @self.mApp.callback(
    #   Output(f'{self.mTableName}-container', 'children'),
    #   [Input(self.mTableName, 'derived_virtual_data'),
    #    Input(self.mTableName, 'derived_virtual_selected_rows')])
    # def update_graphs(rows, derived_virtual_selected_rows):
    #   if derived_virtual_selected_rows is None:
    #     derived_virtual_selected_rows = []
    #
    #   dff = df if rows is None else pd.DataFrame(rows)
    #
    #   colors = ['#7FDBFF' if i in derived_virtual_selected_rows else '#0074D9'
    #             for i in range(len(dff))]
    #
    #   return [
    #     dcc.Graph(id=column, figure={"data": [
    #           {
    #             "x": dff["country"],
    #             "y": dff[column],
    #             "type": "bar",
    #             "marker": {"color": colors},
    #           }
    #         ],
    #         "layout": {
    #           "xaxis": {"automargin": True},
    #           "yaxis": {
    #             "automargin": True,
    #             "title": {"text": column}
    #           },
    #           "height": 250,
    #           "margin": {"t": 10, "l": 10, "r": 10},
    #         },
    #       },
    #     )
    #     for column in ["pop", "lifeExp", "gdpPercap"] if column in dff
    #   ]


if __name__ == '__main__':
  df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder2007.csv')
  table = InteractiveTable(df, 'table1')