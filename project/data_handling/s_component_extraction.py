from pm4py.objects.petri.petrinet import PetriNet
from collections import defaultdict

class SComponentExtraction:
  def __init__(self, petrinet):
    self.petrinet = petrinet
    self.s_comps = []

  def run(self, initial_place):
    D = defaultdict(bool)
    D[initial_place] = True
    self.get_handle([], initial_place, 0, defaultdict(int), defaultdict(int), D, [1])


  def get_handle(self, stack, v, i, prenum, ll, D, maxnum):
    print(f'get handle of {v}')
    i += 1
    prenum[v] = i
    ll[v] = i
    stack.append(v)
    print(prenum)
    for w in sorted([arc.source for arc in v.in_arcs], key=lambda x: str(x), reverse=False):
      # T-arc
      if prenum[w] == 0:
        print('T-arc')
        self.get_handle(stack, w, i, prenum, ll, D, maxnum)
        ll[v] = min(ll[v], ll[w])
        if D[w] and isinstance(v, PetriNet.Transition):
          stack.remove(v)
          return
      # F-arc
      elif prenum[w] > prenum[v]:
        print('F-arc')
        if prenum[w] <= maxnum[0]:
          self.eval_old(stack, v, w, ll, D, verbose=True)
      # B-arc
      elif 0 < prenum[w] < prenum[v] and w in stack:
        print('B-arc')
        ll[v] = min(ll[v], prenum[w])
        if D[w] and isinstance(w, PetriNet.Place):
          self.s_comps.append(f'{w}->{v}')
          print(f'Adding {w}->{v}')
          self.mark_handle(stack, i, prenum, ll, D, maxnum)
          stack.remove(v)
          return
      # C-arc
      elif 0 < prenum[w] < prenum[v] and w not in stack:
        print('C-arc')
        ll[v] = min(ll[v], ll[w])
        if prenum[w] <= maxnum[0]:
          if self.eval_old(stack, v, w, ll, D) == 0:
            self.mark_handle(stack, i, prenum, ll, D, maxnum)
            if isinstance(v, PetriNet.Transition):
              stack.remove(v)
              return

  def mark_handle(self, stack, i, prenum, ll, D, maxnum):
    xprev = None
    for x in stack:
      if maxnum[0] < prenum[x]:
        D[x] = True
        self.s_comps.append(f'{x}->{xprev}')
        print(f'Adding {x}->{xprev}')
        if isinstance(x, PetriNet.Place):
          for y in [arc.source for arc in x.in_arcs]:
            if prenum[y] > 0 and y not in stack:
              self.eval_old(stack, x, y, ll, D)
      xprev = x
    maxnum[0] = i

  def eval_old(self, stack, v, u, ll, D, verbose=False):
    if D[v] or v in stack:
      print(f'D[v] = {D[v]}, v {"" if v in stack else "not "}in stack')
      if isinstance(v, PetriNet.Transition):
        return -1
      elif isinstance(v, PetriNet.Place):
        self.s_comps.append(f'{v}->{u}')
        print(f'Adding {v}->{u}')
        return 0

    D[v] = True
    self.s_comps.append(f'{v}->{u}')
    print(f'Adding {v}->{u}')

    if isinstance(v, PetriNet.Transition):
      predarray = sorted([arc.source for arc in v.in_arcs], key=lambda x: ll[x])
      failures = 0
      ret = -1
      j = 0
      while ret == -1:
        if j == len(predarray):
          break
        w = predarray[j]
        ret = self.eval_old(stack, v, w, ll, D)
        if ret == -1:
          failures += 1
        else:
          break
        j += 1

      if failures == len(predarray):
        D[v] = False
        self.s_comps.remove(f'{v}->{u}')
        print(f'Removing {v}->{u}')
        return -1
    else:
      for w in [arc.source for arc in v.in_arcs]:
        ret = self.eval_old(stack, v, w, ll, D)
        if ret == -1:
          D[v] = False
          self.s_comps.remove(f'{v}->{u}')
          print(f'Removing {v}->{u}')
          return -1
    return 0

if __name__ == '__main__':
  from project.data_handling.petrinet import PetrinetHandler
  from project.data_handling.s_component import compute_s_components

  p_t_arcs = [[0, 0], [2, 1], [3, 2], [4, 3], [5, 3], [0, 4], [1, 5]]
  t_p_arcs = [[0, 2], [0, 3], [1, 4], [2, 5], [3, 1], [4, 1], [5, 0]]

  p_t_arcs = [[0, 0], [0, 1], [1, 2]]
  t_p_arcs = [[0, 1], [1, 1], [2, 0]]

  p_t_arcs = [[0, 0], [1, 1], [2, 1], [3, 2]]
  t_p_arcs = [[0, 1], [0, 2], [1, 3], [2, 0]]
  #
  p_t_arcs = [[0, 0], [1, 1], [2, 2], [3, 3]]
  t_p_arcs = [[0, 1], [0, 2], [1, 3], [2, 3], [3, 0]]

  number_of_places      = len(set([arc[0] for arc in p_t_arcs]).union([arc[1] for arc in t_p_arcs]))
  number_of_transitions = len(set([arc[1] for arc in p_t_arcs]).union([arc[0] for arc in t_p_arcs]))
  petrinet = PetrinetHandler('')

  places      = {name: petrinet.addPlace(f'p{name}') for name in range(number_of_places)}
  transitions = {name: petrinet.addTransition(f't{name}') for name in range(number_of_transitions)}

  for pi, ti in p_t_arcs:
    petrinet.addArc(places[pi], transitions[ti])
  for ti, pi in t_p_arcs:
    petrinet.addArc(transitions[ti], places[pi])
  
  petrinet.visualize(fDebug=True)

  # print(a)
  s_comps = compute_s_components(petrinet.mPetrinet)
  print(s_comps)

  s_component_extraction = SComponentExtraction(petrinet.mPetrinet)
  initial_place = [p for p in petrinet.mPetrinet.places if p.name == 'p0'][0]
  s_component_extraction.run(initial_place=initial_place)
  print(s_component_extraction.s_comps)
