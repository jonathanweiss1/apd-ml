from collections import defaultdict

class Node:
  def __init__(self, fId, fName):
    self.mId = fId
    self.mName = fName

  def __str__(self):
    return f'({self.mId})'

  def __eq__(self, fOther):
    return self.__hash__() == fOther.__hash__()

  def __hash__(self):
    return hash((self.mId))

class State(Node):
  def __init__(self, fId, fName):
    super().__init__(fId, fName)

class Activity(Node):
  def __init__(self, fId, fName):
    super().__init__(fId, fName)

class Edge:
  def __init__(self, fStartNode, fEndNode, fWeight):
    self.mStartNode = fStartNode
    self.mEndNode = fEndNode
    self.mWeight = fWeight

  def __str__(self):
    return f'[{str(self.mStartNode)} -{self.mWeight}-> {str(self.mEndNode)}]'

  def __eq__(self, fOther):
    return self.__hash__() == fOther.__hash__()

  def __hash__(self):
    return hash((self.mStartNode.mId, self.mEndNode.mId))

class Graph:
  def __init__(self, fTraces):
    self.mNodes = set()
    self.mEdges = set()
    self.mAdjacencyGraph = defaultdict(list)
    self.mAdjacencyMatrix = []
    self.__constructGraphFromTraces(fTraces)

  def createAdjacencyMatrix(self):
    self.mAdjacencyMatrix = []
    sortedNodes = sorted(list(self.mNodes), key=lambda x: x.mId)
    numberOfNodes = len(sortedNodes)
    for index, node in enumerate(sortedNodes):
      self.mAdjacencyMatrix.append([0] * numberOfNodes)
      for edge in self.mAdjacencyGraph[node]:
        self.mAdjacencyMatrix[index][sortedNodes.index(edge.mEndNode)] = edge.mWeight

  def __constructGraphFromTraces(self, fTraces):
    for trace in fTraces:
      weight = trace['count']
      nodes = self.__getEventsFromTrace(trace['variant'])
      self.mNodes.update(nodes)
      for startNode, endNode in zip(nodes[:-1], nodes[1:]):
        edge = Edge(startNode, endNode, weight)
        self.__addEdge(edge)

  def __addEdge(self, fEdge):
    weight = fEdge.mWeight
    exists = False
    for existingEdge in self.mEdges:
      if existingEdge == fEdge:
        fEdge = existingEdge
        exists = True
        break
    if exists:
      fEdge.mWeight += weight
    else:
      self.mAdjacencyGraph[fEdge.mStartNode].append(fEdge)
      self.mEdges.add(fEdge)

  def __getEventsFromTrace(self, fTrace):
    return [self.__constructNodeFromEvent(activityName) for activityName in fTrace.split(',')]

  def __constructNodeFromEvent(self, fEvent):
    return Activity(fEvent, fEvent)

if __name__ == '__main__':
  import time

  startTime = time.time()

  from pm4py.objects.log.importer.xes import factory as xes_importer
  from pm4py.statistics.traces.log import case_statistics

  LOG_FILENAME = '../../../git_data/2imi20_data/log.xes'
  log = xes_importer.import_log(LOG_FILENAME)

  from pm4py.statistics.traces.log import case_statistics

  variants_count = case_statistics.get_variant_statistics(log)
  variants_count = sorted(variants_count, key=lambda x: x['count'], reverse=True)

  g = Graph(variants_count)
  g.createAdjacencyMatrix()

  from pprint import pprint
  pprint(g.mAdjacencyMatrix, width=180)

  print(f'{time.time() - startTime:.3f} seconds')

  # print(g.mAdjacencyGraph)
  #
  # [print(e) for e in sorted(g.mEdges, key=lambda  x: x.mWeight, reverse=True)]


