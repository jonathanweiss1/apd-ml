from project.process_mining.process_discovery import AlphaMiner, HeuristicsMiner, InductiveMiner, AIMiner

import argparse
import json
import os

miners = {
  'alpha':      AlphaMiner,
  'heuristics': HeuristicsMiner,
  'inductive':  InductiveMiner,
  'ai':         AIMiner
}

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', help='Config filename in json format', type=str)
parser.add_argument('-d', '--dataDirectory', help='dataDirectory', type=str)
parser.add_argument('-l', '--logFilename', help='logFilename', type=str)
parser.add_argument('-m', '--miner', choices=['alpha', 'heuristics', 'inductive', 'ai'], help='Miner to use for discovery')
parser.add_argument('-mf', '--model_filename', help='Filename of the statedict of the AI model', type=str)
parser.add_argument('-v', '--visualize', help='Visualize the petrinet', action="store_true")
parser.add_argument('-e', '--export', help='Export the petrinet', type=str)
parser.add_argument('-cc', '--conformanceChecking', help='Perform conformance checking on discovered model(s)', action="store_true")
parser.add_argument('-bw', '--beam_width', help='Beam width', type=int)
parser.add_argument('-bl', '--beam_length', help='Beam length', type=int)
parser.add_argument('-np', '--number_of_petrinets', help='Number of petrinets to find', type=int)
parser.add_argument('-tx', '--top_x_traces', help='Number of trace variants to take into account', type=int)
parser.add_argument('-ln', '--length_normalization', help='Perform length normalization', type=int)

args = parser.parse_args()

if args.config is None:
  args.config = 'config.json'

with open(f'{os.path.dirname(os.path.realpath(__file__))}/{args.config}') as jsonFile:
  config = json.load(jsonFile)

if args.dataDirectory is None:
  args.dataDirectory = config['dataDirectory']

if args.logFilename is None:
  args.logFilename = config['logFilename']

if args.miner is None:
  args.miner = config['miner']

###################
# Start of script #
###################

if args.miner == 'ai':
  discoverer = miners[args.miner](f'{args.dataDirectory}{args.logFilename}', args.model_filename)
  options = {
    'beam_width': args.beam_width,
    'beam_length': args.beam_length,
    'number_of_petrinets': args.number_of_petrinets if args.number_of_petrinets is not None else 1,
    'export': '' if args.export is None else args.export,
    'length_normalization': True if args.length_normalization is None else bool(args.length_normalization)
  }
  print(options)
  if args.top_x_traces is not None:
    options['topXTraces'] = args.top_x_traces
else:
  options = {'export': '' if args.export is None else args.export}
  if args.logFilename[-4:] == '.npz':
    args.logFilename = args.logFilename[:-4]
  discoverer = miners[args.miner](f'{args.dataDirectory}{args.logFilename}')

discoverer.discover(conformance_check=args.conformanceChecking, **options)

if args.visualize:
  discoverer.visualize()

# if args.export is not None:
#   discoverer.export(f'{args.dataDirectory}{args.export}')

'''
commands:
for filename in /home/dominique/TUe/thesis/git_data/bpmai/*.npz; do echo "${filename##*/}" && python3.6 -m project.process_mining -d /home/dominique/TUe/thesis/git_data/bpmai/ -m alpha -l ${filename##*/} && python3.6 -m project.process_mining -d /home/dominique/TUe/thesis/git_data/bpmai/ -m inductive -l ${filename##*/} && python3.6 -m project.process_mining -d /home/dominique/TUe/thesis/git_data/bpmai/ -mf /home/dominique/TUe/thesis/git/project/process_discovery_deprecated/checkpoints/checkpoint_gen_5_092.pth -m ai -l ${filename##*/}; done
'''