from project.data_handling.log import LogHandler
from project.data_handling.petrinet import PetrinetHandler
from project.ml_models.inference import Inference
from project.ml_models.model_generative import load_from_file
from project.ml_models.preprocessing import GraphBuilder
from project.ml_models.utils import print_variants
from project.evaluation.petrinets import PetrinetEvaluation

from pm4py.algo.discovery.alpha      import algorithm as alpha_miner
from pm4py.algo.discovery.heuristics import algorithm as heuristics_miner
from pm4py.algo.discovery.inductive  import algorithm as inductive_miner
# from pm4py.objects.petri.exporter    import factory as pnml_exporter
# from pm4py.visualization.petrinet    import factory as vis_factory
import numpy as np
from pprint import pprint
import json
from colorama import Fore, Style
import time
import matplotlib.pyplot as plt

class ProcessDiscovery:
  def __init__(self, fLogFilename):
    self.mLogHandler = LogHandler(fLogFilename)
    self.mNet = None
    self.mInitialMarking = None
    self.mFinalMarking = None

  def visualize(self):
    gviz = vis_factory.apply(self.mNet, self.mInitialMarking, self.mFinalMarking)
    vis_factory.view(gviz)

  def discover(self):
    raise NotImplementedError('Method to be implemented in concrete class iso this abstract class.')

  def export(self, fPetrinetName, fPetrinetNamePNG=None, fDebug=False):
    if fPetrinetNamePNG is None:
      fPetrinetNamePNG = fPetrinetName
    print(fPetrinetName)
    if self.mNet is not None:
      petrinet_handler = PetrinetHandler()
      petrinet_handler.mPetrinet = self.mNet
      petrinet_handler.mInitialMarking = self.mInitialMarking
      petrinet_handler.mFinalMarking = self.mFinalMarking

      petrinet_handler.merge_initial_final_marking()

      petrinet_handler.export(f'{fPetrinetName}.pnml')
      petrinet_handler.visualize(fDebug=fDebug, fExport=f'{fPetrinetNamePNG}.png')

  def conformance(self):
    # TODO
    return

class AIMiner(ProcessDiscovery):
  def __init__(self, fLogFilename, model_filename, embedding_size=21):
    super().__init__(None)
    self.log_filename = fLogFilename
    self.embedding_size = embedding_size
    self.include_frequency = ('frequency' in model_filename.split('/')[-1])
    self.model = load_from_file(model_filename, embedding_size, include_frequency=self.include_frequency)
    self.model_inference = Inference(self.model)

  def discover(self, export='', conformance_check=True, beam_width=1, beam_length=1, number_of_petrinets=1,  topXTraces=30, length_normalization=True):
    self.mLogHandler._importVariants(self.log_filename)
    dddd = '/'.join(self.log_filename.split('/')[:-1])
    petrinet_name = self.log_filename.split('/')[-1].split('.')[0]
    from pm4py.objects.log.importer.xes import importer as xes_importer

    percentage = 80
    variants = self.mLogHandler.getMostFrequentVariants(percentage, minimum_variants=30, maximum_variants=75)
    traces = [list(variant['variant']) for variant in variants]
    counts = [variant['count'] for variant in variants]
    print(f'{len(self.mLogHandler.mVariants)} variants in original log, taking {len(traces)}.')

    labels = [t for t in self.mLogHandler.mTransitions.keys()]

    show_log = True
    if show_log:
      log_filename = f'{self.log_filename}.xes'
      log = xes_importer.apply(log_filename)

      from pm4py.statistics.traces.log import case_statistics
      from pm4py.algo.filtering.log.variants import variants_filter

      variants_count = case_statistics.get_variant_statistics(log)
      variants_count = sorted(variants_count, key=lambda x: x['count'], reverse=True)
      traces_ = [variant['variant'] for variant in variants_count][:len(traces)]
      counts_ = [variant['count'] for variant in variants_count][:len(traces)]

      print_variants(traces_, labels, counts_)

    fake_data = False
    if fake_data:
      traces = [
        [0, 5],
        # [0],
        [0, 1, 2, 3],
        [0, 2, 1, 3],
        [0, 3, 1, 2],
        [0, 2, 3, 1],
        [0, 3, 2, 1]
      ]
      counts = [10] * len(traces)

      import string
      labels = list(string.ascii_lowercase)
      labels = labels[:max([label for trace in traces for label in trace]) + 1]

    if conformance_check:
      log_filename = f'{self.log_filename}.xes'
      log = xes_importer.apply(log_filename)

      from pm4py.statistics.traces.log import case_statistics
      from pm4py.algo.filtering.log.variants import variants_filter

      variants_count = case_statistics.get_variant_statistics(log)
      variants_count = sorted(variants_count, key=lambda x: x['count'], reverse=True)
      variants = [variant['variant'] for variant in variants_count][:topXTraces]
      [print(variant) for variant in variants]
      log_top_x = variants_filter.apply(log, variants)

    transitionLabels = ['>'] + labels + ['|']
    if len(transitionLabels) > self.embedding_size:
      print('=' * 60, 'too many transitions')
      return
    name = self.mLogHandler.mLogFilename.split('/')[-1].split('.')[0]

    start_construction_time = time.time()
    pb = GraphBuilder(name, self.embedding_size, traces, counts, transitionLabels, fDepth=1, include_frequency=self.include_frequency, fPetrinetHandler=None)
    # print(f'Graph construction took: {time.time() - start_construction_time:.3f} seconds')
    print('number of nodes', pb.mNet.number_of_nodes())
    print('number of candidate places', len(pb.mPossiblePlaces))

    # results = self.model_inference.predict(pb, beam_width=1, beam_length=1, max_number_of_places=40, number_of_petrinets=1)
    results = self.model_inference.predict(pb, beam_width=beam_width, beam_length=beam_length, max_number_of_places=100, number_of_petrinets=number_of_petrinets, length_normalization=length_normalization)

    # Translate place-sets to petrinets
    for i, result in enumerate(results):
      petrinet_handler = PetrinetHandler()
      print(f'Discovered {len(result["places"])} places and {len(result["silent_transitions"])} silent transitions '
            f'({len(result["places"]) + len(result["silent_transitions"])})')
      order_labels = [str(i) for i in range(len(result['places']))]
      choice_prob_labels = [f'{v.tolist():.2f}' for v in result['choice_probabilities']]
      stop_prob_labels = [f'{v.tolist()[0][0]:.2f}' for v in result['add_probabilities']]
      place_labels = [f'{v1}\nc{v2}\na{v3}' for v1, v2, v3 in zip(order_labels, choice_prob_labels, stop_prob_labels)]

      petrinet_handler.fromPlaces(result['places'], transitionLabels, place_labels, fSilentTransitions=result['silent_transitions'])

      petrinet_handler.visualize(fDebug=False)
      self.mNet, self.mInitialMarking, self.mFinalMarking = petrinet_handler.get()

      if export != '':
        prob = f'{result["probability"]:.4f}'.split('.')[-1]
        if len(results) > 1:
          self.export(f'{dddd}/predictions/{petrinet_name}_gcn_{export}_{i}', f'{dddd}/predictions/pngs/{petrinet_name}_gcn_{export}_{i}')
          self.export(f'{dddd}/predictions/{petrinet_name}_gcn_{export}_{i}', f'{dddd}/predictions/pngs/{petrinet_name}_gcn_{export}_{i}_{prob}', fDebug=True)
        else:
          self.export(f'{dddd}/predictions/{petrinet_name}_gcn_{export}', f'{dddd}/predictions/pngs/{petrinet_name}_gcn_{export}')
          self.export(f'{dddd}/predictions/{petrinet_name}_gcn_{export}', f'{dddd}/predictions/pngs/{petrinet_name}_gcn_{export}_{prob}', fDebug=True)

      if conformance_check:
        petrinet_evaluation = PetrinetEvaluation(self.mNet, self.mInitialMarking, self.mFinalMarking)
        petrinet_evaluation.remove_start_end_transitions()
        result = petrinet_evaluation.conformance(log)
        print('FULL:')
        pprint(self.print_conformance(result))
        result_top_x = petrinet_evaluation.conformance(log_top_x)
        print(f'Top {topXTraces}:')
        pprint(self.print_conformance(result_top_x))

        if export != '':
          if len(results) > 1:
            with open(f'{dddd}/predictions/{petrinet_name}_gcn_{export}_{i}_cc.txt', 'w') as cc_file:
              cc_file.write('FULL\n')
              cc_file.write(json.dumps(self.print_conformance(result), sort_keys=True, indent=2, separators=(',', ': ')))
              cc_file.write(f'\nTop {topXTraces}:\n')
              cc_file.write(json.dumps(self.print_conformance(result_top_x), sort_keys=True, indent=2, separators=(',', ': ')))
          else:
            with open(f'{dddd}/predictions/{petrinet_name}_gcn_{export}_cc.txt', 'w') as cc_file:
              cc_file.write('FULL\n')
              cc_file.write(json.dumps(self.print_conformance(result), sort_keys=True, indent=2, separators=(',', ': ')))
              cc_file.write(f'\nTop {topXTraces}:\n')
              cc_file.write(json.dumps(self.print_conformance(result_top_x), sort_keys=True, indent=2, separators=(',', ': ')))


  def print_conformance(self, result):
    scores = {
      'fitness': f'{result["fitness"]["log_fitness"]:.3f}',
      'precision': f'{result["precision"]:.3f}',
      'generalization': f'{result["generalization"]:.3f}',
      'fscore': f'{result["fscore"]:.3f}',
      'metricsAverageWeight': f'{result["metricsAverageWeight"]:.3f}',
      'simplicity': f'{result["simplicity"]:.3f}',
    }
    return scores

class AlphaMiner(ProcessDiscovery):
  def __init__(self, fLogFilename):
    super().__init__(fLogFilename)
    self.log_filename = fLogFilename

  def discover(self, export='', conformance_check=True):
    self.mNet, self.mInitialMarking, self.mFinalMarking = alpha_miner.apply(self.mLogHandler.mLog)
    dddd = '/'.join(self.log_filename.split('/')[:-1])
    petrinet_name = self.log_filename.split('/')[-1].split('.')[0]
    if export != '':
      self.export(f'{dddd}/predictions/{petrinet_name}_alpha')

class HeuristicsMiner(ProcessDiscovery):
  def __init__(self, fLogFilename):
    super().__init__(fLogFilename)
    self.log_filename = fLogFilename

  def discover(self, conformance_check=False, export=''):
    self.mNet, self.mInitialMarking, self.mFinalMarking = heuristics_miner.apply(self.mLogHandler.mLog)
    dddd = '/'.join(self.log_filename.split('/')[:-1])
    petrinet_name = self.log_filename.split('/')[-1].split('.')[0]

    # if export != '':
    self.export(f'{dddd}/predictions/{petrinet_name}_heuristics')

class InductiveMiner(ProcessDiscovery):
  def __init__(self, fLogFilename):
    super().__init__(fLogFilename)
    self.log_filename = fLogFilename

  def discover(self, conformance_check=False, export=''):
    self.mNet, self.mInitialMarking, self.mFinalMarking = inductive_miner.apply(self.mLogHandler.mLog)
    dddd = '/'.join(self.log_filename.split('/')[:-1])
    petrinet_name = self.log_filename.split('/')[-1].split('.')[0]
    self.export(f'{dddd}/predictions/{petrinet_name}_inductive')
