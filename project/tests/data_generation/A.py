import unittest

class x:
  def __init__(self):
    self.x = 0

class TestCase1(unittest.TestCase):
  def setUp(self):
    self.mX = x()

  def tearDown(self):
    self.mX = None

  def test1(self):
    self.assertEqual(self.mX.x, 0, 'good')

  def test2(self):
    self.assertEqual(self.mX.x, 0, 'yeah boi')

class ATestSuite(unittest.TestSuite):
  def __init__(self):
    unittest.TestSuite.__init__(self, map(TestCase1, ('test1', 'test2')))
