import argparse
import numpy as np

from project.ml_models.preprocessing import get_data
from project.ml_models.model_generative import GenerativeModel, load_from_file
from project.ml_models.training import Training

embedding_size = 21
embedding_strategy = 'onehot'

logPrefix      = '/home/dominique/TUe/thesis/git_data/process_trees_medium_ws2/logs_compressed/'
petrinetPrefix = '/home/dominique/TUe/thesis/git_data/process_trees_medium_ws2/petrinets/'

modelname = 'model_candidates_frequency'
include_frequency = 'frequency' in modelname

data_options = {
  'fDepth': 1,
  'fLogPrefix': logPrefix,
  'fEmbeddingSize': embedding_size,
  'fVisualize': False,
  'prepare_petrinet': True,
  'fPetrinetPrefix': petrinetPrefix,
  'embedding_strategy': 'onehot',
  'include_frequency': include_frequency,
  # 'maximum_number_of_places': 350,
  # 'maximum_number_of_places': 1000,
  # 'maximum_number_of_transitions': 10
}

# train_graphs = get_data(range(400),      **data_options)
# test_graphs =  get_data(range(400, 550), **data_options)

train_graphs = get_data(range(2000), **data_options)
print('NUMBER OF TRAIN GRAPHS:', len(train_graphs.keys()))
test_graphs = get_data(range(2000, 2663), **data_options)
print('NUMBER OF TEST GRAPHS:', len(test_graphs.keys()))

test_graphs = {}

# model = GenerativeModel(embedding_size, include_frequency=include_frequency, graph_embedding_type='candidates')
model_filename = f'/home/dominique/TUe/thesis/git/project/ml_models/models/model_candidates_frequency_025.pth'
# model = load_from_file('/home/dominique/TUe/thesis/git/project/ml_models/models/model_candidates_015.pth', embedding_size, include_frequency=include_frequency)
model = load_from_file(model_filename, embedding_size, include_frequency=include_frequency)

training = Training(model)
training.train_generative(train_graphs, number_of_epochs=250, test_pbs=test_graphs, dont_train=False,
                          save_statistics=f'/home/dominique/TUe/thesis/git/project/ml_models/checkpoints/{modelname}',
                          export_prediction=False, start_evaluation_epoch=100, start_save_epoch=1)
