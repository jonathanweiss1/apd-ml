import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import networkx as nx
import dgl
import json
from colorama import Style, Fore, Back

def analyze_attentions(edges, attentions, axes=plt, name='', forward=True, uniform=False, bins=None):
  sources, destinations, edgeids = edges.edges()
  # attentions_scaled = F.softmax(attentions, dim=0)
  source_features = edges.src
  destination_features = edges.dst
  edge_directions = edges.data['direction']
  graph = dgl.DGLGraph()
  graph.add_nodes(max(sources).item() + 1)
  for index, (source, destination, attention) in enumerate(zip(sources, destinations, attentions)):
    if edge_directions[index][0][0] == forward:
      graph.add_edge(source, destination)
      graph.edges[graph.number_of_edges() - 1].data['attention'] = attention
      graph.nodes[source].data['trace_id'] = source_features['trace_id'][index]
      graph.nodes[source].data['label'] = source_features['label'][index]
      graph.nodes[destination].data['trace_id'] = destination_features['trace_id'][index]
      graph.nodes[destination].data['label'] = destination_features['label'][index]

  show_graph = True
  if show_graph:
    plt.style.use('ggplot')
    fig, ax = plt.subplots(figsize=(10, 6), constrained_layout=True)

    dcolors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    nx_G = graph.to_networkx()
    # pos = nx.kamada_kawai_layout(nx_G)
    pos = nx.spring_layout(nx_G)
    colors = []
    for i in range(len(graph.nodes)):
      node = graph.nodes[i]
      if node.data['trace_id'].item() >= 0:
        colors.append(dcolors[0] + '88')  # trace node
      elif node.data['label'].item() == 0:
        colors.append(dcolors[1] + '88')  # place node
      else:
        colors.append(dcolors[2] + '88')  # petrinet transition node

    attentions_normalized = softmax(attentions.data.numpy().flatten())
    print(f'Max attention: {max(attentions_normalized)}')
    print(f'Mean attention: {np.mean(attentions_normalized)}')
    print(f'Min attention: {min(attentions_normalized)}')

    cmap = plt.cm.Reds #plt.cm.coolwarm
    edge_colors = cmap(attentions_normalized / max(attentions_normalized))
    nx.draw(nx_G, pos, axes=ax, node_size=75, with_labels=False, node_color=colors, node_apha=0.5, edge_color=edge_colors, width=2)
    ax.plot([10, 10], [10, 10], marker='o', linestyle='', color=dcolors[0], label='trace')
    ax.plot([10, 10], [10, 10], marker='o', linestyle='', color=dcolors[1], label='place')
    ax.plot([10, 10], [10, 10], marker='o', linestyle='', color=dcolors[2], label='transition')

    # x, y = np.mgrid[0:5, 0:5]
    x = np.linspace(5, 6.12, 11)
    # z = np.sin(x ** 2 + y ** 2)
    mesh = ax.pcolormesh(x, x, np.array([x - 5] * 11), cmap=cmap)
    cbar = plt.colorbar(mesh)

    ax.set_xlim(-1.1, 1.1)
    ax.set_ylim(-1.1, 1.1)

    plt.legend()

    print(name, forward)
    if name != 'uniform':
      with open(f'/home/dominique/TUe/thesis/report/attention_figures/__gat_1_{"_".join(name.split(" ")[:-1])}_{"for" if forward else "back"}ward.json', 'w') as jsonFile:
        json.dump({'attentions': list([float(v) for v in attentions_normalized]), 'name': name, 'forward': forward}, jsonFile, sort_keys=True, indent=2)
      fig.savefig(f'/home/dominique/TUe/thesis/report/attention_figures/__gat_1_{"_".join(name.split(" ")[:-1])}_{"for" if forward else "back"}ward.pdf')
      fig.savefig(f'/home/dominique/TUe/thesis/report/attention_figures/__gat_1_{"_".join(name.split(" ")[:-1])}_{"for" if forward else "back"}ward.png')

    plt.close(fig)
    # plt.show()


  show_histogram = False
  if show_histogram:
    source_as = defaultdict(list)
    for source, destination, edgeid, attention in zip(sources, destinations, edgeids, attentions):
      if graph.nodes[destination.item()].data['trace_id'] < 0:
        attention = attention[0].data.item()
        source_as[destination.item()].append(attention)

    entropies = []
    entropies_uniform = []
    for source, attentionones in source_as.items():
      attentionones_normalized = softmax(attentionones)
      uniform_attention = 1 / len(attentionones_normalized)
      entropies.append(-np.sum([a * np.log(a) for a in attentionones_normalized]))
      entropies_uniform.append(-np.sum([uniform_attention * np.log(uniform_attention) for a in attentionones_normalized]))
    # axes.set_title(f'{name}_{"forward" if forward else "backward"}')
    axes.set_title(name)
    label = f'{"forward" if forward else "backward"} edges'

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    bar_width = (bins[1] - bins[0]) / 2
    axes.set_ylabel('# nodes')
    axes.set_xlabel('entropy')
    if uniform:
      axes.hist(entropies_uniform, alpha=0.5, label=label, bins=bins, edgecolor='black', width=bar_width, color=colors[int(not forward)], zorder=int(forward) + 1, align='mid' if forward else 'right')
      # axes.hist(entropies_uniform, alpha=0.5, label=label, bins=bins, edgecolor='black', color=colors[int(not forward)], zorder=int(forward) + 1)
      axes.legend()
    else:
      axes.hist(entropies, alpha=0.5, label=label, bins=bins, edgecolor='black', width=bar_width, color=colors[int(not forward)], zorder=int(forward) + 1, align='mid' if forward else 'right')
      # axes.hist(entropies, alpha=0.5, label=label, bins=bins, edgecolor='black', color=colors[int(not forward)], zorder=int(forward) + 1)
    # plt.show()
    # plt.savefig(f'/home/dominique/attentions/{name}_{"forward" if forward else "backward"}')
    # plt.close()

def softmax(xx):
  return np.exp(xx) / np.sum(np.exp(xx), axis=0)


def print_variants(variants, labels, counts=None):
  if counts is None:
    counts = [''] * len(variants)

  colors = [Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.CYAN, Fore.WHITE, Fore.LIGHTRED_EX,
            Fore.LIGHTGREEN_EX, Fore.LIGHTYELLOW_EX, Fore.LIGHTBLUE_EX, Fore.LIGHTMAGENTA_EX, Fore.LIGHTCYAN_EX, Fore.LIGHTWHITE_EX]
  colors = [Back.RED, Fore.GREEN, Back.YELLOW, Back.BLUE, Back.MAGENTA, Back.CYAN, Back.WHITE, Back.LIGHTRED_EX,
            Back.LIGHTGREEN_EX, Back.LIGHTYELLOW_EX, Back.LIGHTBLUE_EX, Back.LIGHTMAGENTA_EX, Back.LIGHTCYAN_EX, Back.LIGHTWHITE_EX]

  for variant, count in zip(variants, counts):
    variant_string = ''
    for event in variant.split(','):
      variant_string += f'{colors[labels.index(event) % len(colors)]}{event}{Style.RESET_ALL},'
    print(f'{variant_string} {count}')
