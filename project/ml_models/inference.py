from project.data_handling.petrinet import PetrinetHandler

import torch as th
from colorama import Fore, Style
import time
import numpy as np
from tqdm import tqdm

class Inference:
  def __init__(self, model):
    self.model = model
    self.model.eval()
    self.model.training = False

  def evaluation_string(self, true_places, predicted_places):
    true_places = sorted(true_places)
    string = ''
    for true_place in true_places:
      if true_place in predicted_places:
        string += Fore.CYAN
      string += f'{true_place}{Style.RESET_ALL} '
    string += Fore.RED
    for predicted_place in predicted_places:
      if predicted_place not in true_places:
        string += f'{predicted_place} '

    return string + Style.RESET_ALL

  def getTree(self, top, depth, routes=None):
    if routes is None or len(routes) == 0:
      routes = [[i] for i in range(top)]
    if len(routes[0]) == depth:
      return routes
    return self.getTree(top, depth, [route + [t] for route in routes for t in range(top)])

  def check_prefixes(self, route, prefixes):
    for prefix in prefixes:
      if route[:len(prefix)] == prefix:
        return True
    return False

  def export_prediction(self, target_places, predicted_places, transition_labels, traces=None, fFilename=''):
    pnetHandler = PetrinetHandler()
    pnetHandler.fromPlaces(predicted_places, transition_labels,
                           fPlaceLabels=['' if p in target_places else '1' for p in predicted_places])
    pnetHandler.fromPlaces(target_places, transition_labels,
                           fPlaceLabels=['' if p in predicted_places else '0' for p in target_places])
    if traces is not None:
      startPlace = pnetHandler.addPlace('>')
      pnetHandler.mInitialMarking[startPlace] += 1
      endPlace = pnetHandler.addPlace('|')
      pnetHandler.mFinalMarking[endPlace] += 1
      maxLength = len(max(traces, key=lambda x: len(x)))
      filler_label = len(transition_labels)
      for trace in traces:
        trace += [filler_label] * (maxLength - len(trace))
        currentNode = startPlace
        for index, transition in enumerate(trace):
          letter = transition_labels[transition + 1] if transition < len(transition_labels) else ''
          nextNode = pnetHandler.addTransition(letter) if index % 2 == 0 else pnetHandler.addPlace(letter)
          pnetHandler.addArc(currentNode, nextNode)
          currentNode = nextNode
        if len(trace) % 2 == 0:
          nextNode = pnetHandler.addTransition('')
          pnetHandler.addArc(currentNode, nextNode)
          currentNode = nextNode
        pnetHandler.addArc(currentNode, endPlace)
    pnetHandler.visualize(fExport=fFilename, fDebug=True)

  def predict(self, pb, beam_width=3, beam_length=6, max_number_of_places=20, number_of_petrinets=1, length_normalization=False):
    graph = pb.mNet
    probs = [{'prob': -np.inf}] * number_of_petrinets
    all_places = [str((set(sorted(input)), set(sorted(output)))) for input, output in pb.mPossiblePlaces.keys()]

    if beam_width is None:
      routes = [None]
    else:
      routes = sorted(self.getTree(beam_width, beam_length))
      routes = [route + [0] * (max_number_of_places - beam_length) for route in routes]
    prefixes = []

    original_graph_size = graph.number_of_nodes()

    for route in (tqdm(routes) if len(routes) > 1 else routes):
      if self.check_prefixes(route, prefixes):
        continue
      with th.no_grad():
        predicted_place_indices_try, prob_try, pruned_prefix, choice_probs, add_probs = \
          self.model(graph, pb.mFeatures, len(pb.mPossiblePlaces), pb.mAlphaRelations, route=route, prune=probs[-1]['prob'], max_number_of_places=max_number_of_places, length_normalization=length_normalization)
      prob_try = prob_try.item()
      self.model.clean_graph(graph, original_graph_size)

      if pruned_prefix is not None:
        prefixes.append(pruned_prefix)
      if prob_try > probs[-1]['prob']:
        probs[-1] = {
          'prob': prob_try,
          'predicted_place_indices': predicted_place_indices_try,
          'route': ''.join([str(r) for r in route]) if route is not None else None,
          'choice_probs': choice_probs,
          'add_probs': add_probs
        }
        probs = sorted(probs, reverse=True, key=lambda x: x['prob'])

    number_of_places = len(all_places)

    results = []
    for result in probs:
      if 'predicted_place_indices' not in result:
        results.append({'probability': result['prob'], 'places': [], 'silent_transitions': [], 'choice_probabilities': [], 'add_probabilities': [], 'predicted_place_indices': [] })
        print('Something went wrong, "predicted_place_indices" not found in result.')
        continue
      places = [all_places[i] for i in result['predicted_place_indices'] if i < number_of_places]
      place_indices = [i for i, v in enumerate(result['predicted_place_indices']) if v < number_of_places]
      silent_transition_indices =  [i - number_of_places for i in result['predicted_place_indices'] if i >= number_of_places]
      silent_transitions = self.find_silent_transitions(places, silent_transition_indices)
      result['choice_probs'] = [choice_prob for index, choice_prob in enumerate(result['choice_probs']) if index in place_indices]
      result['add_probs']    = [choice_prob for index, choice_prob in enumerate(result['add_probs']) if index in place_indices] + [result['add_probs'][-1]]
      results.append({
        'probability': result['prob'],
        'places': places,
        'silent_transitions': silent_transitions,
        'choice_probabilities': result['choice_probs'],
        'add_probabilities': result['add_probs'],
        'predicted_place_indices': result['predicted_place_indices']
      })

    return results

  def find_silent_transitions(self, places, silent_transition_indices):
    if len(silent_transition_indices) == 0:
      return []
    silent_transitions = []
    added_per_place = [(i - 1)*2 for i in range(1, len(places) + 1)]
    added_total_per_place = [sum(added_per_place[:i + 1]) for i in range(len(places))]
    connections = sum([list(range(i)) for i in range(1, len(places) + 1)], [])
    for silent_transition_index in silent_transition_indices:
      connection_1, count = next(((i, c) for i, c in enumerate(added_total_per_place) if c > silent_transition_index), (None, None))
      connection_2 = connections[int(silent_transition_index / 2)]
      s = (places[connection_2], places[connection_1]) if (silent_transition_index % 2 == 0) else (places[connection_1], places[connection_2])
      silent_transitions.append(s)
    return silent_transitions

  def evaluate(self, pb, beam_width=3, beam_length=6, max_number_of_places=20, number_of_petrinets=1):
    start_time = time.time()
    graph = pb.mNet
    places = [i for i, p in enumerate(pb.mTarget) if p == 1]
    probs = [{'prob': 0}] * number_of_petrinets

    all_places = [str((set(sorted(input)), set(sorted(output)))) for input, output in pb.mPossiblePlaces.keys()]

    routes = sorted(self.getTree(beam_width, beam_length))
    routes = [route + [0] * (max_number_of_places - beam_length) for route in routes]
    prefixes = []
    for route in routes:
      if self.check_prefixes(route, prefixes):
        continue
      predicted_place_indices_try, prob_try, pruned_prefix = \
        self.model(graph, pb.mFeatures, len(pb.mTarget), route=route, prune=probs[-1]['prob'], max_number_of_places=max_number_of_places)
      if pruned_prefix is not None:
        prefixes.append(pruned_prefix)
      if prob_try > probs[-1]['prob']:
        probs[-1] = {
          'prob': prob_try.item(),
          'predicted_place_indices': predicted_place_indices_try,
          'route': ''.join([str(r) for r in route])}
        probs = sorted(probs, reverse=True, key=lambda x: x['prob'])

    for i in range(len(probs)):
      correct = set(places) == set(probs[i]['predicted_place_indices'])
      correct_with_fps = len(set(places) - set(probs[i]['predicted_place_indices'])) == 0
      predicted_places = [all_places[i] for i in probs[i]['predicted_place_indices']]
      evaluation_string = self.evaluation_string(pb.mTargetPlaces, predicted_places)
      print(f'{Fore.CYAN if correct else Fore.GREEN if correct_with_fps else ""} ({pb.mName:>7}) ({probs[i]["prob"]:.4f})'
            f'{evaluation_string} {Style.RESET_ALL if correct else ""}({time.time() - start_time:.1f}s) {probs[i]["route"]}')

    return probs
