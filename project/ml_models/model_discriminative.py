import torch as th
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import random
import tqdm

from project.ml_models.layers import MultiHeadGATLayer
from project.ml_models.preprocessing import onehot

class GATNetwork(nn.Module):
  def __init__(self, in_dim, hidden_dims, out_dim, num_heads):
    super().__init__()
    self.embedding_size = in_dim
    self.number_of_layers = len(hidden_dims) + 1
    self.layers = nn.ModuleList([MultiHeadGATLayer(in_dim, hidden_dims[0], num_heads)])
    for layer_in_dim, layer_out_dim in zip(hidden_dims[:-1], hidden_dims[1:]):
      self.layers.append(MultiHeadGATLayer(layer_in_dim * num_heads, layer_out_dim, num_heads))
    self.layers.append(MultiHeadGATLayer(hidden_dims[2] * num_heads, out_dim, 1))

  def forward(self, graph, hidden_state):
    for index, layer in enumerate(self.layers):
      hidden_state = layer(graph, hidden_state)
      if index < (self.number_of_layers - 1):
        hidden_state = F.relu(hidden_state)
    return hidden_state

class DiscriminativeModel:
  def __init__(self, fModel, fOptimizer):
    self.mModel = fModel
    self.mOptimizer = fOptimizer

  def trainEpoch(self, epoch, builders, training=True, printResult=False):
    nBuilders = len(builders)
    correctCount = 0
    totalNumberOfPlaces = 0
    totalNumberOfCorrectPlaces = 0
    totalNumberOfFalsePositives = 0
    for i, pb in enumerate(builders):
      # if epoch < nEpochs-1 and i == len(builders)-1:
      #   continue
      g = pb.mNet
      target = th.FloatTensor(np.array(pb.mTarget).reshape(len(pb.mTarget), 1))
      features = th.zeros((len(g.nodes), self.mModel.embedding_size))
      for nodeIndex in range(len(g.nodes)):
        nodeLabel = int(g.nodes[nodeIndex].data['label'].item())
        # TODO move feature creation to preprocessing.
        features[nodeIndex] = onehot(nodeLabel, self.mModel.embedding_size)

      logits = self.mModel(g, features)

      loss = F.binary_cross_entropy_with_logits(logits[-len(target):], target)
      predictions = th.round(th.sigmoid(logits[-len(target):].flatten())).data.numpy()
      targetPlaces = target.flatten().data.numpy()
      numberOfPlaces = int(np.sum(targetPlaces))
      totalNumberOfPlaces += numberOfPlaces
      numberOfCorrect = len(np.where((targetPlaces + predictions) == 2)[0])
      totalNumberOfCorrectPlaces += numberOfCorrect
      numberOfFalsePositive = len(np.where((targetPlaces - predictions) == -1)[0])
      totalNumberOfFalsePositives += numberOfFalsePositive
      c = False
      if numberOfCorrect == numberOfPlaces and numberOfFalsePositive == 0:
        c = True
      correctCount += 1
      fc = numberOfCorrect == numberOfPlaces
      printString = f'epoch {epoch:>3}: ({i:>4}/{nBuilders}) {Fore.CYAN if c else ""}{loss.data:.5f}  ({loss.data * 100:>5.1f}% '
      printString += f'{Fore.GREEN if fc and not c else ""}({numberOfCorrect}/{numberOfPlaces} correct) {Style.RESET_ALL if not c else ""} '
      printString += f'{Fore.GREEN if numberOfFalsePositive == 0 and not c else ""}({numberOfFalsePositive} false positive){Style.RESET_ALL}'
      print(printString)

      if printResult:
        print(logits[-len(target):].flatten().data)
      print(predictions)
      print(targetPlaces)

      placeIndicesPred = [i for i, p in enumerate(predictions) if p == 1]
      placeIndices = [i for i, p in enumerate(targetPlaces) if p == 1]

      print('target:')
      print([pb.mPossiblePlaces[i] for i in placeIndices])
      print('predicted:')
      print([pb.mPossiblePlaces[i] for i in placeIndicesPred])

      if training:
        self.mOptimizer.zero_grad()
        loss.backward()
        self.mOptimizer.step()
    print(f'Classified {correctCount}/{nBuilders} ({correctCount/nBuilders*100:.2f}%) graphs, {totalNumberOfCorrectPlaces}/{totalNumberOfPlaces} ({totalNumberOfCorrectPlaces/totalNumberOfPlaces*100:.2f}%) places, {totalNumberOfFalsePositives} false positives')
    return totalNumberOfCorrectPlaces / totalNumberOfPlaces


  def train(self, trainBuilders, testBuilders=[], shuffle=True):
    self.mModel.train()
    nEpochs = 500
    bestTrainEpoch = 0
    bestTrainAccuracy = 0
    bestTestEpoch = 0
    bestTestAccuracy = 0
    try:
      for epoch in tqdm.trange(nEpochs):
        print(epoch, '-' * 50)
        print('training')

        bs = trainBuilders[:]
        random.shuffle(bs)
        acc = self.trainEpoch(epoch, bs, printResult=(epoch == nEpochs - 1))
        if acc > bestTrainAccuracy:
          bestTrainAccuracy = acc
          bestTrainEpoch = epoch
        print(f'Best train epoch: {bestTrainEpoch}')
        th.save({'state_dict': self.mModel.state_dict()}, f'checkpoints/checkpoint_{epoch:03d}.pth')
        print('testing')
        testAcc = self.trainEpoch(epoch, testBuilders, training=False, printResult=(epoch == nEpochs - 1))
        if testAcc > bestTestAccuracy:
          bestTestAccuracy = testAcc
          bestTestEpoch = epoch
        print(f'Best test epoch: {bestTestEpoch}')
    except KeyboardInterrupt:
      print('training')
      self.trainEpoch(-1, trainBuilders, printResult=True)
      print('testing')
      self.trainEpoch(-1, testBuilders, training=False, printResult=True)
