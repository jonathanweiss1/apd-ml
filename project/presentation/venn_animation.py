from manimlib.imports import *
from collections import defaultdict
import random
from itertools import permutations

from project.presentation.svg_petrinet import SVGPetrinet
from project.presentation.smallest_circle import make_circle

def get_location(trace, center, radius):
  locations = defaultdict(lambda: [2*math.pi*random.random(), random.random() * radius])
  locations['O,P,A,S'] = [0, radius * 0.5]
  locations['O,A,P,S'] = [0.25*2*math.pi, radius * 0.5]
  locations['O,P,A,C'] = [0.5 * 2 * math.pi, radius * 0.5]
  locations['O,A,P,C'] = [0.75 * 2 * math.pi, radius * 0.5]
  locations['O,A,C,P'] = [0.125 * 2 * math.pi, radius * 0.8]
  locations['O,A,P,P,S'] = [0.875 * 2 * math.pi, radius * 0.8]

  return [locations[trace][1] * math.cos(locations[trace][0]) + center[0], locations[trace][1] * math.sin(locations[trace][0]) + center[1], 0]

class Venn:
  def __init__(self, color, initial_position, name='', initial_radius=1.5, striped=False):
    self.color = color
    self.name = name
    self.initial_position = initial_position
    self.fill_opacity = 0.15
    if striped:
      self.circle = DashedVMobject(Circle(color=color, radius=initial_radius, fill_opacity=self.fill_opacity))
    else:
      self.circle = Circle(color=color, radius=initial_radius, fill_opacity=self.fill_opacity)
    self.move_to(initial_position)
    self.reset_trace_variants()
    self.radius = initial_radius

  def reset_trace_variants(self):
    self.trace_variants = defaultdict(lambda: {'count': 0, 'dot': None, 'text': None})

  def fade_out_traces(self):
    fade_out_animations = []
    for trace in self.trace_variants.values():
      fade_out_animations.extend([FadeOut(trace['dot']), FadeOut(trace['text']), FadeOut(trace['trace'].text)])
    return fade_out_animations

  def add_trace(self, trace, trace_key, venn=None, radius=0.12):
    dot_fade_in_animations = []
    dot_move_animations = []
    start_position = trace.text.get_edge_center(RIGHT)
    if venn is None:
      venn = self
    if self.trace_variants[trace_key]['count'] != 0:
      old_text = self.trace_variants[trace_key]['text']
      text = TexMobject(self.trace_variants[trace_key]['count'] + 1)
      text.scale(0.5)
      text.move_to(old_text.get_center())
      return None, [Transform(old_text, text)]
    else:
      text = TexMobject(trace.frequency if trace.frequency > 1 else '')
      text.scale(0.5)
      dot = Circle(color=self.color, radius=radius)
      text.dot = dot
      dot.move_to(start_position)
      text.add_updater(lambda m: m.move_to(m.dot.get_center() + 0.12 * UP + 0.1 * RIGHT, aligned_edge=LEFT))
      dot_fade_in_animations.extend([FadeIn(dot), FadeIn(text)])
      dot_move_animations.extend([dot.move_to, self.get_random_point_in_circle(venn, trace_key=trace_key, seed=hash(trace_key)+1)])
      self.trace_variants[trace_key]['count'] += trace.frequency
      self.trace_variants[trace_key]['dot'] = dot
      self.trace_variants[trace_key]['text'] = text
      self.trace_variants[trace_key]['trace'] = trace
      return dot_fade_in_animations, dot_move_animations

  def update_venn(self):
    if len(self.trace_variants.keys()) == 0:
      return self.change_radius(0, new_position=self.initial_position)
    radius = [point['dot'].radius for point in self.trace_variants.values()][0]
    positions = [point['dot'].get_center()[:2] for point in self.trace_variants.values()]
    new_circle = make_circle(positions)
    return self.change_radius(new_circle[2]+radius, new_position=[*new_circle[:2], 0])

  def get_random_point_in_circle(self, venn, trace_key=None, seed=None):
    if trace_key is not None:
      return get_location(trace_key, venn.circle.get_center(), venn.radius)
    if seed is not None:
      random.seed(seed)
    alpha = 2 * math.pi * random.random()
    r = venn.radius * math.sqrt(random.random())
    return [r * math.cos(alpha) + venn.circle.get_center()[0], r * math.sin(alpha) + venn.circle.get_center()[1], 0]

  def move_to(self, position, animate=False):
    if animate:
      return [self.circle.move_to, position]
    else:
      self.circle.move_to(position)

  def fade_in(self):
    return FadeIn(self.circle)

  def get_position(self):
    return self.circle.get_center()

  def change_radius(self, new_radius, new_position=None):
    self.radius = new_radius
    new_circle = Circle(color=self.color, radius=new_radius, fill_opacity=self.fill_opacity)
    if new_position is None:
      new_circle.move_to(self.circle.get_center())
    else:
      new_circle.move_to(new_position)
    self.circle.target = new_circle
    return MoveToTarget(self.circle)

class ProcessInstance:
  def __init__(self, trace, frequency=1, initial_position=3*UP, scale=1.):
    self.trace = trace
    self.key = ','.join([v[0] for v in trace])
    self.text = TexMobject(f'\\left<{", ".join(trace)}\\right>' + '^{' + str(frequency) + '}')
    self.text.scale(scale)
    self.text.move_to(initial_position, aligned_edge=LEFT)
    self.frequency = frequency

  def get_position(self):
    return self.text.get_center()

  def write(self):
    return Write(self.text)
  def fade_in(self):
    return FadeIn(self.text)
  def fade_out(self):
    return FadeOut(self.text)
  def fade_word_for_word(self):
    return AddTextWordByWord(self.text, run_time=2.0)

class ProcessModel:
  def __init__(self, position, filename, name='', size=0.5):
    self.original_size = size
    self.scaled = 1
    self.name = name
    self.position = position
    self.initialize(filename)

  def show(self, fade=True):
    if fade:
      return FadeIn(self.body)
    else:
      return ShowCreation(self.body)

  def scale(self, factor):
    self.scaled = factor
    return [self.body.scale, factor]

  def move_to(self, position, animate=False):
    if animate:
      return [self.body.move_to, position]
    self.body.move_to(position)

  def initialize(self, filename):
    petrinet = SVGPetrinet(filename)
    petrinet.scale(self.original_size)
    petrinet.move_to(self.position)
    if self.name != '':
      text = TexMobject(self.name)
      text.move_to(self.position + self.original_size*2*UP)
      self.body = VGroup(petrinet, text)
      self.body.name = text
    else:
      self.body = VGroup(petrinet)
    self.body.stickman = petrinet

class VennAnimation:
  def __init__(self):
    self.log = [
      [['Order', 'Pay', 'Assemble', 'Send'], 24],
      [['Order', 'Assemble', 'Pay', 'Send'], 21],
      [['Order', 'Pay', 'Assemble', 'Collect'], 16],
      [['Order', 'Assemble', 'Pay', 'Collect'], 13]
    ]
    self.model_log = []

  def animate(self, scene):
    self.initialize_venns()
    self.show_venns(scene)

    process_instances = []
    log_start_position = 8.5*LEFT + 4.5 * UP
    for index, trace in enumerate(self.log):
      process_instances.append(ProcessInstance(trace[0], frequency=trace[1], initial_position=log_start_position + (index+1)*0.55*DOWN, scale=0.8))
    log_text = TexMobject('Event~log:')
    log_text.scale(0.8)
    log_text.move_to(log_start_position, aligned_edge=LEFT)
    scene.play(FadeIn(log_text), *[trace.fade_in() for trace in process_instances])

    for process_instance in process_instances:
      fade_in, move = self.log_venn.add_trace(process_instance, process_instance.key, venn=self.system_venn)
      if fade_in is not None: scene.play(*fade_in)
      scene.play(*move)
      scene.play(self.log_venn.update_venn())

    self.log.append([['Order', 'Assemble', 'Collect', 'Pay'], 1])
    for index, trace in enumerate(self.log[-1:]):
      process_instances.append(ProcessInstance(trace[0], frequency=trace[1], initial_position=log_start_position + (index + 5) * 0.55 * DOWN, scale=0.8))
    scene.play(*[trace.fade_in() for trace in process_instances[-1:]])

    for process_instance in process_instances[-1:]:
      fade_in, move = self.log_venn.add_trace(process_instance, process_instance.key, venn=self.system_venn)
      if fade_in is not None: scene.play(*fade_in)
      scene.play(*move)
      scene.play(self.log_venn.update_venn())

    self.log.append([['Order', 'Assemble', 'Pay', 'Pay', 'Send'], 1])
    for index, trace in enumerate(self.log[-1:]):
      process_instances.append(
        ProcessInstance(trace[0], frequency=trace[1], initial_position=log_start_position + (index + 6) * 0.55 * DOWN,
                        scale=0.8))
    scene.play(*[trace.fade_in() for trace in process_instances[-1:]])

    for process_instance in process_instances[-1:]:
      fade_in, move = self.log_venn.add_trace(process_instance, process_instance.key, venn=self.system_venn)
      if fade_in is not None: scene.play(*fade_in)
      scene.play(*move)
      scene.play(self.log_venn.update_venn())

    info_start_pos = 8.5*LEFT

    info_text = self.create_text('Include everything?', 1, info_start_pos, aligned_edge=LEFT)
    scene.play(FadeIn(info_text))

    self.show_petrinet_long_pay_cycle(scene, remove_log=False, remove_model=True)
    scene.play(*self.model_venn.fade_out_traces(), run_time=0.2)
    self.model_venn.reset_trace_variants()

    scene.play(Transform(info_text, self.create_text('Include only slow payer?', 1, info_start_pos, aligned_edge=LEFT)))
    self.show_petrinet_long_pay(scene, remove_log=False, remove_model=True)
    scene.play(*self.model_venn.fade_out_traces(), run_time=0.2)
    self.model_venn.reset_trace_variants()


    scene.play(Transform(info_text, self.create_text('Include only frequent behavior?', 1, info_start_pos, aligned_edge=LEFT)))
    self.show_petrinet_true(scene, remove_log=True, remove_model=True)

    self.system_venn2 = DashedVMobject(Circle(color=YELLOW, radius=1.6))
    self.system_venn2.move_to(1*UP+4*RIGHT)
    scene.play(FadeIn(self.system_venn2))

    scene.wait(1)

    scene.play(*self.model_venn.fade_out_traces(), run_time=0.2)
    self.model_venn.reset_trace_variants()

    scene.play(Transform(info_text, self.create_text('Conformance metrics', 1, info_start_pos, aligned_edge=LEFT)))

    fitness_tracker = ValueTracker(0)
    precision_tracker = ValueTracker(0)
    generalization_tracker = ValueTracker(0)
    metrics = {}
    animations = []
    for index, metric in enumerate(['Fitness:', 'Precision:', 'Generalization:']):
      text = self.create_text(metric, 0.8, info_start_pos + (index + 1) * 0.6 * DOWN, aligned_edge=LEFT, latex=True)
      number = DecimalNumber(0)
      number.next_to(text)
      animations.extend([Write(text), FadeIn(number)])
      metrics[metric] = [text, number, ValueTracker(0)]
      if index == 0:
        metrics[metric][1].add_updater(lambda m: m.set_value(fitness_tracker.get_value()))
      elif index == 1:
        metrics[metric][1].add_updater(lambda m: m.set_value(precision_tracker.get_value()))
      elif index == 2:
        metrics[metric][1].add_updater(lambda m: m.set_value(generalization_tracker.get_value()))
    scene.play(*animations)

    petrinet_flower = ProcessModel([-0.5, -3.3, 0], 'flower', name='', size=1)
    scene.play(self.model_venn.change_radius(1.65), fitness_tracker.set_value, 1, rate_func=linear, run_time=3)
    scene.play(petrinet_flower.show(fade=False), run_time=1)
    scene.play(self.model_venn.change_radius(5), run_time=2)
    scene.play(FadeOut(petrinet_flower.body))


    petrinet_trace = ProcessModel([-0.5, -3.3, 0], 'trace', name='', size=0.9)
    scene.play(petrinet_trace.show(fade=False), run_time=1)
    scene.play(self.model_venn.change_radius(1.65), precision_tracker.set_value, 1, run_time=3)
    scene.play(FadeOut(petrinet_trace.body))
    scene.play(self.model_venn.change_radius(0.2), run_time=2)

    petrinet_true = ProcessModel([-0.5, -3.3, 0], 'long_pay_cycle', name='', size=0.6)
    scene.play(petrinet_true.show(fade=False), run_time=1)
    scene.play(self.model_venn.change_radius(2.95), generalization_tracker.set_value, 1, run_time=4)

    scene.wait(10)

  # @staticmethod
  # def metric_increase_function(textmobject, alpha, position):
  #   text = TexMobject(f'Fitness: {alpha:.1f}')
  #   Transform(textmobject,  self.create_text('Include only slow payer?', 1, info_start_pos, aligned_edge=LEFT)))
  #   minimum_scale = 0.6
  #   packet.rescale_to_fit(max(minimum_scale, 1 - alpha) * 0.3, 1) if alpha < 0.5 else packet.rescale_to_fit(
  #     max(minimum_scale, alpha) * 0.3, 1)
  #   packet.move_to(utils.get_interpolated_point_on_path(packet.path, alpha))


  def create_text(self, string, scale, position, latex=False, aligned_edge=LEFT):
    if latex:
      text = TexMobject(string)
    else:
      text = Text(string)
    text.scale(scale)
    text.move_to(position, aligned_edge=LEFT)
    return text

  def update_model_venn(self, scene, log, show_log=True, add_dots=False, show_petrinet_animation=None):
    process_instances = []
    model_log = VGroup()
    for index, trace in enumerate(log):
      process_instances.append(ProcessInstance(trace[0], frequency=trace[1], scale=0.65, initial_position=6.5 * LEFT + 3 * DOWN + (index + 1) * 0.6 * DOWN))
      model_log.add(process_instances[-1].text)
    if add_dots:
      dots = TexMobject('...')
      model_log.add(dots)
    model_log.arrange_submobjects(RIGHT)
    model_log.move_to(8.5 * LEFT + 4 * DOWN, aligned_edge=LEFT)

    if show_petrinet_animation is not None:
      scene.play(FadeIn(model_log), show_petrinet_animation)
    else:
      scene.play(FadeIn(model_log))

    fade_ins = []
    moves = []
    for process_instance in process_instances:
      fade_in, move = self.model_venn.add_trace(process_instance, process_instance.key, venn=self.system_venn, radius=0.07)
      fade_ins.extend(fade_in)
      moves.extend(move)

    scene.play(*fade_ins)
    scene.play(*moves)
    scene.play(self.model_venn.update_venn())


  def show_petrinet(self, model, log, model_position, size, scene, add_dots=False, remove_log=True, remove_model=True):
    petrinet = ProcessModel(model_position, model, name='', size=size)
    self.update_model_venn(scene, log, add_dots=add_dots, show_petrinet_animation=petrinet.show(fade=False))
    if remove_log and remove_model:
      scene.play(FadeOut(petrinet.body), *self.model_venn.fade_out_traces()) #, self.model_venn.update_venn())
    elif remove_model:
      scene.play(FadeOut(petrinet.body))
    if remove_log:
      self.model_venn.reset_trace_variants()
      scene.play(self.model_venn.update_venn())

  def show_petrinet_flower(self, scene, remove_log=True, remove_model=True):
    log = [[['O', 'P', 'A', 'S'], 1], [['O', 'A', 'P', 'S'], 1], [['O', 'P', 'A', 'C'], 1], [['O', 'A', 'P', 'C'], 1],
           [['O', 'P'], 1], [['P', 'P', 'P', 'S'], 1], [['S', 'A', 'P', 'C'], 1]]
    self.show_petrinet('flower', log, [-4.5, -1, 0], size=1, scene=scene, remove_log=remove_log, remove_model=remove_model)

  def show_petrinet_trace(self, scene, remove_log=True, remove_model=True):
    log = [[['O', 'P', 'A', 'S'], 1], [['O', 'A', 'P', 'S'], 1], [['O', 'P', 'A', 'C'], 1], [['O', 'A', 'P', 'C'], 1]]
    self.show_petrinet('trace', log, [-4, -1, 0], size=1, scene=scene, remove_log=remove_log, remove_model=remove_model)

  def show_petrinet_long_pay(self, scene, remove_log=True, remove_model=True):
    log = [[['O', 'P', 'A', 'S'], 1], [['O', 'A', 'P', 'S'], 1], [['O', 'P', 'A', 'C'], 1], [['O', 'A', 'P', 'C'], 1], [['O', 'A', 'C', 'P'], 1]]
    self.show_petrinet('long_pay', log, [-2.8, -2, 0], size=0.7, scene=scene, remove_log=remove_log, remove_model=remove_model)

  def show_petrinet_long_pay_cycle(self, scene, remove_log=True, remove_model=True):
    log = [[['O', 'P', 'A', 'S'], 1], [['O', 'A', 'P', 'S'], 1], [['O', 'P', 'A', 'C'], 1], [['O', 'A', 'P', 'C'], 1], [['O', 'A', 'C', 'P'], 1], [['O', 'A', 'P', 'P', 'S'], 1]]
    self.show_petrinet('long_pay_cycle', log, [-2.8, -1.9, 0], size=0.72, scene=scene, remove_log=remove_log, remove_model=remove_model)

  def show_petrinet_true(self, scene, remove_log=True, remove_model=True):
    log = [[['O', 'P', 'A', 'S'], 1], [['O', 'A', 'P', 'S'], 1], [['O', 'P', 'A', 'C'], 1], [['O', 'A', 'P', 'C'], 1]]
    self.show_petrinet('true', log, [-2.8, -1.9, 0], size=0.65, scene=scene, remove_log=remove_log, remove_model=remove_model)

  def show_petrinet_cycle(self, scene, remove_log=True, remove_model=True):
    log = [[['O', 'P', 'A', 'S'], 1], [['O', 'A', 'P', 'S'], 1], [['O', 'P', 'A', 'C'], 1], [['O', 'A', 'P', 'C'], 1], [['O', 'A', 'C', 'P'], 1]]
    self.show_petrinet('cycle', log, [-4, -1, 0], size=0.8, scene=scene, remove_log=remove_log, remove_model=remove_model)

  def show_venns(self, scene):
    animations = [venn.fade_in() for venn in self.venns]
    scene.play(*animations)

  def initialize_venns(self):
    venn_pos = 1*UP+4*RIGHT
    self.system_venn = Venn(YELLOW, venn_pos, name='S', initial_radius=3)
    # self.system_venn.move_to(1.5*UP+4.5*RIGHT)

    self.log_venn = Venn(RED, venn_pos, name='L', initial_radius=0)
    # self.log_venn.move_to(1.5*UP+4.5*RIGHT)
    self.model_venn = Venn(BLUE, venn_pos, name='M', initial_radius=0)
    # self.model_venn.move_to(1.5*UP+4.5*RIGHT)
    self.venns = [self.system_venn, self.log_venn, self.model_venn]
