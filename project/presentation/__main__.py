import sys
sys.path.insert(0, '/home/dominique/TUe/thesis/git/')

from project.ml_models.preprocessing import GraphBuilder
from project.presentation.animation_dep import GraphAnimation
from project.presentation.venn_animation import VennAnimation
from project.presentation.process_animation import ProcessAnimation
from project.presentation.animation import PresentationAnimation

import string
from manimlib.imports import *


class Presentation(Scene):
  def init(self):
    self.traces = [
      [0, 2, 1, 3],
      [0, 1, 2, 3],
      [0, 2, 1, 4],
      [0, 1, 2, 4]
    ]
    self.counts = [10] * len(self.traces)
    labels = list(string.ascii_lowercase)
    labels = labels[:max([label for trace in self.traces for label in trace]) + 1]
    labels = ['O', 'A', 'P', 'S', 'C']
    self.transition_labels = ['>'] + labels + ['|']

  def construct(self):
    self.init()
    self.graph_builder = GraphBuilder('example', 21, self.traces, self.counts, self.transition_labels, fDepth=1, include_frequency=True, fPetrinetHandler=None)

    dep = False
    if dep:
      animation = GraphAnimation(self.graph_builder)
      animations = animation.animate(self)
    else:
      animation = PresentationAnimation(self.graph_builder)
      animation.animate(self)

    # animation = GraphAnimation(self.graph_builder)
    # animations = animation.animate(self)

    # for a in animations:
    #   getattr(self, a[0])(*a[1:])
    # animations = animation.animate2()
    # for a in animations:
    #   getattr(self, a[0])(*a[1:])

class Venn(Scene):
  def construct(self):
    animation = VennAnimation()
    animation.animate(self)

class Process(Scene):
  def construct(self):
    animation = ProcessAnimation()
    animation.animate(self)

