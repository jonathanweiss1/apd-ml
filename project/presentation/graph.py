from manimlib.imports import *
from collections import defaultdict
import networkx as nx
import manimnx as mnx
import numpy as np
import random
import matplotlib.pyplot as plt
from project.presentation import utils
from collections import defaultdict

arrow_kwargs = {'stroke_width': 2, 'tip_length': 0.15, 'max_tip_length_to_length_ratio': np.inf, 'buff': 0}

class Graph:
  def __init__(self, dgl_graph, labels, colors=None):
    self.dgl_graph = dgl_graph
    self.labels = labels
    if colors is None:
      colors = {
        '>': TEAL, 'O': BLUE, 'A': RED, 'P': ORANGE, 'C': GREEN, 'S': YELLOW, '|': PURPLE
      }
    self.colors = colors

    self.traces = defaultdict(list)
    self.places = []
    self.dgl_node_mapping = {}
    self.manim_node_mapping = {}
    self.dgl_edge_mapping = {}
    self.manim_edge_mapping = {}


  def  initialize_trace_graph(self):
    for node_index in self.dgl_graph.nodes():
      trace_index = self.dgl_graph.ndata['trace_id'][node_index].numpy()[0]
      if trace_index >= 0:
        node = self.add_node(self.dgl_graph.ndata['position'][node_index].numpy(), label=self.labels[self.dgl_graph.ndata['label'][node_index].numpy()[0] - 1], node_type='square')
        self.traces[trace_index].append(node)
        self.dgl_node_mapping[node_index.item()] = node
        self.manim_node_mapping[node] = node_index

    for node_index, node in self.dgl_node_mapping.items():
      trace_index = self.dgl_graph.ndata['trace_id'][node_index].numpy()[0]
      in_neighbors, out_neighbors = self._get_neighbors(node_index)
      for out_neighbor in out_neighbors:
        if out_neighbor not in self.dgl_node_mapping:
          continue
        out_neighbor_trace_index = self.dgl_graph.ndata['trace_id'][out_neighbor].numpy()[0]
        arrow = self.add_edge(node, self.dgl_node_mapping[out_neighbor])
        self.dgl_edge_mapping[(node_index, out_neighbor)] = arrow
        self.manim_edge_mapping[arrow] = (node_index, out_neighbor)
        if out_neighbor_trace_index == 0:
          self.traces[0].append(arrow)
        else:
          self.traces[trace_index].append(arrow)

  def visualize_traces(self, scene, trace_indices=None):
    animations = []
    for trace_index in trace_indices:
      for node in self.traces[trace_index]:
        animations.append(FadeIn(node))
    scene.play(*animations)

  def initialize_petrinet_graph(self):
    y_offset = -1.5
    for node_index in self.dgl_graph.nodes():
      node_label = self.dgl_graph.ndata['label'][node_index].numpy()[0]
      trace_index = self.dgl_graph.ndata['trace_id'][node_index].numpy()[0]
      if trace_index == -1 and node_label > 0:
        node = self.add_node(np.array([node_label - len(self.labels) / 2 - 0.5, y_offset, 1.]), label=self.labels[node_label - 1], node_type='square')
        self.traces[trace_index].append(node)
        self.dgl_node_mapping[node_index.item()] = node
        self.manim_node_mapping[node] = node_index

    for node_index, node in self.dgl_node_mapping.items():
      node_label = self.dgl_graph.ndata['label'][node_index].numpy()[0]
      trace_index = self.dgl_graph.ndata['trace_id'][node_index].numpy()[0]
      if trace_index == -1 and node_label > 0:
        in_neighbors, out_neighbors = self._get_neighbors(node_index)
        for in_neighbor in in_neighbors:
          if in_neighbor in self.dgl_node_mapping:
            arrow = self.add_edge(self.dgl_node_mapping[in_neighbor], node)
            self.dgl_edge_mapping[(in_neighbor, node_index)] = arrow
            self.manim_edge_mapping[arrow] = (in_neighbor, node_index)
            self.traces[trace_index].append(arrow)

  def adjust_place_position(self, place_label, place_pos):
    if place_label == '(A, P)':
      return place_pos + 0.2 * LEFT
    elif place_label == '(P, A)':
      return place_pos + 0.2 * RIGHT
    elif place_label == '(A, S)':
      return place_pos + 0.2 * UP
    elif place_label == '(P, C)':
      return place_pos + 0.2 * DOWN
    return place_pos

  def initialize_places(self, scene, possible_places, log=None):
    labels = [self.labels[0], self.labels[-1], *self.labels[1:-1]]
    animations = []
    one_to_one_animations, many_to_one_animations, many_to_many_animations = {}, {}, {}
    first_place_index = list(self.dgl_graph.ndata['label'].numpy() == [0]).index(True)
    for index, place in enumerate(possible_places):
      input_labels, output_labels = [labels[label] for label in place[0]], [labels[label] for label in place[1]]
      input_transitions =  [transition for transition in self.traces[-1] if isinstance(transition, VGroup) and transition.label in input_labels]
      output_transitions = [transition for transition in self.traces[-1] if isinstance(transition, VGroup) and transition.label in output_labels]
      if len(input_transitions) + len(output_transitions) == 2:
        place_pos = (sum([t.get_center() for t in input_transitions]) + sum([t.get_center() for t in output_transitions])) / (len(input_transitions) + len(output_transitions))
      else:
        place_pos = (sum([t.get_center() for t in input_transitions]) * 2 + sum([t.get_center() for t in output_transitions])) / (len(input_transitions) * 2 + len(output_transitions))

      place_label = f'({"".join([labels[i] for i in place[0]])}, {"".join([labels[i] for i in place[1]])})'
      place_pos = self.adjust_place_position(place_label, place_pos)

      new_place = self.add_node(place_pos, node_type='circle', color=WHITE, radius=0.15)
      new_place.identifier = place_label
      new_place.input_transitions = input_transitions
      new_place.output_transitions = output_transitions
      in_arrows = []
      for input_transition in input_transitions:
        arrow = self.add_edge(input_transition, new_place)
        in_arrows.append(arrow)
        self.dgl_edge_mapping[(first_place_index + index, self.manim_node_mapping[input_transition])] = arrow
        self.manim_edge_mapping[arrow] = (first_place_index + index, self.manim_node_mapping[input_transition])
      out_arrows = []
      for output_transition in output_transitions:
        arrow = self.add_edge(new_place, output_transition)
        out_arrows.append(arrow)
        self.dgl_edge_mapping[(self.manim_node_mapping[output_transition], first_place_index + index)] = arrow
        self.manim_edge_mapping[arrow] = (self.manim_node_mapping[output_transition], first_place_index + index)

      self.dgl_node_mapping[first_place_index + index] = new_place
      self.manim_node_mapping[new_place] = first_place_index + index
      self.places.extend([new_place, *in_arrows, *out_arrows])
      animations.extend([FadeIn(new_place), *[FadeIn(arrow) for arrow in in_arrows], *[FadeIn(arrow) for arrow in out_arrows]])
      if len(input_transitions) == 1 and len(output_transitions) == 1:
        one_to_one_animations[place_label] = {'mobjects': [new_place, *in_arrows, *out_arrows]}
        one_to_one_animations[place_label]['anis'] = [FadeIn(new_place), *[FadeIn(arrow) for arrow in in_arrows], *[FadeIn(arrow) for arrow in out_arrows]]
      elif len(input_transitions) == 1 or len(output_transitions) == 1:
        many_to_one_animations[place_label] = {'mobjects': [new_place, *in_arrows, *out_arrows]}
        many_to_one_animations[place_label]['anis'] = [FadeIn(new_place), *[FadeIn(arrow) for arrow in in_arrows], *[FadeIn(arrow) for arrow in out_arrows]]
      else:
        many_to_many_animations[place_label] = {'mobjects': [new_place, *in_arrows, *out_arrows]}
        many_to_many_animations[place_label]['anis'] = [FadeIn(new_place), *[FadeIn(arrow) for arrow in in_arrows], *[FadeIn(arrow) for arrow in out_arrows]]

    self.construct_places(one_to_one_animations, scene, log=log, how_many=5)
    self.construct_places(many_to_one_animations, scene, log=log, how_many=3)
    self.construct_places(many_to_many_animations, scene, log=log, how_many=100)


  def construct_places(self, animations, scene, how_many=100, log=None):
    fadeins = []
    for index, (place_label, anis) in enumerate(animations.items()):
      if index < how_many:
        highlight_anis, unhighlight_anis = log.highlight_relation(place_label)
        anis['mobjects'][0].set_fill(RED, 1)
        anis['mobjects'][0].set_color(RED)
        [m.set_stroke(RED, 5) for m in anis['mobjects'][1:]]
        scene.play(*anis['anis'], *highlight_anis)
        scene.wait(0.5)
        scene.play(*unhighlight_anis, run_time=0.5)
        anis['mobjects'][0].set_fill(BLACK, 0)
        anis['mobjects'][0].set_color(WHITE)
        [m.set_stroke(WHITE, 2) for m in anis['mobjects'][1:]]
      else:
        fadeins.extend(anis['anis'])
    if len(fadeins) > 0: scene.play(*fadeins)

  def move_nodes(self, scene, label_to_positions_mapping, only_transitions=False):
    animations = []
    for node_index, node in self.dgl_node_mapping.items():
      node_label  = self.dgl_graph.ndata['label'][node_index].numpy()[0]
      trace_index = self.dgl_graph.ndata['trace_id'][node_index].numpy()[0]
      if only_transitions and trace_index == -1 and node_label > 0:
        new_position = label_to_positions_mapping[self.labels[node_label - 1]]
        animations.extend([node.move_to, new_position])
        for in_neighbor in self._get_neighbors(node_index)[0]:
          new_positioned_node = self.add_node(new_position, node_type='Square')
          if (in_neighbor, node_index) in self.dgl_edge_mapping:
            arrow = self.dgl_edge_mapping[(in_neighbor, node_index)]
            new_arrow = self.add_edge(self.dgl_node_mapping[in_neighbor] if in_neighbor != node_index else new_positioned_node, new_positioned_node)
            arrow.target = new_arrow
            animations.append(MoveToTarget(arrow))
    scene.play(*animations)

  def shorten_trace_to_transition_arrows(self, scene, arrow_length, directions=None):
    animations = []
    nr_connected_arrows = defaultdict(lambda: 0)
    for transition in self.traces[-1]:
      if isinstance(transition, VGroup):
        transition_index = self.manim_node_mapping[transition]
        for (from_index, to_index), arrow in self.dgl_edge_mapping.items():
          if to_index == transition_index and from_index != transition_index:
            nr_connected_arrows[transition_index.item()] += 1
    max_nr_connected_arrows = max(nr_connected_arrows.values())

    counts = defaultdict(lambda: 0)
    arrows_to_add = []
    arrows_to_remove = []
    for arrow in self.traces[-1]:
      if isinstance(arrow, Arrow):
        from_index, to_index = self.manim_edge_mapping[arrow]
        if from_index == to_index:
          continue
        new_to_circle = Circle(color=BLACK, radius=0)
        new_to_circle.move_to(self.dgl_node_mapping[from_index].get_center() + arrow_length * DOWN)

        new_from_circle = Circle(color=BLACK, radius=0)
        apart = (0.5 / max_nr_connected_arrows)
        if nr_connected_arrows[to_index] > 1:
          shift = counts[to_index] * apart * RIGHT - ((nr_connected_arrows[to_index] / 2 - int(nr_connected_arrows[to_index] % 2 == 0) * 0.5) * apart) * RIGHT
        else:
          shift = 0 * RIGHT
        new_from_circle.move_to(self.dgl_node_mapping[to_index].get_center() + shift -
                                arrow_length * directions[self.labels[self.dgl_graph.ndata['label'][to_index].numpy()[0] - 1]])
        new_to_circle2 = Square(color=BLACK, side_length=0.5)
        new_to_circle2.move_to(self.dgl_node_mapping[to_index].get_center() + shift)
        arrows = [self.add_edge(self.dgl_node_mapping[from_index],  new_to_circle), self.add_edge(new_from_circle, new_to_circle2)]
        arrow.target = VGroup(*arrows)
        animations.append(MoveToTarget(arrow))
        animations.append(FadeOut(arrow))
        for index, new_arrow in enumerate(arrows):
          self.manim_edge_mapping[new_arrow] = (from_index, to_index, index)
          self.dgl_edge_mapping[(from_index, to_index, index)] = new_arrow
          arrows_to_add.append(new_arrow)
          animations.append(FadeIn(new_arrow))
          arrows_to_remove.append(arrow)
        counts[to_index] += 1

    for new_arrow in arrows_to_add:
      self.traces[-1].append(new_arrow)

    scene.play(*animations)

  def color_arrows(self, scene):
    animations = []
    for arrow in self.traces[-1]:
      if isinstance(arrow, Arrow):
        if len(self.manim_edge_mapping[arrow]) == 3:
          from_index, to_index, _ = self.manim_edge_mapping[arrow]
          node_label = self.labels[self.dgl_graph.ndata['label'][from_index].numpy()[0] - 1]
          animations.append(FadeToColor(arrow, self.colors[node_label]))
    scene.play(*animations)

  def change_colors(self, scene):
    animations = []
    for node_index, node in self.dgl_node_mapping.items():
      node_label = self.labels[self.dgl_graph.ndata['label'][node_index].numpy()[0] - 1]
      animations.append(FadeToColor(node, self.colors[node_label]))
      node.color = self.colors[node_label]
    scene.play(*animations)

  def scale_and_position_places(self, scene, new_size, positions, stroke_width=6):
    animations = []
    for mobject in self.places:
      if hasattr(mobject, 'identifier'):
        new_place = self.add_node(mobject.get_center() + positions[mobject.identifier], radius=new_size, color=WHITE, stroke_width=stroke_width)
        mobject.target = new_place
      else:
        from_node, to_node = self.manim_edge_mapping[mobject]
        from_node = from_node.item() if not isinstance(from_node, int) else from_node
        to_node = to_node.item() if not isinstance(to_node, int) else to_node
        if hasattr(from_node, 'identifier'):
          new_arrow = self.add_edge(self.dgl_node_mapping[to_node].target, self.dgl_node_mapping[from_node].target)
        else:
          new_arrow = self.add_edge(self.dgl_node_mapping[to_node].target, self.dgl_node_mapping[from_node].target)
        mobject.target = new_arrow
      animations.append(MoveToTarget(mobject))

    scene.play(*animations)

  def remove_places(self, scene, place_identifiers, keep=False):
    animations = []
    to_remove = []
    remove = False
    for mobject in self.places:
      if hasattr(mobject, 'identifier'):
        if (not keep and mobject.identifier in place_identifiers) or (keep and mobject.identifier not in place_identifiers):
          to_remove.append(mobject)
          remove = True
        else:
          remove = False
      elif remove:
        to_remove.append(mobject)

    for mobject in to_remove:
      if isinstance(mobject, Circle):
        del self.dgl_node_mapping[self.manim_node_mapping[mobject]]
        del self.manim_node_mapping[mobject]
      if isinstance(mobject, Arrow):
        del self.dgl_edge_mapping[self.manim_edge_mapping[mobject]]
        del self.manim_edge_mapping[mobject]
      self.places.remove(mobject)
      animations.append(FadeOut(mobject))

    scene.play(*animations)

  def color_place(self, place_label, color=GREEN):
    for mobject in self.places:
      if hasattr(mobject, 'identifier') and mobject.identifier == place_label:
        new_place = mobject.copy()
        new_place.set_stroke(width=10)
        new_place.set_color(color)
        mobject.target = new_place
        return MoveToTarget(mobject)

  def spawn_packets(self, scene):
    animations = []
    for node_index, node in self.dgl_node_mapping.items():
      node.packets = []
      node.new_packets = []
      for i in self._get_neighbors(node_index)[1]:
        # if i == node_index or i not in self.dgl_node_mapping:
        if i not in self.dgl_node_mapping:
          continue
        packet = self.create_packet(node, color=node.color)
        animations.append(FadeIn(packet))
        node.packets.append(packet)
    scene.play(*animations)

  @staticmethod
  def packet_move_function(packet, alpha):
    minimum_scale = 0.6
    packet.rescale_to_fit(max(minimum_scale, 1 - alpha) * 0.3, 1) if alpha < 0.5 else packet.rescale_to_fit(max(minimum_scale, alpha) * 0.3, 1)
    packet.move_to(utils.get_interpolated_point_on_path(packet.path, alpha))

  def propagate_packets(self, scene, time=1):
    animations = []
    for node_index, node in self.dgl_node_mapping.items():
      # neighbors = [i for i in self._get_neighbors(node_index)[1] if i != node_index and i in self.dgl_node_mapping]
      neighbors = [i for i in self._get_neighbors(node_index)[1] if i in self.dgl_node_mapping]
      for out_neighbor, packet in zip(neighbors, node.packets):
        neighbor_node = self.dgl_node_mapping[out_neighbor]
        packet.remove_updater(self.packet_move_function)
        packet.path = [packet.get_center(),
                       node.get_center(),
                       neighbor_node.get_center(),
                       neighbor_node.get_center() + 0.2 * UP + 0.25 * RIGHT]
        animations.append(UpdateFromAlphaFunc(packet, self.packet_move_function, rate_func=linear))
        neighbor_node.new_packets.append(packet)
    scene.play(*animations, run_time=time)

  def reset_packets(self, scene):
    animations = []
    for node_index, node in self.dgl_node_mapping.items():
      # neighbors = [i for i in self._get_neighbors(node_index)[1] if i != node_index and i in self.dgl_node_mapping]
      neighbors = [i for i in self._get_neighbors(node_index)[1] if i in self.dgl_node_mapping]
      if len(neighbors) == 0:
        continue

      if len(node.new_packets) == 0:
        node.packets = [self.create_packet(node, color=node.color) for i in range(len(neighbors))]
        animations.extend([FadeIn(packet) for packet in node.packets])
      else:
        if len(node.new_packets) >= len(neighbors):
          node.packets = node.new_packets[:len(neighbors)]
          animations.extend([FadeOut(packet) for packet in node.new_packets[len(neighbors):]])
        else:
          node.packets = node.new_packets + [self.create_packet(node, color=node.color) for i in range((len(neighbors) - len(node.new_packets)))]
      node.new_packets = []
      new_color = utils.get_average_color([packet.color for packet in node.packets])
      animations.extend([FadeToColor(packet, new_color) for packet in node.packets])
      for packet in node.packets:
        packet.color = new_color
    scene.play(*animations, run_time=0.5)

  def delete_packets(self, scene):
    animations = []
    for node_index, node in self.dgl_node_mapping.items():
      animations.extend([FadeOut(packet) for packet in node.packets])
    scene.play(*animations)

  def packet_propagation(self, scene, rounds=1, spawn_new=False, delete_afterwards=False, wait_after_spawning=False):
    if spawn_new:
      self.spawn_packets(scene)
    if wait_after_spawning:
      scene.wait(1)
    for index, round in enumerate(range(rounds)):
      if wait_after_spawning and index == 0:
        self.propagate_packets(scene, time=3)
      else:
        self.propagate_packets(scene)
      self.reset_packets(scene)
    if delete_afterwards:
      self.delete_packets(scene)

  def scale_graph(self, scene, factor, mobjects):
    scene.play(ScaleInPlace(VGroup(*mobjects), factor))

  def move_graph(self, scene, location, mobjects):
    group = VGroup(*mobjects)
    scene.play(group.move_to, group.get_center() + location)

  def create_packet(self, node, random=False, random_seed=None, as_vector=False, color=None):
    if as_vector:
      packet = TexMobject('\\begin{bmatrix} ' + f'{np.random.rand() if random else 0.12:.2f}'[1:] + ' \\\\ \\vdots \\\\ ' +
                          f'{np.random.rand() if random else 0.73:.2f}'[1:] + ' \\end{bmatrix}')
      packet.scale_in_place(0.3)
      packet.move_to(node.get_center())
    else:
      width, margin = 0.2, 0.03
      if color is None:
        color = utils.get_random_color(random_seed) if random else WHITE
      packet = VGroup(
        Rectangle(height=0.3, width=width, color=color),
        Line((width / 2 - margin) * LEFT, (width / 2 - margin) * RIGHT, color=color, stroke_width=1.5),
        Line((width / 2 - margin) * LEFT + 0.07 * UP, (width / 2 - margin) * RIGHT + 0.07 * UP, color=color, stroke_width=1.5),
        Line((width / 2 - margin) * LEFT + 0.07 * DOWN, (width / 2 - margin) * RIGHT + 0.07 * DOWN, color=color, stroke_width=1.5)
      )
      packet.color = color
      packet.move_to(node.get_center() + 0.2 * UP + 0.25 * RIGHT)
    return packet


  def get_all_trace_mobjects(self):
    trace_objects = []
    for trace, mobjects in self.traces.items():
      if trace >= 0:
        trace_objects.extend(mobjects)
    for fromTo, arrow in self.dgl_edge_mapping.items():
      if len(fromTo) == 3 and fromTo[2] == 0:
        trace_objects.append(arrow)
    return trace_objects

  def set_place_labels(self, labels):
    label_objects = []
    for place_id, label in labels.items():
      for place in self.places:
        if hasattr(place, 'identifier') and place.identifier == place_id:
          label_object = DecimalNumber(label, num_decimal_places=1)
          label_object.scale(0.6)
          label_object.move_to(place.get_center())
          label_objects.append(label_object)
    return label_objects

  def add_node(self, position, label=None, node_type='circle', radius=0.25, color=BLUE, add_packet=True, **node_kwargs):
    if node_type.lower() == 'circle':
      node = Circle(color=color, radius=radius, **node_kwargs)
    elif node_type.lower() == 'square':
      node = Square(color=color, fill_color=color, fill_opacity=0, side_length=2*radius, **node_kwargs)
    else:
      raise ValueError(f'node type: {node_type} not understood.')

    if label is not None:
      label_object = TexMobject(label)
      node = VGroup(node, label_object)
      node.label_object = label_object
    node.label = label

    node.move_to(position)
    return node

  def add_edge(self, from_node, to_node, directed=True, **kwargs):
    arrow_kwargs = {'stroke_width': 2, 'tip_length': 0.15, 'max_tip_length_to_length_ratio': np.inf, 'buff': 0}
    from_position, to_position = utils.get_adjusted_points(from_node, to_node)
    if all(from_position == to_position):
      arrow_kwargs['stroke_width'] = 0.
      arrow_kwargs['tip_length']   = 0.
    arrow = Arrow(from_position, to_position, **arrow_kwargs, **kwargs)
    arrow.from_node = from_node
    arrow.to_node = to_node
    return arrow

  def _get_neighbors(self, node_id):
    all_out_neighbors = self.dgl_graph.successors(node_id).numpy()
    all_in_neighbors = self.dgl_graph.predecessors(node_id).numpy()
    in_neighbors, out_neighbors = [], []
    for neighbor in all_in_neighbors:
      edges = self.dgl_graph.edge_ids(neighbor, node_id, return_uv=True)[2]
      for edge_direction in self.dgl_graph.edata['direction'][edges]:
        if edge_direction[0][0] == 1: in_neighbors.append(neighbor)
    for neighbor in all_out_neighbors:
      edges = self.dgl_graph.edge_ids(node_id, neighbor, return_uv=True)[2]
      for edge_direction in self.dgl_graph.edata['direction'][edges]:
        if edge_direction[0][0] == 1: out_neighbors.append(neighbor)
    return in_neighbors, out_neighbors
