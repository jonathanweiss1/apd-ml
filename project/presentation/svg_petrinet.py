from manimlib.imports import *

class SVGPetrinet(SVGMobject):
  CONFIG = {
    "color" : BLUE_E,
    "file_name_prefix": "petrinet",
    "stroke_width": 2,
    "stroke_color": WHITE,
    "fill_opacity": 0.0,
    "height": 3
  }
  def __init__(self, mode="trace", **kwargs):
    digest_config(self, kwargs)
    self.mode = mode
    self.parts_named = False
    svg_file = os.path.join(f'/home/dominique/TUe/thesis/presentation/figures/{self.file_name_prefix}_{mode}.svg')
    SVGMobject.__init__(self, file_name=svg_file, **kwargs)
    self.add_labels()

  def init_colors(self):
    SVGMobject.init_colors(self)
    places = [m for m in self.submobjects if m.__class__.__name__ == 'Circle']
    [place.set_fill(BLACK, opacity=0) for place in places]
    places[0].set_fill(GREEN, opacity=1)
    places[-1].set_fill(ORANGE, opacity=1)
    places[-2].set_fill(ORANGE, opacity=1)
    return self

  def add_labels(self):
    transitions = [m for m in self.submobjects if m.__class__.__name__ == 'Rectangle']
    labels = ['O', 'A', 'P', 'C', 'S']
    if len(transitions) > 8:
      labels = ['O', 'P', 'A', 'S', 'O', 'A', 'P', 'S', 'O', 'P', 'A', 'C', 'O', 'A', 'P', 'C']
    transitions = [m for m in self.submobjects if m.__class__.__name__ == 'Rectangle']
    for label, transition in zip(labels, transitions):
      t = TexMobject(label)
      t.move_to(transition.get_center())
      self.add(t)
