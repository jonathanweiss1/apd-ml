from manimlib.imports import *

from project.presentation.graph import Graph
from project.presentation.log import Log
from project.presentation.venn_animation import ProcessModel

import numpy as np
import torch as th

class LogText:
  def __init__(self, traces):
    self.traces = traces
    self.initialize_log()

  def initialize_log(self):
    self.trace_texts = []
    self.relations = []
    self.text_objects = {}
    for index, trace in enumerate(self.traces):
      self.text_objects[index] = []
      trace = ['>', *trace, '|']
      self.text_objects[index].append(TexMobject('\\left<\\right.'))
      for index2, event in enumerate(trace):
        self.text_objects[index].append(TexMobject(event))
        if index2 < len(trace) - 1: self.text_objects[index].append(TexMobject(','))
      self.text_objects[index].append(TexMobject('\\left.\\right>\\\\'))

      self.trace_texts.append(VGroup(*self.text_objects[index]))
      self.trace_texts[-1].arrange_submobjects(RIGHT, center=False, aligned_edge=DOWN)

    self.text = VGroup(*self.trace_texts)
    self.text.arrange_submobjects(DOWN, center=False, aligned_edge=LEFT)

  def highlight_traces(self, trace_indices, color=GREEN):
    animations = []
    for trace_index in trace_indices:
      for text_object in self.text_objects[trace_index]:
        animations.extend([text_object.set_color, color])
    return animations

  def highlight_relation(self, relation):
    highlight_animations, unhighlight_animations = [], []
    from_events = list(relation[1:-1].split(', ')[0])
    to_events = list(relation[1:-1].split(', ')[1])
    colors = [GREEN, YELLOW, ORANGE, BLUE]
    rels = []
    for index, trace in enumerate(self.traces):
      trace = ['>', *trace, '|']
      for index2, (event, next_event) in enumerate(zip(trace[:-1], trace[1:])):
        if event in from_events and next_event in to_events:
          if f'{event}-{next_event}' not in rels:
            rels.append(f'{event}-{next_event}')
          color = colors[rels.index(f'{event}-{next_event}')]
          t1 = self.text_objects[index][index2*2 + 1]
          t2 = self.text_objects[index][index2 * 2 + 3]
          t1.rect = Rectangle(height=0.7, width=1.3, color=color, stroke_width=2)
          t1.rect.move_to((t1.get_center() + t2.get_center()) / 2)
          highlight_animations.extend([FadeIn(t1.rect), t1.set_color, color, t2.set_color, color])
          unhighlight_animations.extend([FadeOut(t1.rect), t1.set_color, WHITE, t2.set_color, WHITE])

    return highlight_animations, unhighlight_animations

  def get_position(self):
    return self.text.get_center()

  def write(self):
    return Write(self.text)

  def change_color(self):
    return [self.text_objects[1][1].set_color, RED]

  def fade_in(self):
    return FadeIn(self.text)

  def fade_out(self):
    return FadeOut(self.text)

  def fade_word_for_word(self):
    return AddTextWordByWord(self.text, run_time=2.0)

class PresentationAnimation:
  def __init__(self, graph_builder):
    self.graph_builder = graph_builder
    self.graph = Graph(self.graph_builder.mNet, self.graph_builder.mTransitionNames)
    self.log = Log(self.graph_builder.mTraces, self.graph_builder.mTransitionNames)

  def set_trace_positions(self, y_offset=4):
    graph = self.graph_builder.mNet
    traces = self.graph_builder.mTraces
    x_offset = max([len(t) for t in traces]) / 2 + 0.5 - 4
    positions = [[0.0 - x_offset, y_offset - (len(traces) / 2 - 0.5), 0],
                 [max([len(t) for t in traces]) + 1 - x_offset, y_offset - (len(traces) / 2 - 0.5), 0]]
    for index, trace in enumerate(traces):
      positions.extend([[activity_index + 1 - x_offset, y_offset - index, 0] for activity_index in range(len(trace))])

    graph.nodes[:].data['position'] = np.zeros((graph.number_of_nodes(), 3))
    graph.ndata['position'][:len(positions)] = th.tensor(np.array(positions))

  def get_transition_positions(self, y_offset=-2.5):
    positions = {'>': 4 * LEFT + y_offset, 'O': 1 * LEFT + y_offset, 'A': 2 * RIGHT + y_offset + 1 * UP, 'P': 2 * RIGHT + y_offset + 1 * DOWN,
                 'C': 5 * RIGHT + y_offset + 1 * UP, 'S': 5 * RIGHT + y_offset + 1 * DOWN, '|': 8 * RIGHT + y_offset}
    return positions

  def get_place_positions(self):
    return {
      '(>, O)': 0 * RIGHT + 0 * UP, '(O, A)': -0.25 * RIGHT + 0.5 * UP, '(O, P)': -0.25 * RIGHT - 0.5 * UP, '(OA, P)': 0 * RIGHT + 0.25 * UP,
      '(OP, A)': 0 * RIGHT - 0.25 * UP, '(A, C)': 0.25 * RIGHT + 0.25 * UP, '(P, C)': -0.1 * RIGHT - 0.75 * UP, '(A, S)': -0.1 * RIGHT + 0.75 * UP,
      '(P, S)': 0.25 * RIGHT - 0.25 * UP, '(A, SC)': -0.5 * RIGHT - 0 * UP, '(P, SC)': -0.5 * RIGHT + 0 * UP, '(C, |)': 0 * RIGHT + 0 * UP,
      '(S, |)': 0 * RIGHT + 0 * UP, '(SC, |)': 0 * RIGHT + 0 * UP}

  def get_place_positions2(self):
    return {'(>, O)': 0*UP, '(O, A)': 0*UP, '(O, P)': 0*UP, '(A, SC)': 0.5 * RIGHT + 0.5 * UP, '(P, SC)': 0.5 * RIGHT - 0.5 * UP, '(SC, |)': RIGHT + 0 * UP}

  def create_text(self, string, scale, position, latex=False, aligned_edge=LEFT):
    if latex:
      text = TexMobject(string)
    else:
      text = Text(string)
    text.scale(scale)
    text.move_to(position, aligned_edge=LEFT)
    return text

  def animate(self, scene):
    self.set_trace_positions()
    log = LogText([['O', 'P', 'A', 'S'], ['O', 'A', 'P', 'S'], ['O', 'P', 'A', 'C'], ['O', 'A', 'P', 'C']])
    log.text.move_to([0, 0, 0])
    scene.play(log.write())

    # move log away from center
    scene.play(log.text.move_to, [7.5*LEFT+2.5*UP], {'aligned_edge': LEFT})

    info_start_pos = 8.5 * LEFT
    info_text = self.create_text('Initialize the trace graph', 1, info_start_pos, aligned_edge=LEFT)
    scene.play(FadeIn(info_text))

    self.graph.initialize_trace_graph()
    # visualize first trace
    scene.play(*log.highlight_traces([0]))
    self.graph.visualize_traces(scene, [1])
    scene.play(*log.highlight_traces([0], color=WHITE))

    # highlight all traces in log and visualize all traces
    scene.play(*log.highlight_traces([1, 2, 3]))
    self.graph.visualize_traces(scene, [2, 3, 4])
    scene.play(*log.highlight_traces([1, 2, 3], color=WHITE))

    scene.play(Transform(info_text, self.create_text('Add artificial start and end transitions', 1, info_start_pos, aligned_edge=LEFT)))
    # add artificial start and end transitions
    self.graph.visualize_traces(scene, [0])

    # change colors
    self.graph.change_colors(scene)

    scene.play(Transform(info_text, self.create_text('Create transitions for Petri net', 1, info_start_pos, aligned_edge=LEFT)))
    self.graph.initialize_petrinet_graph()
    # visualize petrinet transitions
    self.graph.visualize_traces(scene, [-1])

    self.graph.change_colors(scene)

    # move petrinet transitions
    self.graph.move_nodes(scene, self.get_transition_positions(), only_transitions=True)
    # shorten trace to transition arrows
    directions = {'>': DOWN, 'O': DOWN, 'A': DOWN, 'C': DOWN, '|': DOWN, 'P': UP, 'S': UP}
    self.graph.shorten_trace_to_transition_arrows(scene, 0.6, directions)
    self.graph.color_arrows(scene)

    # Add places
    # Initialize places while highlighting the log.
    scene.play(Transform(info_text, self.create_text('Add candidate places', 1, info_start_pos, aligned_edge=LEFT)))
    self.graph.initialize_places(scene, self.graph_builder.mPossiblePlaces, log=log)

    scene.play(log.fade_out())

    # Remove and replace places
    keep_paces = ['(>, O)', '(O, A)', '(O, P)', '(OA, P)', '(OP, A)', '(A, C)',
                  '(P, S)', '(A, SC)', '(P, SC)', '(C, |)', '(S, |)', '(SC, |)']
    self.graph.remove_places(scene, keep_paces, keep=True)
    # Reposition and scale places
    self.graph.scale_and_position_places(scene, 0.3, self.get_place_positions())

    scene.play(Transform(info_text, self.create_text('Produce process model', 1, info_start_pos, aligned_edge=LEFT)))
    petrinet = ProcessModel([-4, 3, 0], 'true', name='', size=0.6)
    scene.play(petrinet.show(fade=False))

    scene.wait(5)

    # Reposition and scale trace graph
    # self.graph.scale_graph(scene, 0.8, self.graph.get_all_trace_mobjects())
    # self.graph.move_graph(scene, 3 * RIGHT, self.graph.get_all_trace_mobjects())

    scene.play(Transform(info_text, self.create_text('Packet propagation (4 steps)', 1, info_start_pos, aligned_edge=LEFT)))

    self.graph.packet_propagation(scene, 4, spawn_new=True, wait_after_spawning=True)

    scene.wait(2)

    scene.play(Transform(info_text, self.create_text('Choose places', 1, info_start_pos, aligned_edge=LEFT)))

    ip1 = self.create_information_packet('>', [''], ['OA', 'OP'])
    ip2 = self.create_information_packet('O', ['>'], ['AP', 'PA'])

    scene.play(FadeIn(ip1), FadeIn(ip2))

    ip3 = self.create_information_packet('A', ['>O', 'OP'], ['PC', 'S|'])

    choose_places = ['(>, O)', '(O, A)', '(O, P)', '(A, SC)', '(P, SC)', '(SC, |)']
    for index, choose_place in enumerate(choose_places):
      if index == 1:
        scene.play(FadeOut(ip1), FadeOut(ip2))
      if index == 2:
        scene.play(FadeOut(ip2), FadeOut(ip3))
      if index > 0:
        self.graph.packet_propagation(scene, 2)
      if index == 1:
          scene.play(FadeIn(ip2), FadeIn(ip3))


      jfs = self.graph.set_place_labels(self.get_place_probability_labels(index))
      scene.play(*[FadeIn(j) for j in jfs])
      scene.play(self.graph.color_place(choose_place))
      scene.play(*[FadeOut(j) for j in jfs])
      scene.wait(2)

    scene.play(Transform(info_text, self.create_text('Clean up', 1, info_start_pos, aligned_edge=LEFT)))

    self.graph.delete_packets(scene)
    self.graph.remove_places(scene, choose_places, keep=True)
    self.graph.scale_and_position_places(scene, 0.3, self.get_place_positions2())

    scene.wait(10)

  def get_place_probability_labels(self, index):
    labels = ['(>, O)', '(O, A)', '(O, P)', '(A, SC)', '(P, SC)', '(SC, |)']
    zeroes = {'(>, O)': 0.0, '(O, A)': 0.0, '(O, P)': 0.0, '(A, SC)': 0.0, '(P, SC)': 0.0, '(SC, |)': 0.0,
              '(OP, A)': 0.0, '(OA, P)': 0.0, '(A, C)': 0.0, '(P, S)': 0.0, '(C, |)': 0.0, '(S, |)': 0.0}
    if index == 1:
      zeroes['(O, A)'] = 0.5
      zeroes['(O, P)'] = 0.5
    elif index == 2:
      zeroes['(O, P)'] = 1.0
    else:
      zeroes[labels[index]] = 1.0
    return zeroes

  def create_information_packet(self, transition_label, left_info, right_info, align_right=False):
    if transition_label is None:
      location = [0, 0, 0]
    else:
      transition_node = [transition for transition in self.graph.traces[-1] if isinstance(transition, VGroup) and transition.label == transition_label][0]
      location = transition_node.get_center()
      location += 0.1 * TOP

    l = Rectangle(width=0.8, height=1, stroke_width=2)
    r = Rectangle(width=0.8, height=1, stroke_width=2)
    r.move_to(location, aligned_edge=LEFT + BOTTOM)
    l.move_to(location, aligned_edge=RIGHT + BOTTOM)

    lh = Rectangle(width=0.8, height=0.3, stroke_width=2)
    lh.move_to(location + 0.2 * TOP, aligned_edge=RIGHT+BOTTOM)

    rh = Rectangle(width=0.8, height=0.3, stroke_width=2)
    rh.move_to(location + 0.2 * TOP, aligned_edge=LEFT + BOTTOM)
    lht = TexMobject('\\leftarrow')
    lht.scale(0.8)
    rht = TexMobject('\\rightarrow')
    rht.scale(0.8)
    lht.move_to(lh.get_center())
    rht.move_to(rh.get_center())


    lt = VGroup()
    for i in left_info:
      lt.add(TexMobject(i))
    lt.arrange(DOWN)
    lt.move_to(l.get_center())
    lt.scale(0.8)
    rt = VGroup()
    for i in right_info:
      rt.add(TexMobject(i))
    rt.arrange(DOWN)
    rt.move_to(r.get_center())
    rt.scale(0.8)

    return VGroup(l, r, lt, rt, lh, rh, lht, rht)
