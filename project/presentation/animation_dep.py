from manimlib.imports import *
from collections import defaultdict
import networkx as nx
import manimnx as mnx
import numpy as np
import random
import matplotlib.pyplot as plt
from project.presentation import utils

class Graph(VGroup):
  def __init__(self, dgl_graph, labels, positions, **kwargs):
    super().__init__(**kwargs)
    self.dgl_graph = dgl_graph
    self.labels = labels
    self.positions = positions
    self.traces = defaultdict(list)
    self.places = []
    self.colors = {'>': RED, '|': RED, 'a': BLUE, 'b': GREEN, 'c': PURPLE, 'd': YELLOW, 'e': ORANGE}
    self.done = None
    self.arrow_kwargs = {'stroke_width': 2, 'tip_length': 0.15, 'max_tip_length_to_length_ratio': np.inf}
    self._init_edges()
    self._init_nodes()

  def _init_nodes(self):
    for label, position, trace_id in zip(self.dgl_graph.ndata['label'], self.positions, self.dgl_graph.ndata['trace_id']):
      node_label = self.labels[label - 1]
      # node = Dot(color=BLUE, radius=0.25)
      node = Square(color=BLUE, fill_color=BLUE, fill_opacity=0, side_length=0.5)
      node.node_label = node_label
      node.is_transition = False
      node.trace_graph = True
      self.add_packet_to_node(node, color=self.colors[node_label])
      position = position[0] * RIGHT + position[1] * UP
      node.position = position
      node.move_to(position)
      label = TextMobject(node_label)
      label.move_to(node.get_center())
      label.is_transition = False
      label.trace_graph = True
      self.add(node)
      self.add(label)
      self.traces[trace_id.item()].extend([node, label])

  def _init_edges(self):
    sources, destinations = self.dgl_graph.edges()
    for source, destination, direction in zip(sources, destinations, self.dgl_graph.edata['direction']):
      if direction[0][0] == 0:
        continue
      source_trace_id = self.dgl_graph.ndata['trace_id'][source].item()
      destination_trace_id = self.dgl_graph.ndata['trace_id'][destination].item()
      x1, y1 = self.positions[source]
      x2, y2 = self.positions[destination]
      arrow = Arrow(x1 * RIGHT + y1 * UP, x2 * RIGHT + y2 * UP, color=WHITE, stroke_width=2, tip_length=0.15, max_tip_length_to_length_ratio=np.inf)
      self.add(arrow)
      arrow.trace_graph = True
      if source_trace_id == destination_trace_id or source_trace_id == 0:
        self.traces[source_trace_id].append(arrow)
      else:
        self.traces[destination_trace_id].append(arrow)

  def get_trace_creation_animation(self, traces):
    creations = []
    for trace in traces:
      for mobject in self.traces[trace]:
        creations.append(FadeIn(mobject))
    return ('play', *creations)

  def transform_to_circles(self):
    animations = []
    for node in self:
      if node.__class__.__name__ == 'Square':
        circle = Circle(color=BLUE, radius=0.25)
        circle.move_to(node.get_center())
        animations.append(Transform(node, circle))
    return [('play', *animations)]

  def change_colors(self):
    animations = []
    for node in self:
      if node.__class__.__name__ == 'Square':
        animations.extend([FadeToColor(node, self.colors[node.node_label])])
    return [('play', *animations)]

  def get_creation_animation(self):
    return [('play', *[ShowCreation(m) for m in self])]

  def scale_tracegraph(self, factor):
    trace_objects = []
    for mobject in self:
      if hasattr(mobject, 'trace_graph'):
        trace_objects.append(mobject)
    group = VGroup(*trace_objects)
    return [('play', ScaleInPlace(group, factor))]

  def move_tracegraph(self, location):
    trace_objects = []
    # positions = []
    for mobject in self:
      if hasattr(mobject, 'trace_graph'):
        trace_objects.append(mobject)
        # if hasattr(mobject, 'position'):
        #   positions.append(mobject.position)
    group = VGroup(*trace_objects)
    # return [('play', group.move_to, np.mean(np.array(positions), axis=0) + location)]

    return [('play', group.shift, location)]

  def create_path_arrow(self, start, end, color=WHITE, full=True):
    kwargs = {'color': color, **self.arrow_kwargs}
    if start[0] > end[0]:
      points = [np.array([start[0] - 0.176777, start[1] - 0.176777, start[2]])]
      if abs(end[0] - (start[0] - 0.5)) >= 0.25 and full:
        points.append(np.array([start[0] - 0.5, start[1] - 0.5, start[2]]))
    elif start[0] < end[0]:
      points = [np.array([start[0] + 0.176777, start[1] - 0.176777, start[2]])]
      if abs(end[0] - (start[0] + 0.5)) >= 0.25 and full:
        points.append(np.array([start[0] + 0.5, start[1] - 0.5, start[2]]))
    else:
      points = [np.array([start[0], start[1] - 0.25, start[2]])]

    if full:
      points.append(np.array([end[0], start[1] - 0.5, start[2]]))
    points.append(end)
    lines = [Line(s, e, **kwargs) for s, e in zip(points[:-2], points[1:-1])]
    lines.append(Arrow(np.array([points[-2][0], points[-2][1] + 0.25, points[-2][2]]), points[-1], **kwargs))
    for line in lines:
      line.complex_line = True
    return lines

  def add_transition_edges(self, positions, full=True):
    for trace_index, trace_nodes in self.traces.items():
      if trace_index >= 0:
        for node in trace_nodes:
          if node.__class__.__name__ == 'Square':
            if node.node_label in ['>', '|']:
              x_offset2 = 0
            elif node.node_label == 'd':
              x_offset2 = (0.1 * (trace_index - 1.5)) * RIGHT
            elif node.node_label == 'e':
              x_offset2 = (0.1 * (trace_index - 3.5)) * RIGHT
            else:
              x_offset2 = (0.1 * (trace_index - 2.5)) * RIGHT
            self.traces[-1].extend(self.create_path_arrow(node.position, positions[node.node_label] + x_offset2, color=self.colors[node.node_label], full=full))

  def update_packet_positions(self, m):
    m.position = m.node.get_center() + 0.2 * UP + 0.25 * RIGHT
    # print(m.color, m.position)
    m.move_to(m.position)

  def add_packet_to_node(self, node, color=None):
    if color is not None:
      packet = self.get_packet(color=color)
    else:
      packet = self.get_packet(random=True)
    packet.node = node
    packet.color = color
    packet.add_updater(self.update_packet_positions)
    node.packet = packet

  def add_transitions(self):
    y = min([pos[1] for pos in self.positions]) - 1.5
    x_offset = len(self.labels) / 2 - 0.5
    positions = {label: (index - x_offset) * RIGHT + y * UP for index, label in enumerate(self.labels)}
    for index, label in enumerate(self.labels):
      node = Square(color=self.colors[label], fill_color=self.colors[label], fill_opacity=0, side_length=0.5)
      node.node_label = label
      node.is_transition = True
      self.add_packet_to_node(node, color=self.colors[label])
      positions[label] = np.array([positions[label][0], positions[label][1], 0.0])
      node.position = positions[label]
      node.move_to(positions[label])
      label_object = TextMobject(label)
      label_object.move_to(node.get_center())
      label_object.node_label = label
      label_object.is_transition = True
      self.add(node)
      self.add(label_object)
      self.traces[-1].extend([node, label_object])
    self.add_transition_edges(positions, full=True)

  def add_small_arrows(self, position, edge, number_of_arrows=1, fromTo='from', color=WHITE):
    kwargs = {'color': color, **self.arrow_kwargs}
    arrows = []
    for index in range(number_of_arrows):
      if number_of_arrows == 2:
        x_offset = (0.1 * (index - 0.5)) * RIGHT
      elif number_of_arrows == 4:
        x_offset = (0.1 * (index - 1.5)) * RIGHT
      else:
        x_offset = 0
      points = [position + x_offset * RIGHT, position + .85 * UP * int(edge == 'up') + .85 * DOWN * int(edge == 'down') + x_offset * RIGHT]
      if fromTo == 'to':
        points = [points[1], points[0]]
      arrows.append(Arrow(points[0], points[1], **kwargs))
    return arrows

  def move_transitions(self):
    y = min([pos[1] for pos in self.positions]) - 2.5
    positions = {'>': 4 * LEFT + y, 'a': 1 * LEFT + y, 'b': 2 * RIGHT + y + 1 * UP, 'c': 2 * RIGHT + y + 1 * DOWN, 'd': 5 * RIGHT + y + 1 * UP, 'e': 5 * RIGHT + y + 1 * DOWN, '|': 8 * RIGHT + y}

    animations = []
    for mobject in self.traces[-1]:
      if mobject.__class__.__name__ in ['Square', 'TextMobject'] and mobject.is_transition:
        mobject.position = positions[mobject.node_label]
        animations.extend([mobject.move_to, positions[mobject.node_label]])
      elif mobject.__class__.__name__ in ['Arrow', 'Line']:
        animations.append(FadeOut(mobject))
    number_of_arrows = {'>': 1, '|': 1, 'a': 4, 'b': 4, 'c': 4, 'd': 2, 'e': 2}
    edges = {'>': 'up', '|': 'up', 'a': 'up', 'b': 'up', 'c': 'down', 'd': 'up', 'e': 'down'}
    for trace_index, trace_objects in self.traces.items():
      for mobject in trace_objects:
        if mobject.__class__.__name__ == 'Square':
          arrows = self.add_small_arrows(mobject.position, 'down' if not mobject.is_transition else edges[mobject.node_label],
                                         number_of_arrows=1 if trace_index > 0 else number_of_arrows[mobject.node_label],
                                         fromTo='from' if not mobject.is_transition else 'to', color=self.colors[mobject.node_label])
          if not mobject.is_transition:
            arrows[0].trace_graph = True
          [self.add(arrow) for arrow in arrows]
          self.traces[0 if mobject.is_transition else -1].extend(arrows)
          animations.extend([FadeIn(arrow) for arrow in arrows])
    return [('play', *animations)]

  def find_transition_node(self, label):
    for mobject in self.traces[-1]:
      if mobject.__class__.__name__ == 'Square' and hasattr(mobject, 'is_transition') and mobject.is_transition and label == mobject.node_label:
        return mobject
    return None

  def remove_places(self):
    animations = []
    for mobject in self.places:
      animations.append(FadeOut(mobject))
      self.remove(mobject)
    self.places = []
    return [('play', *animations)]

  def add_places(self, possible_places, big=False):
    arrow_kwargs = {'stroke_width': 2 if big else 1.5, 'tip_length': 0.15 if big else 0.08, 'max_tip_length_to_length_ratio': np.inf, 'buff': 0.0}
    labels = [self.labels[0], self.labels[-1], *self.labels[1:-1]]
    animations = []
    keep_paces = ['(>, a)', '(a, b)', '(a, c)', '(ab, c)', '(ac, b)', '(b, d)', '(c, d)', '(b, e)', '(c, e)', '(b, de)', '(c, de)', '(d, |)', '(e, |)', '(de, |)']
    for place in possible_places:
      new_place = Circle(color=WHITE, fill_opacity=0, radius=0.2 if big else 0.1)

      place_label = f'({"".join([labels[i] for i in place[0]])}, {"".join([labels[i] for i in place[1]])})'
      if place_label not in keep_paces and big:
        continue
      input_transitions  = [self.find_transition_node(labels[i]) for i in place[0]]
      output_transitions = [self.find_transition_node(labels[i]) for i in place[1]]
      if len(input_transitions) + len(output_transitions) == 2:
        place_pos = (sum([t.position for t in input_transitions]) + sum([t.position for t in output_transitions])) / (len(input_transitions) + len(output_transitions))
      else:
        place_pos = (sum([t.position for t in input_transitions]) * 2 + sum([t.position for t in output_transitions])) / (len(input_transitions) * 2 + len(output_transitions))
      if big:
        if place_label in ['(b, de)', '(c, de)']:
          place_pos[0] -= 0.5
          place_pos[1] += (int(place_label == '(c, de)') * 2 - 1) * 0.25
        elif place_label in ['(de, |)']:
          place_pos[0] += 0.25
        elif place_label in ['(a, b)', '(a, c)']:
          place_pos[0] -= 0.25
          place_pos[1] += (int(place_label == '(a, b)') * 2 - 1) * 0.5
        elif place_label in ['(ab, c)', '(b, d)']:
          place_pos[1] += 0.25
          place_pos[0] += (int(place_label == '(b, d)')) * 0.25
        elif place_label in ['(ac, b)', '(c, e)']:
          place_pos[1] -= 0.25
          place_pos[0] += (int(place_label == '(c, e)')) * 0.25
        elif place_label in ['(b, e)', '(c, d)']:
          place_pos[0] += 0
          place_pos[1] += (int(place_label == '(b, e)') * 2 - 1) * 0.75

      for index, transition in enumerate(input_transitions + output_transitions):
        transition_pos = utils.get_point_on_square(np.array([*transition.position[:2], 0]), place_pos, 0.5)
        place_pos_arrow = utils.get_point_on_circle(place_pos, transition.position, new_place.radius)
        start, end = (transition_pos, place_pos_arrow) if index < len(input_transitions) else (place_pos_arrow, transition_pos)
        arrow = Arrow(start, end, **arrow_kwargs)
        self.places.append(arrow)
        animations.append(FadeIn(arrow))
      new_place.move_to(place_pos)
      self.places.append(new_place)
      self.add(new_place)

      self.add_packet_to_node(new_place, WHITE)

      animations.append(FadeIn(new_place))
    return [('play', *animations)]


  def get_packet(self, random=False, random_seed=None, as_vector=False, color=None):
    if as_vector:
      packet = TexMobject('\\begin{bmatrix} ' + f'{np.random.rand() if random else 0.12:.2f}'[1:] + ' \\\\ \\vdots \\\\ ' +
                          f'{np.random.rand() if random else 0.73:.2f}'[1:] + ' \\end{bmatrix}')
      packet.scale_in_place(0.3)
    else:
      width, margin = 0.2, 0.03
      if color is None:
        color = utils.get_random_color(random_seed) if random else WHITE
      packet = VGroup(
        Rectangle(height=0.3, width=width, color=color),
        Line((width / 2 - margin) * LEFT, (width / 2 - margin) * RIGHT, color=color, stroke_width=1.5),
        Line((width / 2 - margin) * LEFT + 0.07 * UP, (width / 2 - margin) * RIGHT + 0.07 * UP, color=color, stroke_width=1.5),
        Line((width / 2 - margin) * LEFT + 0.07 * DOWN, (width / 2 - margin) * RIGHT + 0.07 * DOWN, color=color, stroke_width=1.5)
      )
    return packet

  def packet_propagation(self):
    move_animations = []

    def update_packet(mob, alpha):
      # mob.become(mob.copy())
      minimum_scale = 0.6
      mob.rescale_to_fit(max(minimum_scale, 1 - alpha) * 0.3, 1) if alpha < 0.5 else mob.rescale_to_fit(max(minimum_scale, alpha) * 0.3, 1)
      mob.shift(alpha * 0.1 * RIGHT)

    for mobject in self:
      if hasattr(mobject, 'packet'):
        packet = mobject.packet
        packet.remove_updater(self.update_packet_positions)
        move_animations.append(UpdateFromAlphaFunc(packet, update_packet, rate_func=linear, run_time=1))
        # move_animations.extend([ApplyMethod(packet.shift, RIGHT, ScaleInPlace(packet, 0.5))])
        # scale_animations.extend([ScaleInPlace(packet, 0.5)])
    return [('play', *move_animations)]

  def add_packets(self):
    fade_in_animations = []
    for mobject in self:
      if hasattr(mobject, 'packet'):
        packet = mobject.packet
        packet.move_to(packet.position)
        fade_in_animations.append(FadeIn(packet))
    return [('play', *fade_in_animations)]

  def animate_packet_propagation(self, rounds=1, to_specific_nodes=None, as_vector=False):
    # TODO
    return

class Log:
  def __init__(self, traces, labels):
    self.traces = traces
    self.labels = labels

  def get_creation_animation(self):
    text = TexMobject(
      '\\left<a, b, c, d\\right>\\\\\\left<a, c, b, d\\right>\\\\\\left<a, b, c, e\\right>\\\\\\left<a, c, b, e\\right>',
    )
    text.move_to(5 * LEFT + 2 * UP)
    return [('play', Write(text))]


class GraphAnimation:
  def __init__(self, graph_builder):
    self.graph_builder = graph_builder

  def animate_log(self):
    log = Log(self.graph_builder.mTraces, self.graph_builder.mTransitionNames)
    return log.get_creation_animation()

  def animate_trace_graph(self):
    trace_graph = self.graph_builder.mTraceGraph
    traces = self.graph_builder.mTraces
    y_offset = 3.5
    x_offset = max([len(t) for t in traces]) / 2 + 0.5
    positions = [[0.0 - x_offset, y_offset - (len(traces) / 2 - 0.5)], [max([len(t) for t in traces]) + 1 - x_offset, y_offset - (len(traces) / 2 - 0.5)]]
    for index, trace in enumerate(traces):
      positions.extend([[activity_index + 1 - x_offset, y_offset - index] for activity_index in range(len(trace))])
    self.graph = Graph(trace_graph, self.graph_builder.mTransitionNames, positions)
    animations = []
    for traces in [[1], [2, 3, 4], [0]]:
      animations.append(self.graph.get_trace_creation_animation(traces))
      animations.append(('wait', 0.5))
    return animations

  def animate(self, scene):
    animations = []
    animations.extend(self.animate_trace_graph())
    animations.extend(self.graph.transform_to_circles())
    animations.extend(self.graph.change_colors())
    self.graph.add_transitions()
    animations.append(self.graph.get_trace_creation_animation([-1]))
    animations.extend(self.graph.move_transitions())
    animations.extend(self.graph.scale_tracegraph(0.75))
    animations.extend(self.graph.move_tracegraph(4 * RIGHT))
    animations.extend(self.graph.add_places(self.graph_builder.mPossiblePlaces))
    animations.extend(self.graph.remove_places())
    animations.extend(self.graph.add_places(self.graph_builder.mPossiblePlaces, big=True))
    animations.extend(self.graph.add_packets())

    for a in animations:
      getattr(scene, a[0])(*a[1:])

    animations = []
    animations.extend(self.graph.packet_propagation())
    for a in animations:
      getattr(scene, a[0])(*a[1:])

    scene.wait(10)

    return animations
