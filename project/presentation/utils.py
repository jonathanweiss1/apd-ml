import numpy as np
from math import isclose


def get_average_color(colors):
  rgb_colors = []
  for color in colors:
    if hasattr(color, 'rgb'):
      rgb_colors.append(list(color.rgb))
    else:
      rgb_colors.append([int(color[1:3], 16), int(color[3:5], 16), int(color[5:7], 16)])
  mean_color = np.mean(np.array(rgb_colors), axis=0)
  return f'#{int(mean_color[0]):02X}{int(mean_color[1]):02X}{int(mean_color[2]):02X}'

def get_random_color(random_seed=None):
  if random_seed is not None:
    np.random.seed(random_seed)
  r = lambda: np.random.randint(0, 255)
  color = f'#{r():02X}{r():02X}{r():02X}'
  return color


def get_interpolated_point_on_path(path, alpha):
  if alpha < 0.2:
    adjusted_alpha = (alpha / 0.2)
    return (1 - adjusted_alpha) * path[0] + adjusted_alpha * path[1]
  elif alpha > 0.8:
    adjusted_alpha = ((alpha - 0.8) / 0.2)
    return (1 - adjusted_alpha) * path[-2] + adjusted_alpha * path[-1]
  else:
    adjusted_alpha = ((alpha - 0.2) / 0.6)
    return (1 - adjusted_alpha) * path[1] + adjusted_alpha * path[2]

def line_intersection(line1, line2):
  xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
  ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

  def det(a, b):
    return a[0] * b[1] - a[1] * b[0]

  div = det(xdiff, ydiff)
  if div == 0:
    raise Exception('lines do not intersect')

  d = (det(*line1), det(*line2))
  x = det(d, xdiff) / div
  y = det(d, ydiff) / div
  return x, y


def check_point_on_line(line, point):
  if isclose(point[0], line[0][0]):
    if line[0][1] < line[1][1]:
      return line[0][1] <= point[1] <= line[1][1]
    else:
      return line[1][1] <= point[1] <= line[0][1]
  elif isclose(point[1], line[0][1]):
    if line[0][0] < line[1][0]:
      return line[0][0] <= point[0] <= line[1][0]
    else:
      return line[1][0] <= point[0] <= line[0][0]
  return False

def get_adjusted_points(from_node, to_node):
  if from_node.__class__.__name__ == 'VGroup': from_node = next(iter(from_node))
  if to_node.__class__.__name__ == 'VGroup':   to_node = next(iter(to_node))

  from_center = from_node.get_center()
  to_center = to_node.get_center()
  if all(from_center == to_center):
    return from_center, to_center

  if from_node.__class__.__name__ == 'Square':
    from_position = get_point_on_square(from_center, to_center, from_node.side_length)
  elif from_node.__class__.__name__ == 'Circle':
    from_position = get_point_on_circle(from_center, to_center, from_node.radius)
  else:
    raise ValueError(f'type {from_node.__class__.__name__} not understood.')
  if to_node.__class__.__name__ == 'Square':
    to_position = get_point_on_square(to_center, from_center, to_node.side_length)
  elif to_node.__class__.__name__ == 'Circle':
    to_position = get_point_on_circle(to_center, from_center, to_node.radius)
  else:
    raise ValueError(f'type {to_node.__class__.__name__} not understood.')
  return from_position, to_position

def get_point_on_square(square_midpoint, other_point, square_sidelength=0.5):
  radius = square_sidelength / 2
  square_corners = [[square_midpoint[0] - radius, square_midpoint[1] - radius],
                    [square_midpoint[0] + radius, square_midpoint[1] - radius],
                    [square_midpoint[0] + radius, square_midpoint[1] + radius],
                    [square_midpoint[0] - radius, square_midpoint[1] + radius],
                    [square_midpoint[0] - radius, square_midpoint[1] - radius]]
  line = [[square_midpoint[0], square_midpoint[1]], [other_point[0], other_point[1]]]
  points = []
  for square_line in zip(square_corners[:-1], square_corners[1:]):
    try:
      intersection_point = line_intersection(line, square_line)
      if check_point_on_line(square_line, intersection_point):
        points.append(np.array([intersection_point[0], intersection_point[1], 0.0]))
    except Exception:
      pass

  point = min(points, key=lambda point: np.linalg.norm(other_point - point))
  return point

def get_point_on_circle(circle_midpoint, other_point, radius=0.5):
  angle = np.arctan2(other_point[1] - circle_midpoint[1], other_point[0] - circle_midpoint[0])
  return np.array([circle_midpoint[0] + np.cos(angle)*radius, circle_midpoint[1] + np.sin(angle)*radius, 0.0])
