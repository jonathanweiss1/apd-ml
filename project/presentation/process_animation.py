from manimlib.imports import *
from collections import defaultdict
import random

from project.presentation.stickman import StickMan
# from project.presentation.svg_petrinet import SVGPetrinet
import project.presentation.utils as utils

class Person:
  def __init__(self, position, name='', size=0.5):
    self.original_size = size
    self.scaled = 1
    self.name = name
    self.position = position
    self.initialize()

  def show(self, fade=True):
    if fade:
      return FadeIn(self.body)
    else:
      return ShowCreation(self.body)

  def scale(self, factor):
    self.scaled = factor
    return [self.body.scale, factor]

  def move_to(self, position, animate=False):
    if animate:
      return [self.body.move_to, position]
    self.body.move_to(position)

  def collect(self, product, trace=None):
    if trace is not None:
      trace.append('collect')
    old_position = self.body.get_center()
    return self.move_to(product.get_center(), animate=True), self.move_to(old_position, animate=True)

  def send_to_store(self, shop_position, order=True, trace=None):
    if trace is not None:
      trace.append('order' if order else 'pay')
    start_animations, throw_animations, end_animations = [], [], []
    color = utils.get_random_color()
    if order:
      to_send = Rectangle(height=0.4, width=0.25, color=color)
    else:
      to_send = TexMobject('\\$')
    to_send.move_to(self.body.get_center() + 0.35 * UP + 0.4 * RIGHT)
    waves = self.wave()
    if order:
      start_animations.extend([FadeIn(to_send), waves[0]])
    else:
      start_animations.extend([Write(to_send), waves[0]])
    throw_animations.extend([to_send.move_to, shop_position, waves[1]])
    end_animations.extend([FadeOut(to_send)])
    return start_animations, throw_animations, end_animations, color

  def wave(self):
    body = StickMan()
    body.scale(self.original_size * self.scaled)
    # body.scale()
    body.move_to(self.body.stickman.get_center())
    waving = StickMan('wave')
    waving.scale(self.original_size * self.scaled)
    waving.move_to(self.body.stickman.get_center())
    return [Transform(self.body.stickman,waving), Transform(self.body.stickman, body)]

  def initialize(self):
    stickman = StickMan()
    # stickman = SVGPetrinet()
    stickman.scale(self.original_size)
    stickman.move_to(self.position)
    if self.name != '':
      text = TexMobject(self.name)
      text.move_to(self.position + self.original_size*2*UP)
      self.body = VGroup(stickman, text)
      self.body.name = text
    else:
      self.body = VGroup(stickman)
    self.body.stickman = stickman

class Shop:
  def __init__(self, position, name=''):
    self.position = position
    self.initialize()

  def show(self, fade=True):
    if fade:
      return FadeIn(self.shop)
    else:
      return ShowCreation(self.shop)

  def move_to(self, position, animate=False):
    if animate:
      return [self.shop.move_to, position]
    self.shop.move_to(position)

  def assemble_product(self, color=None, trace=None):
    if color is None:
      color = utils.get_random_color()
    if trace is not None:
      trace.append('assemble')
    # to_assemble = Rectangle(height=0.25+0.1*random.random(), width=0.4+0.1*random.random(), color=color, fill_opacity=0.8)
    # to_assemble.move_to(self.shop.get_center() + 2 * DOWN + random.random() * DOWN + (2 * random.random() - 1) * RIGHT)
    to_assemble = Rectangle(height=0.25, width=0.4, color=color, fill_opacity=0.8)
    to_assemble.move_to(self.shop.get_center() + 1.5*DOWN)
    return ShowCreation(to_assemble), to_assemble

  def send_product(self, product, person_position, trace=None):
    if trace is not None:
      trace.append('send')
    return [product.move_to, person_position]

  def aborb_product(self, product):
    return [FadeOut(product)]

  def initialize(self):
    main = Rectangle(height=2, width=3, color=RED)
    door = Rectangle(height=1, width=0.5, color=RED)
    door.move_to(0.5*DOWN+0.25*LEFT)
    door2 = Rectangle(height=1, width=0.5, color=RED)
    door2.move_to(0.5 * DOWN + 0.25 * RIGHT)
    roof = Polygon(UP+1.7*LEFT, 2*UP, UP+1.7*RIGHT, color=RED)
    text = TexMobject(f'HW Store')
    text.move_to(0.75*UP)
    text.scale(0.8)
    text2 = TexMobject(f'SALE!!')
    text2.move_to(1.25*UP)
    self.shop = VGroup(main, roof, door, door2, text, text2)
    self.shop.move_to(self.position)

class ProcessInstance:
  def __init__(self, trace, initial_position=3*UP):
    self.trace = trace
    self.key = ','.join(trace)
    self.text = TexMobject(f'\\left<{", ".join(trace)}\\right>')
    self.text.move_to(initial_position)

  def write(self):
    return Write(self.text)

  def fade_in(self):
    return FadeIn(self.text)
  def fade_out(self):
    return FadeOut(self.text)
  def fade_word_for_word(self):
    return AddTextWordByWord(self.text, run_time=2.0)

class ProcessAnimation:
  def __init__(self):
    self.log_start_position = 2.5*LEFT+DOWN
    self.log_mobjects = None
    self.traces = defaultdict(list)

  def animate(self, scene):
    me = Person([0, 1, 0], 'Me')
    scene.play(me.show(fade=False))
    [scene.play(wave) for wave in me.wave()]
    scene.play(*me.move_to(3*LEFT+UP, animate=True))
    scene.play(*me.scale(0.6))

    self.shop_pos = [3.5, 2, 0]
    self.shop = Shop(self.shop_pos)
    scene.play(self.shop.show(fade=False))

    self.animate_person_order(me, -1, scene, collect=False)

    persons = self.add_people(10, scene)
    for index, person in enumerate(persons):
      if index == 0:
        self.animate_person_order(person, index, scene, first_pay=False, collect=False, time=0.8)
      elif index == 1:
        self.animate_person_order(person, index, scene, first_pay=True, collect=True, time=0.8)
      elif index < 6:
        self.animate_person_order(person, index, scene, first_pay=random.choice([True, False]), collect=random.choice([True, False]), time=0.25)
      else:
        self.animate_person_order(person, index, scene, first_pay=random.choice([True, False]), collect=random.choice([True, False]), time=0.1)

    scene.wait(10)

  def animate_person_order(self, person, index, scene, first_pay=True, collect=False, time=1.):
    start_animations, throw_animations, end_animations, color = person.send_to_store(self.shop_pos, trace=self.traces[index])
    scene.play(*start_animations, run_time=time)
    scene.play(*throw_animations, *self.update_log(), run_time=time)

    if first_pay:
      start_animations, throw_animations, end_animations2, _ = person.send_to_store(self.shop_pos, order=False, trace=self.traces[index])
      scene.play(*end_animations, *start_animations, run_time=time)
      scene.play(*throw_animations, *self.update_log(), run_time=time)
      assemble_animation, product = self.shop.assemble_product(color=color, trace=self.traces[index])
      scene.play(assemble_animation, *self.update_log(), run_time=time)
      scene.play(*end_animations2, run_time=time)
    else:
      assemble_animation, product = self.shop.assemble_product(color=color, trace=self.traces[index])
      scene.play(assemble_animation, *self.update_log(), run_time=time)
      start_animations, throw_animations, end_animations2, _ = person.send_to_store(self.shop_pos, order=False, trace=self.traces[index])
      scene.play(*end_animations, *start_animations, run_time=time)
      scene.play(*throw_animations, *self.update_log(), run_time=time)
      scene.play(*end_animations2, run_time=time)

    send_animations = self.shop.send_product(product, person.body.get_center(), trace=None if collect else self.traces[index])
    if collect:
      go, come = person.collect(product, trace=self.traces[index])
      scene.play(*go, run_time=time)
      scene.play(*send_animations, *come, *self.update_log(), run_time=time)
    else:
      scene.play(*send_animations, *self.update_log(), run_time=time)

  def update_log(self):
    animations = []
    last_trace = sorted(self.traces.items(), key=lambda v: v, reverse=True)[0][1]
    if self.log_mobjects is None:
      t = TexMobject(last_trace[0])
      t.move_to(self.log_start_position)
      self.log_mobjects = [[t]]
      return [FadeIn(t)]

    if len(self.traces) == len(self.log_mobjects):
      t = TexMobject(f', {last_trace[-1]}')
      t.next_to(self.log_mobjects[-1][-1])
      animations.append(FadeIn(t))
      self.log_mobjects[-1].append(t)
    else:
      for tracie in self.log_mobjects:
        for log_mobject in tracie:
          animations.extend([log_mobject.shift, 0.6 * DOWN])
      t = TexMobject(last_trace[-1])
      t.move_to(self.log_start_position)
      animations.append(FadeIn(t))
      self.log_mobjects.append([t])
    return animations

  def add_people(self, n, scene, names=None, positions=None):
    if names is None:
      names = [''] * n
    random.seed(0)
    if positions is None:
      positions = [[-2.5 + random.random()*-4, 3.5 - random.random()*5, 0] for i in range(n)]
    in_animations = []
    wave_animations_0, wave_animations_1 = [], []
    persons = []
    for name, position in zip(names, positions):
      p = Person(position, name, size=0.3)
      persons.append(p)
      p.move_to(position, animate=False)
      in_animations.append(p.show(fade=True))
      waves = p.wave()
      wave_animations_0.append(waves[0])
      wave_animations_1.append(waves[1])
    scene.play(*in_animations)
    scene.play(*wave_animations_0)
    scene.play(*wave_animations_1)
    return persons
