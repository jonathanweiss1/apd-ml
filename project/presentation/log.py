from manimlib.imports import *
from collections import defaultdict
import networkx as nx
import manimnx as mnx
import numpy as np
import random
import matplotlib.pyplot as plt
from project.presentation import utils

class Log:
  def __init__(self, traces, labels):
    self.traces = traces
    self.labels = labels

  def move(self, location):
    return

  def scale(self, factor):
    return

  def highlight_trace(self, trace_index):
    return

  def highlight_directly_follows_from_index(self, trace_index, event_index):
    return

  def highlight_directly_follows_from_relation(self, relation):
    return
