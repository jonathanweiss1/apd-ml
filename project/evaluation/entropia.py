import os
import shlex
import csv
from subprocess import Popen, PIPE, STDOUT, CalledProcessError
from threading import Timer
from pathlib import Path
import json

from project.data_handling.petrinet import PetrinetHandler
from project.evaluation.exporter import export_net

def get_precision_recall(command, verbose=False):
  if os.name == 'posix':
    command = shlex.split(command)

  results = {'precision': None, 'recall': None}
  keywords = {'precision': 'Precision: ', 'recall': 'Recall: '}

  process = Popen(command, stdout=PIPE, stderr=STDOUT, universal_newlines=True)
  for stdout_line in iter(process.stdout.readline, ""):
    if verbose:
      print(stdout_line, end='')
    for metric in ['precision', 'recall']:
      if stdout_line[:len(keywords[metric])] == keywords[metric]:
        results[metric] = float(stdout_line.split(' ')[1][:-2])

  process.stdout.close()
  process.wait()
  if verbose:
    print()
  return results

class Entropia:
  def __init__(self, dataset, method, datadir='/mnt/c/Users/s140511/tue/thesis/thesis_data/evaluation_data', namegetter=None):
    self.datadir = datadir
    self.basedir = '/mnt/c/Users/s140511/tue/thesis/codebase/jbpt-pm/entropia'
    self.dataset = dataset
    self.method = method
    try:
      int(dataset)
      self.log_filename = f'{self.datadir}/logs/{self.dataset}.xes'
    except ValueError:
      self.log_filename = f'{self.datadir}/{self.dataset}/data.xes'
    if namegetter is None:
      self.pnml_filename = f'{self.datadir}/{self.dataset}/results/data_{self.method}.pnml'
    else:
      self.pnml_filename = namegetter(dataset, method)
    self.results = {'precision': 'nan', 'recall': 'nan'}

  def prepare(self):
    filename = self.pnml_filename
    petri_net = PetrinetHandler()
    petri_net.importFromFile(filename)
    export_net(petri_net.mPetrinet, petri_net.mInitialMarking, f'{self.pnml_filename[:-5]}_temp.pnml', final_marking=petri_net.mFinalMarking)

  def cleanup(self):
    os.remove(f'{self.pnml_filename[:-5]}_temp.pnml')

  def compute(self, exact=True, verbose=False, skip=False):
    if skip:
      return
    try:
      self.prepare()
    except OSError:
      print('No file found')
      return
    if exact:
      command = f'java -jar {self.basedir}/jbpt-pm-entropia-1.6.jar -empr -rel={self.log_filename} -ret={self.pnml_filename[:-5]}_temp.pnml'
    else:
      command = f'java -jar {self.basedir}/jbpt-pm-entropia-1.6.jar -pmpr -rel={self.log_filename} -ret={self.pnml_filename[:-5]}_temp.pnml'
    print(command)
    if verbose:
      print()
      print()
    self.results = get_precision_recall(command, verbose=verbose)
    if verbose:
      print()
      print()
    self.cleanup()

  def export(self):
    filename = f'{self.datadir}/{self.dataset}/results/data_{self.method}_cc_temp.txt'
    with open(filename, 'r') as file:
      data = json.load(file)
      data['full'][f'{"entropia_partial_precision":<32}'] = str(self.results['precision'])
      data['full'][f'{"entropia_partial_recall":<32}'] = str(self.results['recall'])
    with open(f'{self.datadir}/{self.dataset}/results/data_{self.method}_cc_temp2.txt', 'w') as f:
      json.dump(data, f, indent=2)

  def export2(self):
    basedir = f'/mnt/c/Users/s140511/tue/thesis/thesis_data/process_trees_medium_ws2'
    filename = f'{basedir}/logs/predictions/new_conformance.csv'
    fields = [f'{self.dataset}/{self.method}', self.results['precision'], self.results['recall']]
    with open(filename, 'a') as f:
      writer = csv.writer(f)
      writer.writerow(fields)

def find_gcn_model(dataset, datadir2='evaluation_data'):
  datadir = f'/mnt/c/Users/s140511/tue/thesis/thesis_data/{datadir2}'
  dir = f'{datadir}/{dataset}/results'
  gcns = []
  for filename in os.listdir(dir):
    if filename[-5:] == '.pnml' and 'gcn' in filename and 'temp' not in filename:
      gcns.append(filename.split('data_')[1].split('.pnml')[0])
  if len(gcns) != 1:
    print('waaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaah', gcns)
  return gcns[0]


def get_name(dataset, method):
  basedir = f'/mnt/c/Users/s140511/tue/thesis/thesis_data/process_trees_medium_ws2'
  if method == 'gcn_sound':
    return f'{basedir}/logs_compressed/predictions/{dataset}_gcn_sound.pnml'
  elif method == 'groundtruth':
    return f'{basedir}/petrinets/{dataset}.pnml'
  else:
    return f'{basedir}/logs/predictions/{dataset}_{method}.pnml'

def eval_data():
  # datadir = '/mnt/c/Users/s140511/tue/thesis/thesis_data/evaluation_data/'
  datasets = ['BPI_2012_A', 'BPI_2012_O', 'BPI_2017_A', 'BPI_2017_O', 'BPI_2020_Domestic_declarations',
              'BPI_2020_International_declarations', 'BPI_2020_Permit_log', 'BPI_2020_Prepaid_travel_cost',
              'BPI_2020_Request_for_payment', 'road_traffic_fine', 'sepsis']
  datasets = ['BPI_2020_Prepaid_travel_cost', 'BPI_2020_Request_for_payment', 'road_traffic_fine', 'sepsis']
  methods = ['split_reduced', 'gcn', 'inductive_reduced', 'heuristics_reduced', 'ilp_reduced']
  for dataset in datasets:
    print(dataset)
    for method in methods:
      if method == 'gcn':
        method = find_gcn_model(dataset)

      entropia = Entropia(dataset, method)
      entropia.compute(exact=False, verbose=True)
      entropia.export()
      print(method, entropia.results)

def test_data():
  datasets = [f'{i:04d}' for i in range(2081, 2082)]
  methods = ['groundtruth', 'gcn_sound', 'ilp', 'split', 'heuristics', 'inductive']
  for dataset in datasets:
    print(dataset)
    skip = False
    for method in methods:
      entropia = Entropia(dataset, method, datadir='/mnt/c/Users/s140511/tue/thesis/thesis_data/process_trees_medium_ws2', namegetter=get_name)
      entropia.compute(verbose=True, skip=skip)
      if method == 'gcn_sound':
        if entropia.results['precision'] == 'nan' or entropia.results['precision'] is None:
          skip = True
      # entropia.export2()
      print(method, entropia.results)


if __name__ == '__main__':
  test_data()


  # results = get_precision_recall(f'java -jar {basedir}/jbpt-pm-entropia-1.6.jar -empr -rel={datadir}/{dataset}/data.xes -ret={datadir}/{dataset}/results/data_{method}.pnml')
  # print(results)

  # results = {'precision': None, 'recall': None}
  # keywords = {'precision': 'Precision: ', 'recall': 'Recall: '}
  # for line in exec(f'java -jar {dir}/jbpt-pm-entropia-1.6.jar -empr -rel={dir}/data/data.xes -ret={dir}/data/inductive.pnml'):
  #   for metric in ['precision', 'recall']:
  #     if line[:len(keywords[metric])] == keywords[metric]:
  #       results[metric] = float(line.split(' ')[1][:-2])
  # print(results)
