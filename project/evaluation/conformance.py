import csv
import os
from collections import defaultdict
import numpy as np
from colorama import Style, Fore

results = []

for filename in os.listdir('/home/dominique/TUe/thesis/git_data/bpmai/predictions'):
  if filename[-4:] == '.csv':
    with open(os.path.join('/home/dominique/TUe/thesis/git_data/bpmai/predictions', filename), 'r') as f:
      csv_reader = csv.reader(f, delimiter=',')
      result = {'dataset': filename[:-len('_conformance.csv')]}
      for index, row in enumerate(csv_reader):
        if index == 0:
          mapping = row
        else:
          result[row[0]] = {}
          for index2, score in enumerate(row[1:]):
            result[row[0]][mapping[index2 + 1]] = score
      results.append(result)

print(len(results))

stats = {'gcn': defaultdict(list), 'alpha': defaultdict(list), 'inductive': defaultdict(list)}
for result in results:
  for algo in ['alpha', 'gcn', 'inductive']:
    for key, value in result[algo].items():
      stats[algo][key].append(float(value))

for metric in ['fscore', 'fitness', 'precision', 'generalization', 'simplicity']:
  print(Fore.RED + metric + Style.RESET_ALL)
  means = []
  for algo in ['alpha', 'gcn', 'inductive']:
    means.append(np.mean(stats[algo][metric]))
  for index, algo in enumerate(['alpha', 'gcn', 'inductive']):
    if np.argmax(means) == index:
      print(f'{Fore.CYAN}{algo:<10}{Style.RESET_ALL} mean: {np.mean(stats[algo][metric]):.3f}        min: {np.min(stats[algo][metric]):.3f}  max: {np.max(stats[algo][metric]):.3f}')
    else:
      print(f'{algo:<10} mean: {np.mean(stats[algo][metric]):.3f}        min: {np.min(stats[algo][metric]):.3f}  max: {np.max(stats[algo][metric]):.3f}')
