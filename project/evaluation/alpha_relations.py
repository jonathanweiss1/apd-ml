import numpy as np
from collections import defaultdict
from pprint import pprint


class AlphaRelations:
  def __init__(self, variants, transitions_names):
    self.variants = variants
    self.__add_start_end()
    self.transition_mapping = self.__get_transition_mapping(list(transitions_names))
    self.directly_follows_relations = self.__get_directly_follows_relations()
    self.directly_follows_relations_dict = self.__get_directly_follows_relations_dict(self.directly_follows_relations)
    self.causal_relations = self.__get_causal_relations()
    self.parallel_relations = self.__get_parallel_relations()


  def get_matrix(self, names=False):
    from prettytable import PrettyTable
    legend = list(self.transition_mapping.values()) if names else list(self.transition_mapping.keys())

    matrix = [[''] + legend]
    for t in legend:
      matrix.append([t] + ['#'] * len(legend))

    for t1, t2 in self.directly_follows_relations:
      matrix[t1 + 1][t2 + 1] = '-'

    for t1, t2 in self.causal_relations:
      matrix[t1 + 1][t2 + 1] = '>'

    for t1, t2 in self.parallel_relations:
      matrix[t1 + 1][t2 + 1] = '||'

    table = PrettyTable(matrix[0])
    for row in matrix[1:]:
      table.add_row(row)

    return matrix, table


  def __add_start_end(self):
    newvariants = []
    for variant in self.variants:
      newvariants.append([0] + [t + 2 for t in variant] + [1])
    self.variants = newvariants


  def __get_transition_mapping(self, transitions_names):
    return {**{0: '>', 1: '|'}, **dict(zip(range(2, len(transitions_names) + 2), transitions_names))}


  def __get_directly_follows_relations(self):
    return set([(t1, t2) for variant in self.variants for t1, t2 in zip(variant[:-1], variant[1:])])


  def __get_directly_follows_relations_dict(self, directly_follows_relations):
    d = defaultdict(set)
    for relation_from, relation_to in directly_follows_relations:
      d[relation_from].add(relation_to)
    return d


  def __get_causal_relations(self):
    return set([(t1, t2) for (t1, t2) in self.directly_follows_relations if (t2, t1) not in self.directly_follows_relations])


  def __get_parallel_relations(self):
    return set([(t1, t2) for (t1, t2) in self.directly_follows_relations if (t2, t1) in self.directly_follows_relations])


if __name__ == '__main__':
  i = 8
  npz = np.load(f'/home/dominique/TUe/thesis/git_data/process_trees/logs_compressed/{i:04d}.npz', allow_pickle=True)
  variants = [list(variant) for count, variant in npz['variants']]
  alpha_relations = AlphaRelations(variants, npz['transitions'][:,0])
  _, table = alpha_relations.get_matrix(names=True)
  print(table)

  show_petrinet = True
  alpha_algo = False
  inductive_algo = False

  # # Parse the event log data and create a alpha relations table from the activities.
  # from pm4py.objects.log.importer.xes import importer as xes_importer
  #
  # i = 1
  #
  # log = xes_importer.apply(f'/home/dominique/TUe/thesis/git_data/process_trees/logs/{i:04d}.xes')
  #
  # from pm4py.algo.filtering.log.variants import variants_filter
  # variants = list(variants_filter.get_variants(log).keys())
  # print(variants)

  if show_petrinet:
    from pm4py.objects.petri.importer import importer as pnml_importer
    net, initial_marking, final_marking = pnml_importer.apply(f'/home/dominique/TUe/thesis/git_data/process_trees/petrinets/{i:04d}.pnml')

    from pm4py.visualization.petrinet import visualizer as pn_visualizer
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    pn_visualizer.view(gviz)

  if alpha_algo:
    from pm4py.objects.log.importer.xes import importer as xes_importer
    from pm4py.algo.discovery.alpha import algorithm as alpha_miner

    log = xes_importer.apply(f'/home/dominique/TUe/thesis/git_data/process_trees/logs/{i:04d}.xes')
    net, initial_marking, final_marking = alpha_miner.apply(log)
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    pn_visualizer.view(gviz)

  if inductive_algo:
    from pm4py.objects.log.importer.xes import importer as xes_importer
    from pm4py.algo.discovery.inductive import algorithm as inductive_miner
    from pm4py.algo.discovery.inductive.util.petri_cleaning import petri_reduction_treplay

    log = xes_importer.apply(f'/home/dominique/TUe/thesis/git_data/process_trees/logs/{i:04d}.xes')
    net, initial_marking, final_marking = inductive_miner.apply(log)

    from pm4py.algo.conformance.tokenreplay import algorithm as token_replay
    replayed_traces = token_replay.apply(log, net, initial_marking, final_marking)

    net = petri_reduction_treplay(net, {'aligned_traces': replayed_traces})

    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    pn_visualizer.view(gviz)



  # from pm4py.algo.discovery.dfg import algorithm as dfg_discovery
  # dfg = dfg_discovery.apply(log)
  # print(dfg)
  #
  #
  # from pm4py.algo.discovery.alpha.data_structures.alpha_classic_abstraction import ClassicAlphaAbstraction
  #
  # ab = ClassicAlphaAbstraction([], [], dfg)
  # print(ab.causal_relation)
  # print(ab.parallel_relation)
  #
  # # from pm4py.visualization.dfg import visualizer as dfg_visualization
  # # gviz = dfg_visualization.apply(dfg, log=log, variant=dfg_visualization.Variants.FREQUENCY)
  # # dfg_visualization.view(gviz)