from copy import copy
import numpy as np
import random
import simpy

class Node:
  def __init__(self, fId):
    self.mId = fId

  def createIncomingEdge(self, fOtherNode):
    pass

  def createOutgoingEdge(self, fOtherNode):
    pass

class StartNode(Node):
  def __init__(self, fId):
    super().__init__(fId)

  def createIncomingEdge(self, fOtherNode):
    return

class EndNode(Node):
  def __init__(self, fId):
    super().__init__(fId)

  def createOutgoingEdge(self, fOtherNode):
    return

class Activity(Node):
  def __init__(self, fId, fActivityName):
    super().__init__(fId)
    self.mActivityName = fActivityName

class Gateway(Node):
  def __init__(self, fId):
    super().__init__(fId)

class Split:
  def __init__(self):
    pass

class Join:
  def __init__(self):
    pass

class Choice(Gateway, Split, Join):
  def __init__(self, fId, fSplitOrJoin=False):
    Gateway.__init__(self, fId)
    Split.__init__(self) if fSplitOrJoin else Join.__init__(self)

class Parallelism(Gateway, Split, Join):
  def __init__(self, fId, fSplitOrJoin=False):
    Gateway.__init__(self, fId)
    Split.__init__(self) if fSplitOrJoin else Join.__init__(self)

class ModelGenerator:
  def __init__(self, fParameters):
    self.mParameters = fParameters
    self.mModel = StartNode(0)
    self.mNodeIdCount = 1

    self.mSimulationEnvironment = simpy.Environment()
    self.mTraces = []
    self.mTrace = []
    self.mMaxTraceLength = 0

    self.mActions = {
      'sequence':    self.__createSequence,
      'parallelism': self.__createParallelismSplit,
      'choice':      self.__createChoiceSplit,
      'cycle':       self.__createCycle,
    }


  def generate(self):
    action = np.random.choice(list(self.mParameters['actions'].keys()), 1, p=list(self.mParameters['actions'].values()))[0]
    self.mSimulationEnvironment.process(self.mActions[action])

  def __createParallelismSplit(self, fMaxEndTimestamp, fVerbose=False):
    yield self.mSimulationEnvironment.timeout(0)
    # TODO
    pass

  def __createChoiceSplit(self, fMaxEndTimestamp, fVerbose=False):
    yield self.mSimulationEnvironment.timeout(0)
    pass

  def __createSequence(self, fMaxEndTimestamp, fVerbose=False):
    yield self.mSimulationEnvironment.timeout(0)
    return

  def __createCycle(self, fMaxEndTimestamp, fVerbose=False):
    yield self.mSimulationEnvironment.timeout(0)
    # TODO
    pass

  def __createActivity(self, fMaxEndTimestamp, fVerbose=False):
    yield self.mSimulationEnvironment.timeout(0)
    pass


if __name__ == '__main__':
  parameters = {'actions': {'sequence': 0.8, 'parallelism': 0.1, 'choice': 0.1, 'cycle': 0.0}}
  modelGenerator = ModelGenerator(parameters)
  modelGenerator.generate()
