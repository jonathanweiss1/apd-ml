import numpy as np
from colorama import Fore, Style

from collections import defaultdict

COLORS = [Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.CYAN, Fore.WHITE]

class Node():
  def __init__(self, fName):
    self.mName = fName
    self.mChildren = []

  def isLeaf(self):
    return len(self.mChildren) == 0

  def getLeafs(self):
    leafs = [child for child in self.mChildren if child.isLeaf()]
    for child in self.mChildren:
      leafs.extend(child.getLeafs())
    return leafs

  def getTransitions(self):
    leafs = self.getLeafs()
    return [leaf for leaf in leafs if leaf.mName not in ['sequence', 'parallelism', 'choice']]

  def toString(self, fDepth):
    color = COLORS[fDepth % len(COLORS)]
    if self.isLeaf():
      return f'{color}{self.mName}{Style.RESET_ALL}'
    else:
      return f'{self.mName} -> {color}({", ".join([(child.toString(fDepth + 1)) for child in self.mChildren])}{color}){Style.RESET_ALL}'

class ProcessTree:
  def __init__(self, fParameters):
    self.mRoot = Node('sequence')
    self.mParameters = fParameters

  def fromString(self, fString):
    node = fString.split(')')[-1]

    print(node)

  def generateTree(self, fMaxNumberOfNodes):
    self.mCurrentNodeName = 0
    self.mMaxNumberOfNodes = fMaxNumberOfNodes
    self.generate(self.mRoot)
    return self.mRoot

  def __normalize(self, mProbabilities):
    total = sum(mProbabilities)
    return [p / total for p in mProbabilities]

  def checkForStop(self):
    return len(self.mRoot.getTransitions()) >= self.mMaxNumberOfNodes

  def generate(self, fCurrentNode):
    if self.checkForStop():
      return
    forbiddenActions = [fCurrentNode.mName]
    probabilityMapping = self.mParameters['actions'].copy()
    for forbiddenAction in forbiddenActions:
      probabilityMapping[forbiddenAction] = 0
    probabilities = self.__normalize(probabilityMapping.values())
    action = np.random.choice(list(self.mParameters['actions'].keys()), 1, p=probabilities)[0]
    if action == 'leaf':
      fCurrentNode.mChildren.append(Node(self.mCurrentNodeName))
      self.mCurrentNodeName += 1
    else:
      node = Node(action)
      fCurrentNode.mChildren.append(node)
      numberOfChildren = np.random.randint(2, 3)
      [self.generate(node) for i in range(numberOfChildren)]

if __name__ == '__main__':
  parameters = {'actions': {'sequence': 0.45, 'parallel': 0.25, 'choice': 0.2, 'loop': 0.1, 'or': 0.0},
                'numberOfNodes': {'min': 4, 'mode': 4, 'max': 5},
                'additional': defaultdict(float)}
  processTree = ProcessTree(parameters)
  tree = processTree.generateTree(5)


  # processTree.fromString('((c:0.333333,a:0.333333,(e:1,(f:1,g:1)sequence:1)parallel:0.333333)choice:1,(b:1,d:1)parallel:1)sequence:1')
  # processTree.generateTree(5)
  # print(processTree.mRoot.toString(0))
  