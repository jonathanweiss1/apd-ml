from project.data_handling.petrinet import getPetrinetFromFile
from project.data_handling.log import LogHandler

from pm4py.algo.conformance.alignments import algorithm as alignments
from pm4py.algo.conformance.tokenreplay import algorithm as tokenreplay_factory
from pm4py.evaluation.replay_fitness import evaluator as replay_fitness_factory
from pm4py.evaluation.precision import evaluator as precision_evaluator

from collections import defaultdict
from pm4py.algo.conformance.alignments import algorithm as alignments
import time

class ConformanceChecker:
  def __init__(self, fLogFilename, fPetrinetFilename):
    self.mLogHandler = LogHandler(fLogFilename)
    self.mPetrinet = getPetrinetFromFile(fPetrinetFilename)
    self.mAlignments = None
    self.mMetrics = defaultdict(lambda: None)

  def align(self):
    start_time = time.time()
    self.mAlignments = alignments.apply_log(self.mLogHandler.mLog, *self.mPetrinet[:3], parameters={'max_align_time_trace': 60}, variant=alignments.VERSION_DIJKSTRA_LESS_MEMORY)
    print(f'Computing alignments took {time.time() - start_time:.2f} seconds.')

  def replay(self):
    self.mAlignments = tokenreplay_factory.apply(self.mLogHandler.mLog, *self.mPetrinet)

  def computeFitness(self, fMethod='alignments'):
    if fMethod == 'alignments':
      self.align()
      self.mMetrics['fitness'] = replay_fitness_factory.evaluate(self.mAlignments, variant='alignments')
    elif fMethod == 'tokenReplay':
      self.replay()
      self.mMetrics['fitness'] = replay_fitness_factory.evaluate(self.mAlignments, variant='token_replay')

  def computePrecision(self):
    print(f'Precision: {precision_evaluator.apply(self.mLogHandler.mLog, *self.mPetrinet)}')

  def computeGeneralization(self):
    raise NotImplementedError('Not implemented yet.')

  def report(self):
    for metric, value in self.mMetrics.items():
      print(f'{metric}: {value}')

if __name__ == '__main__':
  dataset = 'BPI_2012_A'
  petrinet_filename = f'/home/dominique/TUe/thesis/git_data/evaluation_data/{dataset}/data_gcn_beam_14.pnml'
  log_filename = f'/home/dominique/TUe/thesis/git_data/evaluation_data/{dataset}/data.xes'
  conformance = ConformanceChecker(log_filename, petrinet_filename)
  conformance.computeFitness()
  print(conformance.mMetrics['fitness'])
  conformance.computePrecision()
