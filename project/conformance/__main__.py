from project.conformance.conformance_checking import ConformanceChecker

import argparse
import json
import os

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', help='Config filename in json format', type=str)
parser.add_argument('-d', '--dataDirectory', help='dataDirectory', type=str)
parser.add_argument('-l', '--logFilename', help='logFilename', type=str)
parser.add_argument('-pf', '--petrinetFilename', help='petrinetFilename', type=str)

parser.add_argument('-f', '--fitness', help='Compute fitness', action='store_true')
parser.add_argument('-p', '--precision', help='Compute precision', action='store_true')
parser.add_argument('-g', '--generalization', help='Compute generalization', action='store_true')

args = parser.parse_args()

if args.config is None:
  args.config = 'config.json'

with open(f'{os.path.dirname(os.path.realpath(__file__))}/{args.config}') as jsonFile:
  config = json.load(jsonFile)

if args.dataDirectory is None:
  args.dataDirectory = config['dataDirectory']

if args.logFilename is None:
  args.logFilename = config['logFilename']

###################
# Start of script #
###################

conformanceChecker = ConformanceChecker(
  f'{args.dataDirectory}/{args.logFilename}', f'{args.dataDirectory}/petrinets/{args.petrinetFilename}')

# if any([args.fitness, args.precision, args.generalization]):
#   conformanceChecker.align()

if args.fitness:
  conformanceChecker.computeFitness('tokenReplay')

if args.precision:
  conformanceChecker.computePrecision()

if args.generalization:
  conformanceChecker.computeGeneralization()

conformanceChecker.report()
