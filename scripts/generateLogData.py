import os
from tqdm import tqdm

currentDirectory = os.getcwd()
os.chdir('/home/dominique/TUe/thesis/git')

# with open('/home/dominique/TUe/thesis/git_data/BPMNs/bpmai/soundNets1.txt', 'r') as file:
directory = '/home/dominique/TUe/thesis/git_data/NRIExperimentData/graphs'
for filename in tqdm(sorted(os.listdir(directory))):
  if not filename.endswith('.pnml'):
    continue

  filename = f'{directory}/{filename}'

  datasetName = filename.split('/')[-1].split('.')[0]
  generateLogCommand = f'python3 -m project.data_generation -pf {filename} -d /home/dominique/TUe/thesis/git_data/NRIExperimentData/logs -n 10000'
  os.system(generateLogCommand)
  convertToNPYCommand = f'python3 -m project.log_data_analysis -lf NRIExperimentData/logs/{datasetName} -e NRIExperimentData/logs/{datasetName}'
  os.system(convertToNPYCommand)
  os.system(f'rm -f /home/dominique/TUe/thesis/git_data/NRIExperimentData/logs/{datasetName}.xes')

os.chdir(currentDirectory)
