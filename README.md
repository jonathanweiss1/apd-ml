This repository contains the source code for our paper 'Process Discovery using Graph Neural Networks' [[1]](#1).


This project has several 'main' files with different purposes:
Run with --help to show the usage, i.e. which additional arguments should be used for each script.

E.g. `python3 -m project/ml_models --help`.   

### Training a new model
`./project/ml_models/__main__.py`

### Process model discovery
`./project/process_mining/__main__.py`

### Compute and report conformance between process models and event logs
`./project/conformance/__main__.py`

### Analyzing properties of event logs
`./project/log_data_analysis/__main__.py`

### Generate event logs from Petri nets
`./project/data_generation/__main__.py`

Furthermore, the `evaluation` and `presentation` directories contain messy code used to generate respectively figures and animations.

### Remark
The code currently still contains hardcoded filepaths that link to a local filesystem.
The files that are used for training can be generated using ProM as mentioned in the paper.
The files that are used for evaluation are publicly available online as mentioned in the paper as well.
alternatively, these files are also hosted on https://gitlab.com/dominiquesommers/thesis_data and can be used accordingly.
Note that this does require the user to correct some hardcoded filepaths.

## References
<a id="1">[1]</a> 
Sommers, D., Menkovski, V., & Fahland, D. (2021, October). Process discovery using graph neural networks. In 2021 3rd International Conference on Process Mining (ICPM) (pp. 40-47). IEEE.